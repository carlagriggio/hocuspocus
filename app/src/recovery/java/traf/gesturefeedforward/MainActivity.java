package traf.gesturefeedforward;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.gesture.Gesture;
import android.gesture.GestureStroke;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.RectF;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.InputType;
import android.text.format.DateFormat;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.view.ViewGroup.LayoutParams;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.util.Date;


public class MainActivity extends Activity implements DialogInterface.OnClickListener, View.OnClickListener {

    // TODO adjust size and atom count for Path-based feedforward
    // TODO evaluate > 4 paths, display top 3 and bottom 1

    //Mode mode = Mode.OCTOPOCUS_FF;

    EditText mParticipantInput;
    TextView commandName;
    TextView commandInstruction;


    Button nextTrialButton;
    int participantID = 0;
    ExperimentSession experiment;


    public GestureCanvasFrame gestureCanvasFrame;
    private FrameLayout mainLayout;
    public DrawRenderer renderer;
    private Button backButton;
    Constructor<GestureStroke> mGestureStroke_constructor;
    private ExperimentSession mainExperiment;


//    public void hideDetectedCommand() {
//        detectedGesture.setVisibility(View.INVISIBLE);
//    }
//    public void hideScore() {
//        currentScore.setVisibility(View.INVISIBLE);
//    }
//    public void showDetectedCommand() {
//        detectedGesture.setVisibility(View.VISIBLE);
//    }
//    public void showScore() {
//        currentScore.setVisibility(View.VISIBLE);
//    }

    public void startNextTask(Task nextTask) {
        experiment.setCurrentTask(nextTask);
        nextTask.start(this);
    }
    public void showFFCanvas() {
        mainLayout.removeView(gestureCanvasFrame);
        gestureCanvasFrame = new GestureCanvasFrameWithFF(this);
        mainLayout.addView(gestureCanvasFrame);
        gestureCanvasFrame.resetCanvas(this);
        gestureCanvasFrame.setRegistrationFeedback();

    }
    public void showPlainCanvas() {
        mainLayout.removeView(gestureCanvasFrame);
        gestureCanvasFrame = new GestureCanvasFrame(this);
        mainLayout.addView(gestureCanvasFrame);
        gestureCanvasFrame.resetCanvas(this);
    }




    // Resources needed for matching the stroke to existing templates
    File mGesturesFile = new File(Environment.getExternalStorageDirectory(), "gestures");
//    GestureStore mStore = new GestureStore();


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Task.logFilePath = this.getFilesDir().getPath();
        //Session.getInstance().openConnection();
        GLSurfaceView gl = new GLSurfaceView(this);
        gl.setEGLContextClientVersion(2);
        gl.setEGLConfigChooser(8, 8, 8, 8, 0, 0);
        renderer = new DrawRenderer();
        gl.setRenderer(renderer);
        addContentView(gl, new LayoutParams(-1, -1));

        mainLayout = new FrameLayout(this);
        LinearLayout buttonsLayout = new LinearLayout(this);

        gestureCanvasFrame = new GestureCanvasFrameWithFF(this);
        mainLayout.addView(gestureCanvasFrame);
        //commandName.setId(1);

        //create a text view
        commandName = new TextView(this);
        commandName.setText("");
        commandName.setTextSize(28.0f);
        //commandName.setId(1);
        commandName.setGravity(Gravity.CENTER_HORIZONTAL);
        commandName.setY(180);
        commandName.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        //adds the textview
        //layout.addView(commandName);

        commandInstruction = new TextView(this);
        commandInstruction.setText("");
        commandInstruction.setTextSize(16.0f);
        //commandName.setId(1);
        commandInstruction.setGravity(Gravity.CENTER_HORIZONTAL);
        commandInstruction.setY(290);


        mainLayout.addView(commandName);
        mainLayout.addView(commandInstruction);

        //create a button
        backButton = new Button(this);
        backButton.setText("Skip");
        backButton.setTextSize(24);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goToNextTrial();
            }
        });

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        //create a button
        nextTrialButton = new Button(this);
        nextTrialButton.setOnClickListener(this);
        //b.setId(2);
        nextTrialButton.setText("Save Gesture");
        nextTrialButton.setTextSize(24);
        nextTrialButton.setWidth(800);
        nextTrialButton.setPadding(20, 40, 20, 50);
        nextTrialButton.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

        //b.setId(2);
        backButton.setText("Skip");
        backButton.setTextSize(24);
        backButton.setPadding(20, 40, 20, 50);
        backButton.setWidth(280);
        backButton.setVisibility(View.INVISIBLE);

        buttonsLayout.addView(backButton);
        buttonsLayout.addView(nextTrialButton);

        //I'm very sorry for this horrible way of NOT grouping views together :P

        addContentView(mainLayout, new LayoutParams(-1, -1));
//        addContentView(nextTrialButton, new LayoutParams(-2, -2));
//        addContentView(backButton, new LayoutParams(-2, -2));
        addContentView(buttonsLayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));


        mParticipantInput = new EditText(this);
        mParticipantInput.setHint("Participant number");
        mParticipantInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setView(mParticipantInput)
                .setCancelable(false)
                .setPositiveButton("OK", this)
                .show();

        // Mandatory hack to make a GestureStroke out of an array of points.
        try {
            mGestureStroke_constructor = GestureStroke.class.getDeclaredConstructor(RectF.class, float.class, float[].class, long[].class);
            mGestureStroke_constructor.setAccessible(true);
        }
        catch (NoSuchMethodException e) {
            System.out.println(e.getMessage());
        }

        // Load the initial set of templates
        // TODO: do we need this now?
//        try {
//            experiment.getGestureStore().load(mGesturesFile.canRead()
//                    ? new FileInputStream(mGesturesFile)
//                    : getResources().openRawResource(R.raw.gestures), true);
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
        //Toast.makeText(this, mStore.getGestureEntries().size() + " gestures loaded from " + (mGesturesFile.canRead() ? mGesturesFile.getPath() : "resource file"), Toast.LENGTH_SHORT).show();



//        mCommands = new Command[mStore.getGestureEntries().size()];
//        int i = 0;
//        for (String s : mStore.getGestureEntries())
//            mCommands[i++] = new Command(s, mStore.getGestures(s).get(0).getStrokes().get(0).points);



        // build the commands
//        buildGestureSet(experiment.getGestureSetNumber());
        //int gestureSetIndex = 0;
        if (!Environment.getExternalStorageState().equals("mounted"))
            Toast.makeText(this, "SD card not detected", Toast.LENGTH_SHORT).show();


    }

    private void goToNextTrial() {
        experiment.currentTask.skip(this);
    }

    private void goBackToPreviousTask() {
        Task previousTask = experiment.getCurrentTask().getPreviousTask();
        experiment.setCurrentTask(previousTask);
        previousTask.start(this);
    }

    public void showBackButton() {
        backButton.setVisibility(View.VISIBLE);
    }
    public void hideBackButton() {
        backButton.setVisibility(View.INVISIBLE);
    }



    // OnClickListener interface for receiving the participant number.
    public void onClick(DialogInterface dialog, int which) {
        participantID = Integer.parseInt(mParticipantInput.getText().toString());
        try {
            createExperiment();
            File participantsGestures = new File(experiment.fileLocation() + "/gestures-P"+participantID+"-exp-exp");
            experiment.getGestureStore().load(new FileInputStream(participantsGestures), true);
            mainExperiment = experiment;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        startSecondTraining();
        //buildGestureSet(1);
    }

//    void buildCommands(int gestureSetIndex) {
//        mCommands = new Command[6];
//        int i = 0;
//
//        switch (gestureSetIndex) {
//            case 0: // Gesture Set 1
//                // Cut (1 stroke)
//                mCommands[i++] = new Command("banana", 4, new int[]{1, 4, 1, 0},
//                        new int[]{1, -1, -1, 1}, new double[]{Math.PI * -0.67, 0, 0, 0},
//                        new float[]{1, 0.5f, 1, 0.5f});
//
//                // energy save on (1 stroke)
//                mCommands[i++] = new Command("lemon", 3, new int[]{4, 1, 0},
//                        new int[]{1, -1, 1}, new double[]{Math.PI * 0.42, 0, 0},
//                        new float[]{1, 0.5f, 0.5f});
//
//                // weather report (2 strokes)
//                mCommands[i++] = new Command("avocado", 2, new int[]{0, 0}, new int[]{-1, 1},
//                        new double[]{Math.PI, Math.PI * -0.5}, new float[]{1f, 1.5f});
//
//                // clock set (3 strokes)
//                mCommands[i++] = new Command("cherry", 3, new int[]{0, 0, 0},
//                        new int[]{1, 1, -1},
//                        new double[]{Math.PI * -0.4, Math.PI * 0.8, Math.PI * -0.8},
//                        new float[]{1f, 0.5f, 1.5f});
//                // paste (3 strokes)
//                mCommands[i++] = new Command("orange", 3, new int[]{0, 0, 0}, new int[]{1, 1, 1},
//                        new double[]{Math.PI * 0.5, Math.PI * -0.5, Math.PI * -0.5},
//                        new float[]{0.75f, 0.75f, 1.5f});
//
//                // news report (4 strokes)
//                mCommands[i++] = new Command("kiwi", 4, new int[]{0, 0, 0, 0},
//                        new int[]{1, 1, 1, 1},
//                        new double[]{Math.PI * 0.25, Math.PI * 0.5, Math.PI * -0.75, Math.PI * 0.6},
//                        new float[]{1.5f, 0.5f, 0.5f, 0.5f});
//
//
//                break;
//
//        }
//    }








    @Override
    // OnClickListener interface for saving the current gesture when user presses Button.
    public void onClick(View v) {
        experiment.getCurrentTask().finishTask(this);
    }


    public void saveGesture(String label, Gesture g) {
        experiment.getGestureStore().addGesture(label, g);
        //TODO: do the following at the end of the experiment with all gestures?
//        try {
//            experiment.gestureStore.save(new FileOutputStream(new File(experiment.fileLocation() + "/gestures-"+experiment.logFileName())), true);
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
    }

    public void startNewTrial() {
        experiment.setCurrentTask(experiment.newTrial()); //returns the first task of the new trial
        renderer.mode = experiment.currentBlock.currentFeedForwardType();
        blinkCommandName();
//        ffCanvasView.currentScore.setText("");
        experiment.getCurrentTask().start(this);
    }

    public void setCommandLabelAndInstructions(String label, String instructions) {
        commandName.setText(label);
        commandInstruction.setText(instructions);
    }
    public void setLabelOfNextTaskButton(String label) {
        nextTrialButton.setText(label);
    }

    public void blinkCommandName() {
        blink(commandName);
    }
    public void blink(final TextView aTextView) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 150;    //in milissegunds
                int timesBlinked = 0;

                try {
                    Thread.sleep(timeToBlink);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (aTextView.getVisibility() == View.VISIBLE) {
                            aTextView.setVisibility(View.INVISIBLE);
                            blink(aTextView);
                        } else {
                            aTextView.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        }).start();
    }
    private void startTraining() {
        experiment = new TrainingSession(participantID);
        experiment.trialOrderStrategy = new IncrementalOrder();

        experiment.setLogPrefix("tra");
        experiment.addBlock(new ExperimentBlock(experiment, 2, new FixedConditionBlockingStrategy(Mode.NO_FF), false, false));
        experiment.addBlock(new ExperimentBlock(experiment, 2,new FixedConditionBlockingStrategy(new BetweenSubjectsBlockingStrategy().feedForwardForTrial(participantID,1,1)), false, false));
        experiment.addBlock(new ExperimentBlock(experiment, 2,new FixedConditionBlockingStrategy(new BetweenSubjectsBlockingStrategy().feedForwardForTrial(participantID,1,1)), false, false));

//        experiment.setParticipant(participantID);
        startNewBlock();
    }
    private void startSecondTraining() {
        mainExperiment = experiment;
        experiment = new TrainingSession(participantID);
        experiment.blockNumber = 3;
        experiment.trialOrderStrategy = new IncrementalOrder();
        experiment.createCommands();

        experiment.setLogPrefix("tra2");
        experiment.saveGesture(experiment.commands.get(0).label, mainExperiment.gestureStore.getGestures(experiment.commands.get(0).label).get(0));
        experiment.addBlock(new ExperimentBlock(experiment, 2, new FixedConditionBlockingStrategy(Mode.NO_FF), false, false));
        experiment.addBlock(new ExperimentBlock(experiment, 2, new FixedConditionBlockingStrategy(new BetweenSubjectsBlockingStrategy().feedForwardForTrial(participantID, 1, 1)), false, false));
        experiment.addBlock(new ExperimentBlock(experiment, 2, new FixedConditionBlockingStrategy(new BetweenSubjectsBlockingStrategy().feedForwardForTrial(participantID, 1, 1)), false, false));
        experiment.addBlock(new ExperimentBlock(experiment, 3,new FixedConditionBlockingStrategy(new BetweenSubjectsBlockingStrategy().feedForwardForTrial(participantID,4,1)), false, false));
        experiment.addBlock(new ExperimentBlock(experiment, 3,new FixedConditionBlockingStrategy(new BetweenSubjectsBlockingStrategy().feedForwardForTrial(participantID,5,1)), false, false));

//        experiment.setParticipant(participantID);
        startNewBlock();
    }
    private void startExperiment() {
        ExperimentSession experimentalConditions = new ExperimentSession(participantID);
        experimentalConditions.setGestureStore(experiment.gestureStore);
//        experimentalConditions.testAfterEachBlock = false;
        experimentalConditions.trialOrderStrategy = new Counterbalanced3x6OrderBetween();
//        experimentalConditions.showTitleAlerts = false;
        experimentalConditions.setLogPrefix("exp");
        experimentalConditions.addBlock(new ExperimentBlock(experimentalConditions, 6, new BetweenSubjectsBlockingStrategy()));
        experimentalConditions.addBlock(new ExperimentBlock(experimentalConditions, 6, new BetweenSubjectsBlockingStrategy()));
        experimentalConditions.addBlock(new ExperimentBlock(experimentalConditions, 6, new BetweenSubjectsBlockingStrategy()));

        experiment = experimentalConditions;
        startNewBlock();
    }
    private void createExperiment() {
        ExperimentSession experimentalConditions = new ExperimentSession(participantID);
//        experimentalConditions.testAfterEachBlock = false;
        experimentalConditions.trialOrderStrategy = new Counterbalanced3x6OrderBetween();
//        experimentalConditions.showTitleAlerts = false;
        experimentalConditions.setLogPrefix("exp");
        experimentalConditions.addBlock(new ExperimentBlock(experimentalConditions, 6, new BetweenSubjectsBlockingStrategy()));
        experimentalConditions.addBlock(new ExperimentBlock(experimentalConditions, 6, new BetweenSubjectsBlockingStrategy()));
        experimentalConditions.addBlock(new ExperimentBlock(experimentalConditions, 6, new BetweenSubjectsBlockingStrategy()));

        experiment = experimentalConditions;
    }
    private void startSecondExperiment() {
        experiment = mainExperiment;
        new AlertDialog.Builder(this)
                .setTitle("EXPERIMENT PART 2")
                .setMessage("All the gestures from PART 1 are still in the system, but the gestures you created during the last training were deleted.")
                .setCancelable(false)
                .setPositiveButton("OK, START", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mainExperiment.blockNumber = 3;
                        mainExperiment.addBlock(new ExperimentBlock(mainExperiment, 6, new BetweenSubjectsBlockingStrategy()));
                        mainExperiment.addBlock(new ExperimentBlock(mainExperiment, 6, new BetweenSubjectsBlockingStrategy()));
                        mainExperiment.addBlock(new ExperimentBlock(mainExperiment, 6, new BetweenSubjectsBlockingStrategy()));
                        startNewBlock();
                    }
                })
                .show();

    }
//    private void startPractice() {
//        PracticeSession practice  = new PracticeSession(participantID);
//        practice.testAfterEachBlock = false;
//        practice.setGestureStore(experiment.gestureStore);
//        practice.addBlock(new NoFFExperimentBlock(practice, 2 , new FixedConditionBlockingStrategy(Mode.NO_FF)));
//        experiment = practice;
////        experiment.setParticipant(participantID);
//        startNewBlock();
//    }

    public void restartExperiment() {
        experiment = new ExperimentSessionReclycled(experiment);
        startNewBlock();
    }

    public void startNewBlock() {
        try {
            experiment.startNewBlock(this);
            //popAlertNewGestureCreationBlock();
        } catch (TrainingSessionOverException e) {
            new AlertDialog.Builder(this)
                    .setTitle("TRAINING COMPLETE!")
                    .setMessage("Well done!")
                    .setCancelable(false)
                    .setPositiveButton("START EXPERIMENT", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if(mainExperiment==null)
                                startExperiment();
                            else
                                startSecondExperiment();
                        }
                    })
                    .show();

        }
        catch (NoMoreExperimentBlocksException e) {
            final AlertDialog.Builder justInCase = new AlertDialog.Builder(this)
                    .setTitle("WARNING!")
                    .setMessage("Are you the experimenter? You shouldn't start PART 2 until the experimenter says so.")
                    .setCancelable(false)
                    .setPositiveButton("I AM CARLA", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startSecondTraining();
                        }
                    });
            if(mainExperiment == null)
                new AlertDialog.Builder(this)
                    .setTitle("EXPERIMENT PART 1 COMPLETE!")
                    .setMessage("Call the experimenter to start answering the questionnaire")
                    .setCancelable(false)
                    .setPositiveButton("START PART 2", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            justInCase.show();
                        }
                    })
                    .show();
            else
                new AlertDialog.Builder(this)
                        .setTitle("EXPERIMENT PART 2 COMPLETE!")
                        .setMessage("Call the experimenter to start answering the questionnaire")
                        .setCancelable(false)
//                        .setPositiveButton("START PART 2", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                justInCase.show();
//                            }
//                        })
                        .show();

        } catch (PracticeSessionOverException e) {
            new AlertDialog.Builder(this)
                    .setTitle("PRACTICE COMPLETE!")
                    .setMessage("Take a break if you want, and continue to the experiment.")
                    .setCancelable(false)
                    .setPositiveButton("START EXPERIMENT", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startExperiment();
                        }
                    })
                    .show();

        }

    }
//    public void bufferGesture(GestureRegistrationTask gestureRegistrationTask) {
//        Gesture g = new Gesture();
//        float[] pts = Arrays.copyOf(mStroke, mStrokeSize);
//        try {
//            g.addStroke(mGestureStroke_constructor.newInstance(mStrokeBox, mStrokeLength, pts, Arrays.copyOf(mStrokeStamps, mStrokeSize / 2)));
//            gestureRegistrationTask.addGesture(experiment.getCurrentTask().command.label, g);
//            //Log.i("BufferedGesture", experiment.getCurrentTask().command.label);
//
//            //TODO: registration counter ++
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//    }

    public void showBoxesForReview() {
        this.showPlainCanvas();
        gestureCanvasFrame.setReviewFeedback();

    }

    public void showBoxesForRegistration() {
        this.showPlainCanvas();
        //this.showFFCanvas();
        gestureCanvasFrame.setRegistrationFeedback();
    }


    public Gesture gestureFromFFCanvas() {
        return gestureCanvasFrame.gestureCanvas.drawnGesture();
    }

    public Gesture gestureFromCanvas() {
        return gestureCanvasFrame.gestureCanvas.drawnGesture();
    }

    public void takeScreenshot() {
        Date now = new Date();
        CharSequence date = DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = experiment.fileLocation() + "/P"+participantID+"-B"+experiment.blockNumber+"-T"+(experiment.currentBlock.trialNumber+1)+"-"+experiment.currentTask.command.commandCode()+"-"+experiment.currentTask.taskNameForLogger()+"-A"+experiment.currentTask.numberOfGesturesTried+"-"+date + ".jpg";


            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            View v1 = this.gestureCanvasFrame.gestureCanvas;
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }

    public static Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(1080 , 1950, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(b);
        v.layout(0, 0, v.getLayoutParams().width, v.getLayoutParams().height);
        v.draw(c);
        return b;
    }

}
