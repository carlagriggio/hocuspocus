package traf.gesturefeedforward;

import android.gesture.Gesture;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by carlagriggio on 11/16/15.
 */
public class GestureRegistrationTask extends GestureGenerationTask {
    protected int gestureExampleNumber;
    //private ArrayList<Gesture> gestureBuffer = new ArrayList<Gesture>();
    protected static HashMap GestureBuffer = new HashMap<String,ArrayList<Gesture>>();

    public GestureRegistrationTask(int trialNumber, CommandLabel command, Block block) {
        super(trialNumber, command, block);
        gestureExampleNumber = 1;
        GestureBuffer.put(command.label,new ArrayList<Gesture>());
    }
    public GestureRegistrationTask(int trialNumber, CommandLabel command, Block block, int aGestureExampleNumber) {
        super(trialNumber, command, block);
        gestureExampleNumber = aGestureExampleNumber;
    }
    public void initTask(MainActivity mainActivity) {
        mainActivity.setCommandLabelAndInstructions(command.label, command.instruction);
        mainActivity.setLabelOfNextTaskButton(this.textForNextTaskButton());
        if(gestureExampleNumber == 1)
            mainActivity.showFFCanvas();
        else
            mainActivity.showBoxesForRegistration();
        mainActivity.nextTrialButton.setEnabled(false);
        mainActivity.showBackButton();

        //mainActivity.showBackButton();
    }
    public String textForNextTaskButton() {
        return "REGISTER "+String.valueOf(gestureExampleNumber)+"/3";
    }
    public boolean shouldRegisterGestures() {
        return true;
    }

    public void gestureFinished(GestureCanvas aCanvas) {
        super.gestureFinished(aCanvas);
        if(aCanvas.gestureDistance >= GesturePerformanceFeedback.OkThreashold || aCanvas.gestureDistance == 0.0) {
            aCanvas.mainActivity.nextTrialButton.setEnabled(true);
        } else {
            aCanvas.mainActivity.nextTrialButton.setEnabled(false);
        }
    }
    public void finishTask(MainActivity mainActivity) {
        this.end();
        ArrayList<Gesture> buffer = (ArrayList<Gesture>) GestureBuffer.get(command.label);
        buffer.add(mainActivity.gestureFromCanvas());
        if(gestureExampleNumber == 3) {
            //TODO: start over the 3 examples and discard the buffer
            for(Gesture g : buffer) {
                try {
                    block.experiment.getGestureStore().addGesture(command.label, g);
                } catch(Exception e) {
                    //TODO: should actually prevent user from leaving blank canvas?
                    Log.i("Blank Canvas: ", command.label);
                }
            }
            //finish block and start testing
            block.trialFinished(mainActivity);
        } else {
            GestureRegistrationTask nextTask = new GestureRegistrationTask(this.trialNumber,this.command,this.block, gestureExampleNumber +1);
            mainActivity.startNextTask(nextTask);
            nextTask.setPreviousTask(this);
        }
        //mainActivity.hideBoxes();

    }
    @Override
    public void skip(MainActivity mainActivity) {
        this.end();
        ArrayList<Gesture> buffer = (ArrayList<Gesture>) GestureBuffer.get(command.label);
        buffer.add(mainActivity.gestureFromCanvas());
        for(Gesture g : buffer) {
            try {
                block.experiment.getGestureStore().addGesture(command.label, g);
            } catch(Exception e) {
                //TODO: should actually prevent user from leaving blank canvas?
                Log.i("Blank Canvas: ", command.label);
            }
        }
        super.skip(mainActivity);
    }
    protected String taskNameForLogger() {
        return "R"+String.valueOf(gestureExampleNumber);
    }
}
