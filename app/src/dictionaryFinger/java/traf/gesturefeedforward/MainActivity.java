package traf.gesturefeedforward;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.gesture.Gesture;
import android.gesture.GestureStore;
import android.gesture.GestureStroke;
import android.gesture.Prediction;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.text.InputType;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.view.ViewGroup.LayoutParams;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.Random;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


public class MainActivity extends Activity implements DialogInterface.OnClickListener, View.OnClickListener {

    // TODO adjust size and atom count for Path-based feedforward
    // TODO evaluate > 4 paths, display top 3 and bottom 1

    Mode mode = Mode.OCTOPOCUS_FF;
    static final Random sRand = new Random();
    static final long[] sTimestamps = new long[0];
    EditText mParticipantInput;
    TextView commandName;
    TextView currentScore;

    Button saveGesture;
    Button nextCommand;
    int participantID = 0;
    ExperimentSession experiment;
    EvaluationSession evaluation;
    File mDirectory;
    int mGestureNum = 0;
    SimpleDateFormat mDatePattern = new SimpleDateFormat("yyyyMMdd_HHmmss");
    long[] mStrokeStamps = new long[6];
    GestureGenerationTask currentTrial;
    int mVec, mProgram; // Has to be a global handle for OpenGL to work!
    FloatBuffer mVerts;
    ByteBuffer mIndices0, mIndices1;

    static float hypot(float a, float b) {
        if (android.os.Build.VERSION.SDK_INT >= 17)
            return FloatMath.hypot(a, b);
        else
            return FloatMath.sqrt(a * a + b * b);
    }

    // The current stroke is stored as a raw array of points for ease of prototyping
    int mStrokeSize;
    float[] mStroke = new float[12];
    float mStrokeLength;
    RectF mStrokeBox = new RectF();
    Path mStrokePath = new Path();
    Paint mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    int mScore;
    int userScore;
    String mScoreName;
    double velx;
    double vely;
    float mLastStrokeLength;
    double angle;
    RectF mBox;

    @Override
    // OnClickListener interface for saving the current gesture when user presses Button.
    public void onClick(View v) {
        Gesture g = new Gesture();
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize);
        try {
            g.addStroke(mGestureStroke_constructor.newInstance(mStrokeBox, mStrokeLength, pts, Arrays.copyOf(mStrokeStamps, mStrokeSize / 2)));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        //String s = "Gesture " + Integer.toString(mGestureNum++);

        mStore.addGesture(currentTrial.command.label, g);
        //Toast.makeText(this, s + " added!", Toast.LENGTH_SHORT).show();

        if (mGestureNum == 6) try {
            if (!mDirectory.exists())
                mDirectory.mkdirs();
            mStore.save(new FileOutputStream(new File(mDirectory, "gesture-"+mDatePattern.format(new Date()))), true);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //close the trial and start a new one
        showAchievementDialog();
    }

    class Command {
        String label;
        float[] points;
        Path path = new Path();
        float width, labelX, labelY;
        int color;
        float length;
        RectF box;
        Command(String l, float[] p) {
            label = l;
            points = p;
            color = 0x80000000 | sRand.nextInt(0x01000000);
        }
        Command(String l, int numAtoms, int[] atoms, int[] signs, double[] angles, float[] scales) {
            label = l;
            int numPoints = (numAtomSamples - 1) * numAtoms + 1;
            points = new float[numPoints * 2];
            float offsetX = points[0] = 0;
            float offsetY = points[1] = 0;

            float cosTheta;
            float sinTheta;
            int index = 2;
            int i, j;
            box = new RectF(0, 0, 0, 0);

            double lastAngle = 0;

            // Concatenate the atoms
            for (i = 0; i < numAtoms; i++) {
                cosTheta = (float) Math.cos(angles[i] + lastAngle);
                sinTheta = (float) Math.sin(angles[i] + lastAngle);
                for (j = 2; j < numAtomSamples * 2; j += 2, index += 2) {
                    points[index] = mAtoms[atoms[i]][j] * scales[i] * cosTheta - mAtoms[atoms[i]][j + 1] * scales[i] * sinTheta * signs[i] + offsetX;
                    points[index + 1] = mAtoms[atoms[i]][j] * scales[i] * sinTheta + mAtoms[atoms[i]][j + 1] * scales[i] * cosTheta * signs[i] + offsetY;
                    box.union(points[index], points[index+1]);
                }

                lastAngle = Math.atan2(points[index - 1] - points[index - 3], points[index - 2] - points[index - 4]);
                offsetX = points[index - 2];
                offsetY = points[index - 1];
            }

            // recalulate the total length from the points since some atoms may have been scaled
            length = 0;
            for (i = 2; i < numPoints * 2; i += 2) {
                length += hypot(points[i] - points[i - 2], points[i + 1] - points[i - 1]);
            }
        }
    }
    Command[] mCommands;

    // Class for keeping track of the feedforward gesture candidates
    class Candidate {
        float[] points;
        float[] lengths;
        String label;
        Path opaquePath;
        Path translucentPath;
        float width;
        float startLength;
        int index;
        int score;
        float labelX, labelY;
        boolean active;
        Candidate(String _label, float[] _points, float[] _lengths) {
            label = _label;
            points = _points;
            lengths = _lengths;
            opaquePath = new Path();
            translucentPath = new Path();
            width = 0;
            index = 0;
            startLength = mStrokeLength;
            active = true;
        }
    }
    LinkedList<Candidate> mCandidates = new LinkedList<>();
    float mLastEventX, mLastEventY;
    boolean mFeedForward = false;
    Handler mHandler = new Handler();
    float mNextLength;
    Paint mPaint;

    // Resources needed for matching the stroke to existing templates
    File mGesturesFile = new File(Environment.getExternalStorageDirectory(), "gestures");
    GestureStore mStore = new GestureStore();
    Constructor<GestureStroke> mGestureStroke_constructor;
    int numAtomSamples;
    float[][] mAtoms;
    float[][] mAtomLengths;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GLSurfaceView gl = new GLSurfaceView(this);
        gl.setEGLContextClientVersion(2);
        gl.setEGLConfigChooser(8, 8, 8, 8, 0, 0);
        gl.setRenderer(new DrawRenderer());

        Session.getInstance().openConnection();

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        currentScore = new TextView(this);
        currentScore.setText("");
        currentScore.setTextSize(40.0f);
        currentScore.setY(10);
        //commandName.setId(1);
        currentScore.setGravity(Gravity.CENTER);
        currentScore.setPadding(0, 20, 20, 0);

        //create a text view
        commandName = new TextView(this);
        commandName.setText("");
        commandName.setTextSize(50.0f);
        //commandName.setId(1);
        commandName.setGravity(Gravity.CENTER);
        commandName.setY(150);
        //adds the textview
        //layout.addView(commandName);

        //create a button
        saveGesture = new Button(this);
        saveGesture.setOnClickListener(this);
        saveGesture.setVisibility(View.GONE);
        //b.setId(2);
        saveGesture.setText("Save Gesture");
        saveGesture.setTextSize(30);
        saveGesture.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

        //create a button
        nextCommand = new Button(this);
        nextCommand.setTextSize(30);
        nextCommand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             nextCommandButtonPressed();
            }


        });
        //next.setVisibility(View.VISIBLE);
        nextCommand.setText("NEXT COMMAND");
        nextCommand.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));


        // add the button
        LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);

        addContentView(gl, new LayoutParams(-1, -1));
        addContentView(new DrawView(this), new LayoutParams(-1, -1));
        addContentView(saveGesture, new LayoutParams(-2, -2));
        addContentView(nextCommand, new LayoutParams(-2, -2));

        addContentView(commandName, layoutParam);
        addContentView(currentScore, layoutParam);

        mParticipantInput = new EditText(this);
        mParticipantInput.setHint("Participant number");
        mParticipantInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setView(mParticipantInput)
                .setCancelable(false)
                .setPositiveButton("OK", this)
                .show();

        // Load the initial set of templates
        // TODO load all 3 gestureSets here
        try {
            mStore.load(mGesturesFile.canRead()
                        ? new FileInputStream(mGesturesFile)
                        : getResources().openRawResource(R.raw.gestures), true);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        //Toast.makeText(this, mStore.getGestureEntries().size() + " gestures loaded from " + (mGesturesFile.canRead() ? mGesturesFile.getPath() : "resource file"), Toast.LENGTH_SHORT).show();

        // Mandatory hack to make a GestureStroke out of an array of points.
        try {
            mGestureStroke_constructor = GestureStroke.class.getDeclaredConstructor(RectF.class, float.class, float[].class, long[].class);
            mGestureStroke_constructor.setAccessible(true);
        }
        catch (NoSuchMethodException e) {
            System.out.println(e.getMessage());
        }

//        mCommands = new Command[mStore.getGestureEntries().size()];
//        int i = 0;
//        for (String s : mStore.getGestureEntries())
//            mCommands[i++] = new Command(s, mStore.getGestures(s).get(0).getStrokes().get(0).points);

        mStrokePaint.setStrokeCap(Paint.Cap.ROUND);
        mStrokePaint.setStrokeJoin(Paint.Join.BEVEL);
        mStrokePaint.setStrokeWidth(20);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mPaint = new Paint(mStrokePaint);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTextSize(48);
        mBox = new RectF();

// Stuff for Path-Based Feedforward
        float scale = 250;

        // create some atoms for composing new potential gestures
        numAtomSamples = 20;
        mAtoms = new float[6][numAtomSamples * 2];
        mAtomLengths = new float[6][numAtomSamples];
        float norm = 1f / ((float)numAtomSamples * 2f);

        // first atom is just a straight line
        mAtomLengths[0][0] = 0;
        int i, j;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[0][i] = i * norm * scale;
            mAtoms[0][i+1] = 0f * scale;
            if (i >= 2)
                mAtomLengths[0][j] = mAtomLengths[0][j-1]
                        + hypot(mAtoms[0][i] - mAtoms[0][i-2], mAtoms[0][i+1] - mAtoms[0][i-1]);
        }

        // next atom describes cosine arc from 0 – π/3
        mAtomLengths[1][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[1][i] = FloatMath.sin(i * norm * (float)Math.PI * 0.33f) * scale;
            mAtoms[1][i+1] = (1f - FloatMath.cos(i * norm * (float)Math.PI * 0.33f)) * scale;
            if (i >= 2)
                mAtomLengths[1][j] = mAtomLengths[1][j-1]
                        + hypot(mAtoms[1][i] - mAtoms[1][i-2], mAtoms[1][i+1] - mAtoms[1][i-1]);
        }

        // next atom describes cosine arc from 0 – π/2
        mAtomLengths[2][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[2][i] = FloatMath.sin(i * norm * (float)Math.PI * 0.5f) * scale * 0.75f;
            mAtoms[2][i+1] = (1f - FloatMath.cos(i * norm * (float)Math.PI * 0.5f)) * scale * 0.75f;
            if (i >= 2)
                mAtomLengths[2][j] = mAtomLengths[2][j-1]
                        + hypot(mAtoms[2][i] - mAtoms[2][i-2], mAtoms[2][i+1] - mAtoms[2][i-1]);
        }

        // next atom describes arc from 0 – 2π/3
        mAtomLengths[3][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[3][i] = FloatMath.sin(i * norm * (float)Math.PI) * scale * 0.67f;
            mAtoms[3][i+1] = (1f - FloatMath.cos(i * norm * (float)Math.PI)) * scale * 0.67f;
            if (i >= 2)
                mAtomLengths[3][j] = mAtomLengths[3][j-1]
                        + hypot(mAtoms[3][i] - mAtoms[3][i-2], mAtoms[3][i+1] - mAtoms[3][i-1]);
        }

        // next atom describes arc from 0 – 4π/3
        mAtomLengths[4][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[4][i] = FloatMath.sin(i * norm * (float)Math.PI * 1.333f) * scale * 0.5f;
            mAtoms[4][i+1] = (1f - FloatMath.cos(i * norm * (float)Math.PI * 1.333f)) * scale * 0.5f;
            if (i >= 2)
                mAtomLengths[4][j] = mAtomLengths[4][j-1]
                        + hypot(mAtoms[4][i] - mAtoms[4][i-2], mAtoms[4][i+1] - mAtoms[4][i-1]);
        }

        // next atom describes arc from 0 – 5π/3
        mAtomLengths[5][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[5][i] = FloatMath.sin(i * norm * (float)Math.PI * 1.667f) * scale * 0.5f;
            mAtoms[5][i+1] = (1f - FloatMath.cos(i * norm * (float)Math.PI * 1.667f)) * scale * 0.5f;
            if (i >= 2)
                mAtomLengths[5][j] = mAtomLengths[5][j-1]
                        + hypot(mAtoms[5][i] - mAtoms[5][i-2], mAtoms[5][i+1] - mAtoms[5][i-1]);
        }

        // build the commands
//        buildGestureSet(experiment.getGestureSetNumber());
        //int gestureSetIndex = 0;


// Stuff for Field-based feedforward
        // Above 16/10, the renderer has visual artefacts.
        final int resX = 13, resY = 8;
        final float incX = 2560f / resX;
        final float incY = 1550f / resY;
        mVerts = ByteBuffer.allocateDirect((resX + 1) * (resY + 1) * 16).order(ByteOrder.nativeOrder()).asFloatBuffer();
        int index;
        for (i = 0, index = 0; i <= resY; i++) {
            for (j = 0; j <= resX; j++) {
                mVerts.put(index++, j * incX);
                mVerts.put(index++, i * incY);
                mVerts.put(index++, 0);
                mVerts.put(index++, 0);
            }
        }

        // Create two sets of triangles that will display the grid as shades.
        mIndices0 = ByteBuffer.allocateDirect(resX * resY * 6);
        mIndices1 = ByteBuffer.allocateDirect(resX * resY * 6);
        int index0, index1;
        for (i = 0, index0 = 0, index1 = 0; i < resY; i++) {
            for (j = 0; j < resX; j++) {
                byte topLeft = (byte)(i * (resX + 1) + j);
                byte topRight = (byte)(topLeft + 1);
                byte bottomLeft = (byte)(topLeft + resX + 1);
                byte bottomRight = (byte)(bottomLeft + 1);
                mIndices0.put(index0++, topLeft);
                mIndices0.put(index0++, bottomLeft);
                mIndices0.put(index0++, topRight);
                mIndices0.put(index0++, topRight);
                mIndices0.put(index0++, bottomLeft);
                mIndices0.put(index0++, bottomRight);
                mIndices1.put(index1++, bottomLeft);
                mIndices1.put(index1++, bottomRight);
                mIndices1.put(index1++, topLeft);
                mIndices1.put(index1++, topLeft);
                mIndices1.put(index1++, bottomRight);
                mIndices1.put(index1++, topRight);
            }
        }

        if (!Environment.getExternalStorageState().equals("mounted"))
            Toast.makeText(this, "SD card not detected", Toast.LENGTH_SHORT).show();

    }


    // OnClickListener interface for receiving the participant number.
    public void onClick(DialogInterface dialog, int which) {
        participantID = Integer.parseInt(mParticipantInput.getText().toString());
        startExperiment();
        buildGestureSet(experiment.getGestureSetNumber());
    }

    void buildCommands(int gestureSetIndex) {
        mCommands = new Command[6];
        int i = 0;

        switch (gestureSetIndex) {
            case 0: // Gesture Set 1
                // Cut (1 stroke)
                mCommands[i++] = new Command("cut", 4, new int[]{1, 4, 1, 0},
                        new int[]{-1, -1, -1, 1}, new double[]{Math.PI * -0.67, 0, 0, 0},
                        new float[]{1, 0.5f, 1, 0.5f});

                // Stock report (1 stroke)
                mCommands[i++] = new Command("stock report", 3, new int[]{2, 5, 2},
                        new int[]{-1, -1, -1}, new double[]{Math.PI * -1.75, 0, 0},
                        new float[]{1.2f, 0.3f, 1.2f});

                // camera on (2 strokes)
                mCommands[i++] = new Command("camera on", 2, new int[]{0, 0}, new int[]{1, 1},
                        new double[]{Math.PI * -0.25, Math.PI * 0.5}, new float[]{1f, 1.5f});

                // alarm set (3 strokes)
                mCommands[i++] = new Command("alarm set", 3, new int[]{0, 0, 0},
                        new int[]{1, 1, 1}, new double[]{Math.PI * 0.25, Math.PI * 0.5, Math.PI * 0.5},
                        new float[]{1f, 1f, 1f});

                // flashlight on (3 strokes)
                mCommands[i++] = new Command("flashlight on", 3, new int[]{0, 0, 0},
                        new int[]{1, 1, 1},
                        new double[]{Math.PI * 0.75, Math.PI * -0.85, Math.PI * -0.7},
                        new float[]{1.5f, 1f, 1f});

                // airplane mode on (4 strokes)
                mCommands[i++] = new Command("airplane mode on", 4, new int[]{0, 0, 0, 0},
                        new int[]{1, 1, 1, 1},
                        new double[]{Math.PI * -0.5, Math.PI * 0.5, Math.PI * 0.5, Math.PI * 0.5},
                        new float[]{1.8f, 0.75f, 0.75f, 1.5f});

                break;
            case 1: // Gesture Set 2
                // energy save on (1 stroke)
                mCommands[i++] = new Command("energy save on", 3, new int[]{4, 1, 0},
                        new int[]{1, -1, 1}, new double[]{Math.PI * -0.42, 0, 0},
                        new float[]{1, 0.5f, 0.5f});

                // timer set (1 stroke)
                mCommands[i++] = new Command("timer Set", 3, new int[]{3, 3, 3},
                        new int[]{-1, -1, -1}, new double[]{Math.PI * -0.5, 0, 0},
                        new float[]{1, 1, 1});

                // weather report (2 strokes)
                mCommands[i++] = new Command("weather report", 2, new int[]{0, 0}, new int[]{1, 1},
                        new double[]{Math.PI, Math.PI * -0.5}, new float[]{1f, 1.5f});

                // paste (3 strokes)
                mCommands[i++] = new Command("paste", 3, new int[]{0, 0, 0}, new int[]{1, 1, 1},
                        new double[]{Math.PI * -0.5, Math.PI * -0.5, Math.PI * -0.5},
                        new float[]{0.75f, 0.75f, 1.5f});

                // video record on (3 strokes)
                mCommands[i++] = new Command("video record on", 3, new int[]{0, 0, 0},
                        new int[]{1, 1, 1}, new double[]{Math.PI * 0.4, Math.PI * -0.8, Math.PI * 0.4},
                        new float[]{0.5f, 0.5f, 1.5f});

                // geolocation on (4 strokes)
                mCommands[i++] = new Command("geolocation on", 4, new int[]{0, 0, 0, 0},
                        new int[]{1, 1, 1, 1},
                        new double[]{Math.PI * -0.25, Math.PI * 0.5, Math.PI * -0.75, Math.PI * 0.6},
                        new float[]{1.5f, 0.5f, 0.5f, 0.5f});

                break;
            case 2: // Gesture Set 3
                // screen bright (1 stroke)
                mCommands[i++] = new Command("screen bright", 5, new int[]{0, 3, 0, 3, 0},
                        new int[]{1, 1, 1, -1, -1}, new double[]{Math.PI * -0.25, 0, 0, 0, 0},
                        new float[]{0.8f, 0.15f, 0.3f, 0.15f, 1.2f});

                // bluetooth on (1 stroke)
                mCommands[i++] = new Command("bluetooth on", 3, new int[]{1, 3, 1},
                        new int[]{-1, -1, -1}, new double[]{Math.PI * -1.24, 0, 0},
                        new float[]{1.5f, 0.8f, 1.5f});

                // search (2 strokes)
                mCommands[i++] = new Command("search", 3, new int[]{2, 2, 0},
                        new int[]{1, 1, 1}, new double[]{Math.PI * 0.04, Math.PI, 0},
                        new float[]{1.5f, 1.5f, 0.5f});

                // audio record on (3 strokes)
                mCommands[i++] = new Command("audio record on", 3, new int[]{0, 0, 0},
                        new int[]{1, 1, 1},
                        new double[]{Math.PI * -0.75, Math.PI * -0.75, Math.PI * -0.75},
                        new float[]{1.5f, 1f, 1.5f});

                // clock set (3 strokes)
                mCommands[i++] = new Command("clock set", 3, new int[]{0, 0, 0},
                        new int[]{1, 1, 1},
                        new double[]{Math.PI * 0.4, Math.PI * 0.8, Math.PI * -0.8},
                        new float[]{1f, 0.5f, 1.5f});

                // news report (4 strokes)
                mCommands[i++] = new Command("news report", 4, new int[]{0, 0, 0, 0},
                        new int[]{1, 1, 1, 1},
                        new double[]{Math.PI * -0.5, Math.PI * -0.8, Math.PI * -0.7, Math.PI * 0.7},
                        new float[]{1.8f, 1f, 0.5f, 1f});

                break;
        }
    }

    void buildGestureSet(int gestureSetIndex) {
        mGesturesFile = new File(Environment.getExternalStorageDirectory(), "gestures");
        mStore = new GestureStore();
        buildCommands(gestureSetIndex-1);
        int counter = 0;
        for (Command c : mCommands) {
            Gesture g = new Gesture();
            try {
                g.addStroke(mGestureStroke_constructor.newInstance(c.box, c.length, c.points, new long[0]));
                mStore.addGesture(c.label, g);
                //Toast.makeText(this, "Created gesture for command "+counter+" '" + c.label + "'", Toast.LENGTH_SHORT).show();
            }
            catch (Exception e) {
                Toast.makeText(this, "Couldn't create gesture for command "+counter+" '"+c.label+"'", Toast.LENGTH_SHORT).show();
                System.out.println(e.getMessage());
            }
            ++counter;
        }
        java.util.Set<String> foo = mStore.getGestureEntries();
        for (String s : foo) {
            //Toast.makeText(this, "Gesture '"+s+"' is in mStore", Toast.LENGTH_SHORT).show();
        }

        File GestureSets = new File(Environment.getExternalStorageDirectory(), "GestureSets");
        try {
            if (!GestureSets.exists())
                GestureSets.mkdirs();
            mStore.save(new FileOutputStream(new File(GestureSets, "Gesture_Set_" + gestureSetIndex), true));
            //Toast.makeText(this, "Stored file GestureSet_"+gestureSetIndex, Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            System.out.println("Failed to save file: " + e.getMessage());
            //Toast.makeText(this, "Did not store file GestureSet_"+gestureSetIndex+": "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    void updateOctopocus() {
        for (Command c : mCommands) {
            c.path.rewind();
            c.width = 0;
            int pos = 0;
            float length = 0;
            while (pos < c.points.length - 2 && length < mStrokeLength) {
                length += hypot(c.points[pos + 2] - c.points[pos], c.points[pos + 3] - c.points[pos + 1]);
                pos += 2;
            }
            if (pos >= c.points.length - 2)
                continue;

            // Create a composite stroke at the join point.
            c.path.moveTo(mStroke[mStrokeSize - 2], mStroke[mStrokeSize - 1]);
            mBox.set(mStrokeBox);
            float[] pts = Arrays.copyOf(mStroke, mStrokeSize + c.points.length - pos - 2);
            float dx = c.points[pos++] - mStroke[mStrokeSize - 2];
            float dy = c.points[pos++] - mStroke[mStrokeSize - 1];
            float len = 0;
            int dst = mStrokeSize;
            while (pos < c.points.length) {
                float X = pts[dst++] = c.points[pos++] - dx;
                float Y = pts[dst++] = c.points[pos++] - dy;
                mBox.union(X, Y);
                len += hypot(X - pts[dst - 4], Y - pts[dst - 3]);
                if (len < 400)
                    c.path.lineTo(c.labelX = X, c.labelY = Y);
            }

            // Match the gesture with the existing commands
            Gesture g = new Gesture();
            try {
                g.addStroke(mGestureStroke_constructor.newInstance(mBox, mStrokeLength + len, pts, sTimestamps));
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
            double score = mStore.recognize(g).get(0).score;
            c.width = Double.isNaN(score) ? 32 : Math.min((float)score * 2, 32);
        }
    }

    void appendPathCandidates(int numCandidates, int numAtomsPer) {
        Candidate[] candidates = new Candidate[numCandidates];
        for (int c = 0; c < candidates.length; c++) {

            int[] atoms = new int[numAtomsPer];
            int[] signs = new int[numAtomsPer];
            double[] angles = new double[numAtomsPer];

            // Generate a random gesture.
            for (int i = 0; i < numAtomsPer; i++) {
                atoms[i] = sRand.nextInt(mAtoms.length);
                signs[i] = sRand.nextInt(2) == 1 ? 1 : -1;
            }

            int numPoints = (numAtomSamples - 1) * numAtomsPer + 1;
            float[] points = new float[numPoints * 2];
            float[] lengths = new float [numPoints];
            float offsetX = points[0] = 0;
            float offsetY = points[1] = 0;
            lengths[0] = 0;
            if (mStrokeSize > 10)
                angles[0] = Math.atan2(vely, velx);
            else
                angles[0] = Math.PI * 2 / numCandidates * c;
            float offsetLength = 0;
            float cosTheta;
            float sinTheta;
            int index = 2;
            int j, m, n = 1;

            // Append the atoms to the candidate
            for (int i = 0; i < numAtomsPer; i++) {
                cosTheta = (float)Math.cos(angles[i]);
                sinTheta = (float)Math.sin(angles[i]);
                for (j = 2, m = 1; j < numAtomSamples * 2; j += 2, m++, n++, index += 2) {
                    points[index] = mAtoms[atoms[i]][j] * cosTheta - mAtoms[atoms[i]][j+1] * sinTheta * signs[i] + offsetX;
                    points[index+1] = mAtoms[atoms[i]][j] * sinTheta + mAtoms[atoms[i]][j+1] * cosTheta * signs[i] + offsetY;
                    lengths[n] = mAtomLengths[atoms[i]][m] + offsetLength;
                }
                if (i < numAtomsPer-1) {
                    angles[i+1] = angle = Math.atan2(points[index-1] - points[index-3], points[index-2] - points[index-4]);
                }
                offsetX = points[index-2];
                offsetY = points[index-1];
                offsetLength = lengths[n-1];
            }

            candidates[c] = new Candidate("<Command>", points, lengths);
            mCandidates.add(candidates[c]);
        }
    }

    void updatePathCandidates() {
        float strokeVel = 0;
        if (mStrokeSize > 4)
            strokeVel = hypot((float)velx, (float)vely);
        for (Candidate c : mCandidates) {
            // check if velocities match approximately between atom position and user stroke
            float atomVelx = 0;
            float atomVely = 0;
            if ((c.index > 1) && (c.index < c.lengths.length - 1)) {
                atomVelx = c.points[c.index*2] - c.points[c.index*2-2];
                atomVely = c.points[c.index*2+1] - c.points[c.index*2-1];
            }

            // update atom offset index
            if (c.index < c.lengths.length - 1) {
                float offset = mStrokeLength - c.startLength;
                while (c.lengths[c.index] < offset) {
                    ++c.index;
                    if (c.index >= c.lengths.length - 1)
                        break;
                }
            }

            // TODO if end of atom, append another one
            if (c.index >= c.lengths.length - 1) {
                c.opaquePath.rewind();
                c.translucentPath.rewind();
                c.active = false;
                continue;
            }
            
            // update candidate path
            if (c.opaquePath == null)
                c.opaquePath = new Path();
            else if (!c.opaquePath.isEmpty())
                c.opaquePath.rewind();
            if (c.translucentPath == null)
                c.translucentPath = new Path();
            else if (!c.translucentPath.isEmpty())
                c.translucentPath.rewind();
            mBox.set(mStrokeBox);
            int pos = c.index * 2;
            float[] pts = Arrays.copyOf(mStroke, mStrokeSize + c.points.length - c.index * 2 - 2);
            float offsetx = mStroke[mStrokeSize - 2] - c.points[pos++];
            float offsety = mStroke[mStrokeSize - 1] - c.points[pos++];
            int dst = mStrokeSize;
            c.opaquePath.moveTo(mStroke[mStrokeSize - 2], mStroke[mStrokeSize - 1]);
            c.translucentPath.moveTo(mStroke[mStrokeSize - 2], mStroke[mStrokeSize - 1]);
            float len = 0;
            while (pos < c.points.length) {
                float X = pts[dst++] = c.points[pos++] + offsetx;
                float Y = pts[dst++] = c.points[pos++] + offsety;
                mBox.union(X, Y);
                len += hypot(X - pts[dst - 4], Y - pts[dst - 3]);
                if (len < 300) {
                    mBox.union(X, Y);
                    c.opaquePath.lineTo(c.labelX = X, c.labelY = Y);
                }
                c.translucentPath.lineTo(X, Y);
            }

            Gesture g = new Gesture();
            try {
                float remainingLength = c.lengths[c.lengths.length-1] - c.lengths[c.index];
                g.addStroke(mGestureStroke_constructor.newInstance(mBox,
                                                                   mStrokeLength + remainingLength, pts, new long[0]));
                double score = mStore.recognize(g).get(0).score;
                c.width = Double.isNaN(score) ? 20 : Math.min(Math.max((float)score * 10, 1), 40);
                c.score = Math.min((int)score * 32, 127);
            }
            catch (Exception e) {
                System.out.println("3: "+e.getMessage());
                c.width = 0;
                c.opaquePath = null;
                c.translucentPath = null;
            }
        }
    }

    void updateStroke() {
        // check score for current stroke
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize - 1);
        Gesture g = new Gesture();
        try {
            g.addStroke(mGestureStroke_constructor.newInstance(mStrokeBox, mStrokeLength, pts, new long[0]));
            double score = mStore.recognize(g).get(0).score;
            mScore = Math.min((int)score * 32, 127);
            userScore = Math.min((int)score * 25, 100);
            mScoreName = mStore.recognize(g).get(0).name;
            for(Prediction prediction: mStore.recognize(g)) {
                currentTrial.addLastAttempt(prediction.name,prediction.score);
            }
        }
        catch (Exception e) {
            System.out.println("4: "+e.getMessage());
            mScore = 0;
        }
    }

    void updateField() {
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize + 2);
        for (int i = 0; i < mVerts.limit() / 4; i++) {
            pts[mStrokeSize] = mVerts.get(4 * i);
            pts[mStrokeSize + 1] = mVerts.get(4 * i + 1);
            mBox.set(mStrokeBox);
            mBox.union(pts[mStrokeSize], pts[mStrokeSize + 1]);
            float len = hypot(pts[mStrokeSize] - pts[mStrokeSize - 2],
                    pts[mStrokeSize + 1] - pts[mStrokeSize - 1]);

            Gesture g = new Gesture();
            try {
                g.addStroke(mGestureStroke_constructor.newInstance(mBox, mStrokeLength + len, pts, new long[0]));
                Prediction p = mStore.recognize(g).get(0);
                mVerts.put(4 * i + 2, Math.min((float) p.score / 4, 1));
            } catch (Exception e) {
                System.out.println("5: "+e.getMessage());
            }
        }
    }

    class DrawRenderer implements GLSurfaceView.Renderer {
        public void onDrawFrame(GL10 glUnused) {
            mIndices0.position(0);
            mIndices1.position(0);
            GLES20.glDisable(GLES20.GL_BLEND);
            if (mode == Mode.FIELD_FF && mFeedForward) {
                GLES20.glDrawElements(GLES20.GL_TRIANGLES, mIndices0.limit(), GLES20.GL_UNSIGNED_BYTE, mIndices0);
                GLES20.glEnable(GLES20.GL_BLEND);
                GLES20.glDrawElements(GLES20.GL_TRIANGLES, mIndices1.limit(), GLES20.GL_UNSIGNED_BYTE, mIndices1);
            }
            else {
                GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
            }
        }

        public void onSurfaceChanged(GL10 glUnused, int width, int height) {
            GLES20.glViewport(0, 0, width, height);
        }

        public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
            mProgram = GLES20.glCreateProgram();
            int vshader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
            int fshader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
            GLES20.glShaderSource(vshader, "attribute vec4 aVec; varying lowp vec4 vCol; void main() { gl_Position = vec4(aVec.x/1280.0-1.0,1.0-aVec.y/775.0,0,1); vCol = vec4(aVec.z,0.5-aVec.z*0.5,1.0-aVec.z,0.5); }");
            GLES20.glShaderSource(fshader, "varying lowp vec4 vCol; void main() { gl_FragColor = vCol; }");
            GLES20.glCompileShader(vshader);
            GLES20.glCompileShader(fshader);
            GLES20.glAttachShader(mProgram, vshader);
            GLES20.glAttachShader(mProgram, fshader);
            GLES20.glLinkProgram(mProgram);
            GLES20.glUseProgram(mProgram);
            mVec = GLES20.glGetAttribLocation(mProgram, "aVec");
            GLES20.glEnableVertexAttribArray(mVec);
            GLES20.glVertexAttribPointer(mVec, 4, GLES20.GL_FLOAT, false, 16, mVerts);
            GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
            GLES20.glBlendFunc (GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        }
    }

    class DrawView extends View implements Runnable {
        DrawView(Context context) { super(context); }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    currentTrial.addTriedGesture();
                    currentScore.setText("");


                    mStrokeSize = 4;
                    mStroke[0] = mLastEventX = x;
                    mStroke[1] = mLastEventY = y;
                    mStroke[2] = x + 1;
                    mStroke[3] = y + 1;
                    mLastStrokeLength = mStrokeLength = hypot(1, 1);
                    velx = vely = 0;
                    mStrokeBox.set(x, y, x + 1, y + 1);
                    mStrokePath.rewind();
                    mStrokePath.moveTo(x, y);
                    mStrokePath.lineTo(x + 1, y + 1);
                    mScore = -1;
                    mHandler.postDelayed(this, 500);

                    currentTrial.logGestureFragment("TOUCH_DOWN", x, y);
                    break;
                case MotionEvent.ACTION_MOVE:
                    // check if has moved more than threshold
                    float distMoved = hypot(x - mStroke[mStrokeSize - 2],
                                            y - mStroke[mStrokeSize - 1]);
                    if (distMoved < 5)
                        break;
                    if (mStrokeSize >= 4) {
                        // try smoothing last point
                        // calculate midpoint between current sample and z-2
                        float mpx = (mStroke[mStrokeSize-4] + x) * 0.5f;
                        float mpy = (mStroke[mStrokeSize-3] + y) * 0.5f;

                        // interpolate between midpoint and z-1
                        mStroke[mStrokeSize-2] = mStroke[mStrokeSize-2] * 0.1f + mpx * 0.9f;
                        mStroke[mStrokeSize-1] = mStroke[mStrokeSize-1] * 0.1f + mpy * 0.9f;

                        // remove previous strokelength
                        mStrokeLength -= mLastStrokeLength;

                        // add new previous strokelength
                        mStrokeLength += hypot(mStroke[mStrokeSize-2]
                                               - mStroke[mStrokeSize-4],
                                               mStroke[mStrokeSize-1]
                                               - mStroke[mStrokeSize-3]);

                        // add current strokeLength
                        mLastStrokeLength = hypot(x - mStroke[mStrokeSize-2],
                                                  y - mStroke[mStrokeSize-1]);
                        mStrokeLength += mLastStrokeLength;

                        velx = x - mStroke[mStrokeSize-2];
                        vely = y - mStroke[mStrokeSize-1];
                    }

                    mStrokeBox.union(x, y);
                    mStrokePath.lineTo(x, y);

                    // Reallocate mStroke if necessary
                    if (mStrokeSize + 2 > mStroke.length)
                        mStroke = Arrays.copyOf(mStroke, 2 * mStroke.length);
                    mStroke[mStrokeSize++] = x;
                    mStroke[mStrokeSize++] = y;

                    // Reset the long press when the finger moves far enough
                    if (mFeedForward) {
                        switch (mode) {
                            case OCTOPOCUS_FF:
                                updateOctopocus();
                                break;
                            case PATH_FF:
                                updatePathCandidates();
                                break;
                            case FIELD_FF:
                                updateField();
                                break;
                        }
                    }
                    else if (hypot(x - mLastEventX, y - mLastEventY) > 50) {
                        mHandler.removeCallbacks(this);
                        mHandler.postDelayed(this, 500);
                        mLastEventX = x;
                        mLastEventY = y;
                    }
                    updateStroke();
                    currentScore.setText("Detected: " + String.valueOf(userScore));
                    currentTrial.logGestureFragment("TOUCH_MOVE", x, y);
                    break;
                case MotionEvent.ACTION_UP:
                    mHandler.removeCallbacks(this);
                    mFeedForward = false;
                    mCandidates.clear();

                    if(userScore >=75) currentScore.append(" - " + String.valueOf(mScoreName));
                    else currentScore.append(" - ???");
                    currentTrial.logGestureFragment("TOUCH_UP", x, y);

//                    // testing only: switch mode
//                    switch (mode) {
//                        case NO_FF:
//                            mode = Mode.OCTOPOCUS_FF;
//                            break;
//                        case OCTOPOCUS_FF:
//                            mode = Mode.PATH_FF;
//                            break;
//                        case PATH_FF:
//                            mode = Mode.FIELD_FF;
//                            break;
//                        case FIELD_FF:
//                            mode = Mode.NO_FF;
//                            break;
//                    }

                    break;
            }
            invalidate();
            return true;
        }

        // Runnable interface for implementing the long press
        public void run() {
            if (mode == Mode.NO_FF)
                return;

            ((Vibrator)getSystemService(Context.VIBRATOR_SERVICE)).vibrate(30);
            mFeedForward = true;
            switch (mode) {
                case OCTOPOCUS_FF:
                    updateOctopocus();
                    break;
                case PATH_FF:
                    appendPathCandidates(4, 4);
                    updatePathCandidates();
                    break;
                case FIELD_FF:
                    updateField();
                    break;
            }
            updateStroke();
            invalidate();
        }

        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            if (mode == Mode.NO_FF || mode == Mode.OCTOPOCUS_FF)
                mStrokePaint.setColor(0xff222222);
            else
                mStrokePaint.setColor(mScore < 0 ? 0xff222222 : (mScore * 0x00020102) ^ 0xff007fff);
            canvas.drawPath(mStrokePath, mStrokePaint);
            if (mFeedForward) {
                if (mode == Mode.OCTOPOCUS_FF) {
                    for (Command c : mCommands) {
                        if (c.width > 8) {
                            mPaint.setColor(0x80880000);
                            mPaint.setStrokeWidth(c.width);
                            canvas.drawPath(c.path, mPaint);
                            mPaint.setStyle(Paint.Style.FILL);
                            canvas.drawText(c.label, c.labelX, c.labelY + 12, mPaint);
                            mPaint.setStyle(Paint.Style.STROKE);
                        }
                    }
                }
                else if (mode == Mode.PATH_FF) {
                    for (Candidate c : mCandidates) {
                        if (c == null || c.active == false)
                            continue;
                        mPaint.setColor((c.score * 0x00020102) ^ 0x80007fff);
                        canvas.drawPath(c.opaquePath, mPaint);
                        mPaint.setStyle(Paint.Style.FILL);
                        canvas.drawText(c.label, c.labelX, c.labelY + 12, mPaint);
                        mPaint.setStyle(Paint.Style.STROKE);
                        mPaint.setColor((c.score * 0x00020102) ^ 0x20007fff);
                        canvas.drawPath(c.translucentPath, mPaint);
                    }
                }
            }
        }
    }

    private void blinkText(final TextView txt) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 150;    //in milissegunds
                int timesBlinked = 0;

                try {
                    Thread.sleep(timeToBlink);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (txt.getVisibility() == View.VISIBLE) {
                            txt.setVisibility(View.INVISIBLE);
                            blinkText(txt);
                        } else {
                            txt.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        }).start();
    }

    private void startExperiment() {
        experiment = new ExperimentSession(participantID);
        startNewBlock();
    }

    private void nextCommandButtonPressed() {
        if (experiment.trialNumber < 6)
            startNewOctopocusTrial();
        else {
            learningBlockComplete();
        }
    }
    //the start of every block
    private void startNewOctopocusTrial() {
        currentTrial = experiment.newTrial();
        currentTrial.start();

        commandName.setText("Learn Gesture: " + experiment.getCommandForLearning().label);
        blinkText(commandName);

        currentScore.setText("");
    }

    public void learningBlockComplete() {
        new AlertDialog.Builder(this)
                .setTitle("GESTURE SET TEST")
                .setMessage("\nDraw each gesture 3 times in a row as well as you can remember.")
                .setPositiveButton("START", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        saveGesture.setVisibility(View.GONE);
                        nextCommand.setVisibility(View.VISIBLE);
                        mode = Mode.NO_FF;
                        commandName.setText("");
                        evaluation = new EvaluationSession(experiment, 6);
                        nextCommand.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                nextCommandLearningEvaluationButtonPressed();
                            }
                        });
                       startNewLearningEvaluationTrial();
                    }
                })
                .setCancelable(false)
                .setInverseBackgroundForced(true)
                .setIcon(android.R.drawable.btn_star_big_on)
                .show();
    }


    private void nextCommandLearningEvaluationButtonPressed() {
        if (evaluation.trialNumber < 6) {
            startNewLearningEvaluationTrial();
        }
        else {
            nextCommand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    nextCommandButtonPressed();
                }
            });
            learningEvaluationBlockComplete();
            giveCommandInstructions();
            startNewGestureCreationBlock();
        }
    }


    public void learningEvaluationBlockComplete() {
        experiment.startGestureCreationBlock();
    }

    private void startNewGestureCreationBlock() {
        mode = experiment.getFeedForwardType();
        saveGesture.setVisibility(View.VISIBLE);
        nextCommand.setVisibility(View.GONE);
        new AlertDialog.Builder(this)
                .setTitle("CREATE 3 NEW GESTURES THAT " + currentTrial.getDesignGoalInstructions())
                .setMessage("\nYou will later have to test how well you remember them.\n\nPlease DO NOT draw letters or numbers as gestures.\n\nAll gestures must be different from the ones you learned. \n\n" + currentTrial.getFeedForwardInstructions())
                .setPositiveButton("START", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        blinkText(commandName);
                    }
                })
                .setCancelable(false)
                .setIcon(android.R.drawable.btn_star_big_on)
                .show();
    }

    private void giveCommandInstructions(){
        currentTrial = experiment.newGestureCreationTrial();
        currentTrial.start();
        blinkText(commandName);
        commandName.setText("Create a gesture for: " + experiment.getCommandForGestureCreation(i + 1).label + "\n[" + currentTrial.command.instruction + "]");
    }

    private void showAchievementDialog() {
        final CharSequence[] items = {" I Strongly agree "," I Agree "," Neutral "," I Disagree ", " I Strongly disagree "};
        new AlertDialog.Builder(this)
                .setTitle("It was easy to create a gesture that "+currentTrial.getAssessmentQuestion())
//            .setMessage("How easy was to achieve the goal of the task?") //make it explicit: memorable or recognizable
                .setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        currentTrial.setUserAssesmentOfAchievement(item);
                    }
                })
                .setPositiveButton("NEXT", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        showRecallDialog();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }
//    private void showRecallDialog() {
//        final CharSequence[] items = {" I Strongly agree "," I Agree "," Neutral "," I Disagree ", " I Strongly disagree "};
//        new AlertDialog.Builder(this)
//                .setTitle("I think I will remember this gesture easily.")
////            .setMessage("How memorable do you think this gesture will be for you?")
//                .setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int item) {
//                        currentTrial.setUserAssesmentOfRecall(item);
//                    }
//                })
//                .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        currentTrial.logAssesmentData();
//
//                        if (currentTrial.trialNumber == 3) {
//                            startShortCreatedGestureEvaluationBlock(3);
//                            return;
//                        }
//                        if (currentTrial.trialNumber == 6) {
//                            startShortCreatedGestureEvaluationBlock(6);
//                            return;
//                        }
//                        currentTrial.end();
//                        giveCommandInstructions();
//                    }
//                })
//                .setCancelable(false)
//                .create()
//                .show();
//    }

    private void finishBlock() {
        new AlertDialog.Builder(this)
                .setTitle("BLOCK "+experiment.currentBlock+" COMPLETE!")
                .setMessage("Good job! Now you will have a short discussion about your experience so far. \n\nPress START to continue the experiment.")

//                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // do nothing
//                    }
//                })
                .setIcon(android.R.drawable.btn_star_big_on)
                .setPositiveButton("START", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (experiment.currentBlock < 3) {
                            startNewBlock();
                        } else {
                            startEvaluationBlock();
                        }
                    }
                })
                .setCancelable(false)
                .show();
    }

    private void startNewBlock() {
        experiment.startNewBlock();
        buildGestureSet(experiment.getGestureSetNumber());
        experiment.setGestureCommands(mStore);
        nextCommand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextCommandButtonPressed();
            }


        });
        nextCommand.setVisibility(View.VISIBLE);
        saveGesture.setVisibility(View.GONE);
        mode = Mode.OCTOPOCUS_FF;
        new AlertDialog.Builder(this)
                .setTitle("GESTURE SET PRACTICE")
                .setMessage("Learn and practice each of the following gestures. \n\nYou will later have to test how well you remember them.\n\nA high score means you performed one of the existing gestures.\n\nA low score means that no gesture matches your drawing.")
                .setMessage("Learn and practice each of the following gestures. \n\nYou will later have to test how well you remember them.\n\nA high score means you performed one of the existing gestures.\n\nA low score means that no gesture matches your drawing.")
                .setPositiveButton("START", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startNewOctopocusTrial();
                    }
                })
                .setCancelable(false)
                .setIcon(android.R.drawable.btn_star_big_on)
                .show();
    }

    private void startEvaluationBlock() {
        evaluation = experiment.newCreatedGesturesEvaluationSession();
        nextCommand.setVisibility(View.VISIBLE);
        saveGesture.setVisibility(View.GONE);
        nextCommand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextCommandEvaluationButtonPressed();
            }
        });
        mode = Mode.NO_FF;
        new AlertDialog.Builder(this)
                .setTitle("FINAL REVIEW BLOCK")
                .setMessage("Now you will perform the 18 gestures you created again.\n\nDraw each gesture 3 times as well as you can remember. Then press the \"NEXT COMMAND\" button.")
                .setPositiveButton("START", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mStore = experiment.getGestureStoreForBlock(evaluation.currentBlock);
                        startNewCreatedGestureEvaluationTrial();
                    }
                })
                .setCancelable(false)
                .setIcon(android.R.drawable.btn_star_big_on)
                .show();
    }

    private void nextCommandEvaluationButtonPressed() {
        if (evaluation.trialNumber < 17){
            mStore = experiment.getGestureStoreForBlock(evaluation.currentBlock);
            startNewCreatedGestureEvaluationTrial();
        }
        else {
            new AlertDialog.Builder(this)
                    .setTitle("EXPERIMENT COMPLETE!")
                    .setMessage("Thank you very much for your participation!")
                    .setPositiveButton("START", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setCancelable(false)
                    .setIcon(android.R.drawable.btn_star_big_on)
                    .show();

        }
    }

    private void startNewCreatedGestureEvaluationTrial() {
        currentTrial = evaluation.newTrial();

        commandName.setText("Draw the gesture you created for: " + evaluation.getCommandForGestureCreation().label);
        currentScore.setText("");
        blinkText(commandName);
    }
    private void startNewLearningEvaluationTrial() {
        currentTrial = evaluation.newTrial();

        commandName.setText("Draw the gesture for: " + evaluation.getCommandForLearning().label);
        blinkText(commandName);
    }

    private void startShortCreatedGestureEvaluationBlock(int lastTrialToEvaluate) {
        evaluation = new CreatedGesturesEvaluationSession(experiment,lastTrialToEvaluate,experiment.currentBlock,lastTrialToEvaluate-2);
        nextCommand.setVisibility(View.VISIBLE);
        saveGesture.setVisibility(View.GONE);
        nextCommand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextCommandInShortEvaluationButtonPressed();
            }
        });
        mode = Mode.NO_FF;
        new AlertDialog.Builder(this)
                .setTitle("CREATED GESTURES REVIEW")
                .setMessage("Draw each of the gestures you created 3 times as well as you can remember. Then press the \"NEXT COMMAND\" button.")
                .setPositiveButton("START", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startNewCreatedGestureEvaluationTrial();
                    }
                })
                .setCancelable(false)
                .setIcon(android.R.drawable.btn_star_big_on)
                .show();
    }

    private void nextCommandInShortEvaluationButtonPressed() {
        if (evaluation.trialNumber < evaluation.totalTrials)
            startNewCreatedGestureEvaluationTrial();
        else {
            if(evaluation.trialNumber == 3) {
                giveCommandInstructions();
                //saveGesture.setOnClickListener(this);
                startNewGestureCreationBlock();
            }
            else
                finishBlock();                      //only 6 trials in total
        }
    }


}
