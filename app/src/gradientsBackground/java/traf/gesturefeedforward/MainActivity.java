package traf.gesturefeedforward;

import android.app.Activity;
import android.content.Context;
import android.gesture.Gesture;
import android.gesture.GestureStore;
import android.gesture.GestureStroke;
import android.gesture.Prediction;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.opengl.GLSurfaceView;
import android.opengl.GLES20;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.widget.Toast;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;

public class MainActivity extends Activity {
    // The current stroke is stored as a raw array of points for ease of prototyping
    int mStrokeSize;
    float[] mStroke = new float[12];
    float mStrokeLength;
    RectF mStrokeBox = new RectF();
    Path mStrokePath = new Path();
    Paint mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    // Instances for keeping track of the feedforward
    boolean mFeedForward = false;
    Handler mHandler = new Handler();
    float mShowPressX, mShowPressY;
    RectF mBox = new RectF();
    long[] mTimestamps = new long[0];
    Paint mPaint = new Paint();
    int mVec, mProgram; // Has to be a global handle for OpenGL to work!
    FloatBuffer mVerts;
    ByteBuffer mIndices0, mIndices1;

    // Resources needed for matching the stroke to existing templates
    File mGesturesFile = new File(Environment.getExternalStorageDirectory(), "gestures");
    GestureStore mStore = new GestureStore();
    Constructor<GestureStroke> mGestureStroke_constructor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new DrawView(this));

        // Load the initial set of templates
        try {
            mStore.load(mGesturesFile.canRead() ? new FileInputStream(mGesturesFile) : getResources().openRawResource(R.raw.gestures), true);
        } catch (Exception e) { System.out.println(e.getMessage()); }
        Toast.makeText(this, mStore.getGestureEntries().size() + " gestures loaded from " + (mGesturesFile.canRead() ? mGesturesFile.getPath() : "resource file"), Toast.LENGTH_SHORT).show();

        // Mandatory hack to make a GestureStroke out of an array of points.
        try {
            mGestureStroke_constructor = GestureStroke.class.getDeclaredConstructor(RectF.class, float.class, float[].class, long[].class);
            mGestureStroke_constructor.setAccessible(true);
        } catch (NoSuchMethodException e) { System.out.println(e.getMessage()); }
        mStrokePaint.setStrokeCap(Paint.Cap.ROUND);
        mStrokePaint.setStrokeJoin(Paint.Join.BEVEL);
        mStrokePaint.setStrokeWidth(12);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(20);
        mPaint.setStyle(Paint.Style.STROKE);
        
        // Create the background array of points
        final int resX = 16, resY = 10;
        mVerts = ByteBuffer.allocateDirect((resX + 1) * (resY + 1) * 16).order(ByteOrder.nativeOrder()).asFloatBuffer();
        for (int i = 0, index = 0; i <= resY; i++) {
            for (int j = 0; j <= resX; j++) {
                mVerts.put(index++, j * (2560f / resX));
                mVerts.put(index++, i * (1550f / resY));
                mVerts.put(index++, 0);
                mVerts.put(index++, 0.5f);
            }
        }
        
        // Create the two sets of triangles
        mIndices0 = ByteBuffer.allocateDirect(resX * resY * 6);
        mIndices1 = ByteBuffer.allocateDirect(resX * resY * 6);
        for (int i = 0, index0 = 0, index1 = 0; i < resY; i++) {
            for (int j = 0; j < resX; j++) {
                byte topLeft = (byte)(i * (resX + 1) + j);
                byte topRight = (byte)(topLeft + 1);
                byte bottomLeft = (byte)(topLeft + resX + 1);
                byte bottomRight = (byte)(bottomLeft + 1);
                mIndices0.put(index0++, topLeft);
                mIndices0.put(index0++, bottomLeft);
                mIndices0.put(index0++, topRight);
                mIndices0.put(index0++, topRight);
                mIndices0.put(index0++, bottomLeft);
                mIndices0.put(index0++, bottomRight);
                mIndices1.put(index1++, bottomLeft);
                mIndices1.put(index1++, bottomRight);
                mIndices1.put(index1++, topLeft);
                mIndices1.put(index1++, topLeft);
                mIndices1.put(index1++, bottomRight);
                mIndices1.put(index1++, topRight);
            }
        }
    }



    void update_colors() {
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize + 2);
        for (int i = 0; i < mVerts.limit() / 4; i++) {
            pts[mStrokeSize] = mVerts.get(4 * i);
            pts[mStrokeSize + 1] = mVerts.get(4 * i + 1);
            mBox.set(mStrokeBox);
            mBox.union(pts[mStrokeSize], pts[mStrokeSize + 1]);
            float len = FloatMath.hypot(pts[mStrokeSize] - pts[mStrokeSize - 2], pts[mStrokeSize + 1] - pts[mStrokeSize - 1]);

            // Use our hack-structor to run this custom gesture into the recogniser
            Gesture g = new Gesture();
            try {
                g.addStroke(mGestureStroke_constructor.newInstance(mBox, mStrokeLength + len, pts, mTimestamps));
            } catch (Exception e) { System.out.println(e.getMessage()); }
            Prediction p = mStore.recognize(g).get(0);
            mVerts.put(4 * i + 2, Math.min((float)p.score / 4, 1));
        }
    }



    class DrawRenderer implements GLSurfaceView.Renderer {
        public void onDrawFrame(GL10 glUnused) {
            mIndices0.position(0);
            mIndices1.position(0);
            GLES20.glDisable(GLES20.GL_BLEND);
            if (!mFeedForward) {
                GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
            } else {
                GLES20.glDrawElements(GLES20.GL_TRIANGLES, mIndices0.limit(), GLES20.GL_UNSIGNED_BYTE, mIndices0);
                GLES20.glEnable(GLES20.GL_BLEND);
                GLES20.glDrawElements(GLES20.GL_TRIANGLES, mIndices1.limit(), GLES20.GL_UNSIGNED_BYTE, mIndices1);
            }
        }

        public void onSurfaceChanged(GL10 glUnused, int width, int height) {
            GLES20.glViewport(0, 0, width, height);
        }

        public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
            mProgram = GLES20.glCreateProgram();
            int vshader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
            int fshader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
            GLES20.glShaderSource(vshader, "attribute vec4 aVec; varying lowp vec4 vCol; void main() { gl_Position = vec4(aVec.x/1280.0-1.0,1.0-aVec.y/775.0,0,1); vCol = vec4(0.5+aVec.z*0.5,1.0-aVec.z*0.5,0.5,aVec.w); }");
            GLES20.glShaderSource(fshader, "varying lowp vec4 vCol; void main() { gl_FragColor = vCol; }");
            GLES20.glCompileShader(vshader);
            GLES20.glCompileShader(fshader);
            GLES20.glAttachShader(mProgram, vshader);
            GLES20.glAttachShader(mProgram, fshader);
            GLES20.glLinkProgram(mProgram);
            GLES20.glUseProgram(mProgram);
            mVec = GLES20.glGetAttribLocation(mProgram, "aVec");
            GLES20.glEnableVertexAttribArray(mVec);
            GLES20.glVertexAttribPointer(mVec, 4, GLES20.GL_FLOAT, false, 16, mVerts);
            GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
            GLES20.glBlendFunc (GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        }
    }



    class DrawView extends GLSurfaceView implements Runnable {
        DrawView(Context context) {
            super(context);
            setEGLContextClientVersion(2);
            setEGLConfigChooser(8, 8, 8, 8, 0, 0);
            setRenderer(new DrawRenderer());
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mStrokeSize = 2;
                    mStroke[0] = mShowPressX = x;
                    mStroke[1] = mShowPressY = y;
                    mStrokeLength = 0;
                    mStrokeBox.set(x, y, x, y);
                    mStrokePath.moveTo(x, y);
                    mStrokePaint.setColor(0x80000000);
                    mHandler.postDelayed(this, 500);
                    break;
                case MotionEvent.ACTION_MOVE:
                    mStrokeLength += FloatMath.hypot(x - mStroke[mStrokeSize - 2], y - mStroke[mStrokeSize - 1]);
                    mStrokeBox.union(x, y);
                    mStrokePath.lineTo(x, y);

                    // Reallocate mStroke if necessary
                    if (mStrokeSize + 2 > mStroke.length)
                        mStroke = Arrays.copyOf(mStroke, 2 * mStroke.length);
                    mStroke[mStrokeSize++] = x;
                    mStroke[mStrokeSize++] = y;

                    // Reset the long press when the finger has moved too far
                    if (mFeedForward) {
                        update_colors();
                    } else if (FloatMath.hypot(x - mShowPressX, y - mShowPressY) > 50) {
                        mShowPressX = x;
                        mShowPressY = y;
                        mHandler.removeCallbacks(this);
                        mHandler.postDelayed(this, 500);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    mStrokePath.rewind();
                    mHandler.removeCallbacks(this);
                    mFeedForward = false;
                    break;
            }
            invalidate();
            return true;
        }

        // Runnable interface for implementing the long press
        public void run() {
            ((Vibrator)getSystemService(Context.VIBRATOR_SERVICE)).vibrate(30);
            mFeedForward = true;
            update_colors();
            invalidate();
        }
    }
}
