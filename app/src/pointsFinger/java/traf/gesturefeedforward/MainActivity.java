package traf.gesturefeedforward;

import android.app.Activity;
import android.content.Context;
import android.gesture.Gesture;
import android.gesture.GestureStore;
import android.gesture.GestureStroke;
import android.gesture.Prediction;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.util.Arrays;

public class MainActivity extends Activity {
    static final float RADIUS1 = 120f;
    static final float RADIUS2 = 180f;
    static final float RADIUS3 = 240f;
    static final float PI = (float)Math.PI;
    static final float[] sFan = {
            RADIUS1 * FloatMath.cos(PI * 0 / 12), RADIUS1 * FloatMath.sin(PI * 0 / 12),
            RADIUS1 * FloatMath.cos(PI * 2 / 12), RADIUS1 * FloatMath.sin(PI * 2 / 12),
            RADIUS1 * FloatMath.cos(PI * 4 / 12), RADIUS1 * FloatMath.sin(PI * 4 / 12),
            RADIUS1 * FloatMath.cos(PI * 6 / 12), RADIUS1 * FloatMath.sin(PI * 6 / 12),
            RADIUS1 * FloatMath.cos(PI * 8 / 12), RADIUS1 * FloatMath.sin(PI * 8 / 12),
            RADIUS1 * FloatMath.cos(PI * 10 / 12), RADIUS1 * FloatMath.sin(PI * 10 / 12),
            RADIUS1 * FloatMath.cos(PI * 12 / 12), RADIUS1 * FloatMath.sin(PI * 12 / 12),
            RADIUS1 * FloatMath.cos(PI * 14 / 12), RADIUS1 * FloatMath.sin(PI * 14 / 12),
            RADIUS1 * FloatMath.cos(PI * 16 / 12), RADIUS1 * FloatMath.sin(PI * 16 / 12),
            RADIUS1 * FloatMath.cos(PI * 18 / 12), RADIUS1 * FloatMath.sin(PI * 18 / 12),
            RADIUS1 * FloatMath.cos(PI * 20 / 12), RADIUS1 * FloatMath.sin(PI * 20 / 12),
            RADIUS1 * FloatMath.cos(PI * 22 / 12), RADIUS1 * FloatMath.sin(PI * 22 / 12),
            RADIUS2 * FloatMath.cos(PI * 1 / 16), RADIUS2 * FloatMath.sin(PI * 1 / 16),
            RADIUS2 * FloatMath.cos(PI * 3 / 16), RADIUS2 * FloatMath.sin(PI * 3 / 16),
            RADIUS2 * FloatMath.cos(PI * 5 / 16), RADIUS2 * FloatMath.sin(PI * 5 / 16),
            RADIUS2 * FloatMath.cos(PI * 7 / 16), RADIUS2 * FloatMath.sin(PI * 7 / 16),
            RADIUS2 * FloatMath.cos(PI * 9 / 16), RADIUS2 * FloatMath.sin(PI * 9 / 16),
            RADIUS2 * FloatMath.cos(PI * 11 / 16), RADIUS2 * FloatMath.sin(PI * 11 / 16),
            RADIUS2 * FloatMath.cos(PI * 13 / 16), RADIUS2 * FloatMath.sin(PI * 13 / 16),
            RADIUS2 * FloatMath.cos(PI * 15 / 16), RADIUS2 * FloatMath.sin(PI * 15 / 16),
            RADIUS2 * FloatMath.cos(PI * 17 / 16), RADIUS2 * FloatMath.sin(PI * 17 / 16),
            RADIUS2 * FloatMath.cos(PI * 19 / 16), RADIUS2 * FloatMath.sin(PI * 19 / 16),
            RADIUS2 * FloatMath.cos(PI * 21 / 16), RADIUS2 * FloatMath.sin(PI * 21 / 16),
            RADIUS2 * FloatMath.cos(PI * 23 / 16), RADIUS2 * FloatMath.sin(PI * 23 / 16),
            RADIUS2 * FloatMath.cos(PI * 25 / 16), RADIUS2 * FloatMath.sin(PI * 25 / 16),
            RADIUS2 * FloatMath.cos(PI * 27 / 16), RADIUS2 * FloatMath.sin(PI * 27 / 16),
            RADIUS2 * FloatMath.cos(PI * 29 / 16), RADIUS2 * FloatMath.sin(PI * 29 / 16),
            RADIUS2 * FloatMath.cos(PI * 31 / 16), RADIUS2 * FloatMath.sin(PI * 31 / 16),
            RADIUS3 * FloatMath.cos(PI * 0 / 20), RADIUS3 * FloatMath.sin(PI * 0 / 20),
            RADIUS3 * FloatMath.cos(PI * 2 / 20), RADIUS3 * FloatMath.sin(PI * 2 / 20),
            RADIUS3 * FloatMath.cos(PI * 4 / 20), RADIUS3 * FloatMath.sin(PI * 4 / 20),
            RADIUS3 * FloatMath.cos(PI * 6 / 20), RADIUS3 * FloatMath.sin(PI * 6 / 20),
            RADIUS3 * FloatMath.cos(PI * 8 / 20), RADIUS3 * FloatMath.sin(PI * 8 / 20),
            RADIUS3 * FloatMath.cos(PI * 10 / 20), RADIUS3 * FloatMath.sin(PI * 10 / 20),
            RADIUS3 * FloatMath.cos(PI * 12 / 20), RADIUS3 * FloatMath.sin(PI * 12 / 20),
            RADIUS3 * FloatMath.cos(PI * 14 / 20), RADIUS3 * FloatMath.sin(PI * 14 / 20),
            RADIUS3 * FloatMath.cos(PI * 16 / 20), RADIUS3 * FloatMath.sin(PI * 16 / 20),
            RADIUS3 * FloatMath.cos(PI * 18 / 20), RADIUS3 * FloatMath.sin(PI * 18 / 20),
            RADIUS3 * FloatMath.cos(PI * 20 / 20), RADIUS3 * FloatMath.sin(PI * 20 / 20),
            RADIUS3 * FloatMath.cos(PI * 22 / 20), RADIUS3 * FloatMath.sin(PI * 22 / 20),
            RADIUS3 * FloatMath.cos(PI * 24 / 20), RADIUS3 * FloatMath.sin(PI * 24 / 20),
            RADIUS3 * FloatMath.cos(PI * 26 / 20), RADIUS3 * FloatMath.sin(PI * 26 / 20),
            RADIUS3 * FloatMath.cos(PI * 28 / 20), RADIUS3 * FloatMath.sin(PI * 28 / 20),
            RADIUS3 * FloatMath.cos(PI * 30 / 20), RADIUS3 * FloatMath.sin(PI * 30 / 20),
            RADIUS3 * FloatMath.cos(PI * 32 / 20), RADIUS3 * FloatMath.sin(PI * 32 / 20),
            RADIUS3 * FloatMath.cos(PI * 34 / 20), RADIUS3 * FloatMath.sin(PI * 34 / 20),
            RADIUS3 * FloatMath.cos(PI * 36 / 20), RADIUS3 * FloatMath.sin(PI * 36 / 20),
            RADIUS3 * FloatMath.cos(PI * 38 / 20), RADIUS3 * FloatMath.sin(PI * 38 / 20),
    };

    // The current stroke is stored as a raw array of points for ease of prototyping
    int mStrokeSize;
    float[] mStroke = new float[12];
    float mStrokeLength;
    RectF mStrokeBox = new RectF();
    Path mStrokePath = new Path();
    Paint mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    // Instances for keeping track of the feedforward
    boolean mFeedForward = false;
    Handler mHandler = new Handler();
    float mShowPressX, mShowPressY;
    RectF mBox = new RectF();
    long[] mTimestamps = new long[0];
    Paint mPaint = new Paint();
    int[] mColors = new int[sFan.length / 2];

    // Resources needed for matching the stroke to existing templates
    File mGesturesFile = new File(Environment.getExternalStorageDirectory(), "gestures");
    GestureStore mStore = new GestureStore();
    Constructor<GestureStroke> mGestureStroke_constructor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new DrawView(this));

        // Load the initial set of templates
        try {
            mStore.load(mGesturesFile.canRead() ? new FileInputStream(mGesturesFile) : getResources().openRawResource(R.raw.gestures), true);
        } catch (Exception e) { System.out.println(e.getMessage()); }
        Toast.makeText(this, mStore.getGestureEntries().size() + " gestures loaded from " + (mGesturesFile.canRead() ? mGesturesFile.getPath() : "resource file"), Toast.LENGTH_SHORT).show();

        // Mandatory hack to make a GestureStroke out of an array of points.
        try {
            mGestureStroke_constructor = GestureStroke.class.getDeclaredConstructor(RectF.class, float.class, float[].class, long[].class);
            mGestureStroke_constructor.setAccessible(true);
        } catch (NoSuchMethodException e) { System.out.println(e.getMessage()); }
        mStrokePaint.setStrokeCap(Paint.Cap.ROUND);
        mStrokePaint.setStrokeJoin(Paint.Join.BEVEL);
        mStrokePaint.setStrokeWidth(12);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(20);
        mPaint.setStyle(Paint.Style.STROKE);
    }



    void update_colors() {
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize + 2);
        for (int i = 0; i < sFan.length / 2; i++) {
            pts[mStrokeSize] = pts[mStrokeSize - 2] + sFan[2 * i];
            pts[mStrokeSize + 1] = pts[mStrokeSize - 1] + sFan[2 * i + 1];
            mBox.set(mStrokeBox);
            mBox.union(pts[mStrokeSize], pts[mStrokeSize + 1]);
            float len = FloatMath.sqrt(sFan[2 * i] * sFan[2 * i] + sFan[2 * i + 1] * sFan[2 * i + 1]);

            // Use our hack-structor to run this custom gesture into the recogniser
            Gesture g = new Gesture();
            try {
                g.addStroke(mGestureStroke_constructor.newInstance(mBox, mStrokeLength + len, pts, mTimestamps));
            } catch (Exception e) { System.out.println(e.getMessage()); }
            Prediction p = mStore.recognize(g).get(0);
            int score = Math.min((int)(p.score * 64), 255);
            mColors[i] = Color.argb(192, score, 255 - score, 0);
        }
    }



    class DrawView extends View implements Runnable {
        DrawView(Context context) { super(context); }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mStrokeSize = 2;
                    mStroke[0] = mShowPressX = x;
                    mStroke[1] = mShowPressY = y;
                    mStrokeLength = 0;
                    mStrokeBox.set(x, y, x, y);
                    mStrokePath.moveTo(x, y);
                    mStrokePaint.setColor(0x80000000);
                    mHandler.postDelayed(this, 500);
                    break;
                case MotionEvent.ACTION_MOVE:
                    mStrokeLength += FloatMath.hypot(x - mStroke[mStrokeSize - 2], y - mStroke[mStrokeSize - 1]);
                    mStrokeBox.union(x, y);
                    mStrokePath.lineTo(x, y);

                    // Reallocate mStroke if necessary
                    if (mStrokeSize + 2 > mStroke.length)
                        mStroke = Arrays.copyOf(mStroke, 2 * mStroke.length);
                    mStroke[mStrokeSize++] = x;
                    mStroke[mStrokeSize++] = y;

                    // Reset the long press when the finger has moved too far
                    if (mFeedForward) {
                        update_colors();
                    } else if (FloatMath.hypot(x - mShowPressX, y - mShowPressY) > 50) {
                        mShowPressX = x;
                        mShowPressY = y;
                        mHandler.removeCallbacks(this);
                        mHandler.postDelayed(this, 500);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    mStrokePath.rewind();
                    mHandler.removeCallbacks(this);
                    mFeedForward = false;
                    break;
            }
            invalidate();
            return true;
        }

        // Runnable interface for implementing the long press
        public void run() {
            ((Vibrator)getSystemService(Context.VIBRATOR_SERVICE)).vibrate(30);
            mFeedForward = true;
            update_colors();
            invalidate();
        }

        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawPath(mStrokePath, mStrokePaint);
            for (int i = 0; mFeedForward && i < sFan.length / 2; i++) {
                mPaint.setColor(mColors[i]);
                canvas.drawPoint(mStroke[mStrokeSize - 2] + sFan[2 * i], mStroke[mStrokeSize - 1] + sFan[2 * i + 1], mPaint);
            }
        }
    }
}
