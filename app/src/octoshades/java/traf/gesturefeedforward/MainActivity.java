package traf.gesturefeedforward;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.gesture.Gesture;
import android.gesture.GestureStore;
import android.gesture.GestureStroke;
import android.gesture.Prediction;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.opengl.GLSurfaceView;
import android.opengl.GLES20;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.util.FloatMath;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class MainActivity extends Activity implements DialogInterface.OnClickListener, View.OnClickListener {
    static final long[] sTimestamps = new long[0];
    static float hypot(float a, float b) {
        if (android.os.Build.VERSION.SDK_INT >= 17)
            return FloatMath.hypot(a, b);
        else
        return FloatMath.sqrt(a * a + b * b);
    }
    
    // Instances for keeping a list of existing gestures and displaying them.
    EditText mParticipantInput;
    GestureStore mStore = new GestureStore();
    Constructor<GestureStroke> mGestureStroke_constructor;
    File mDirectory;
    int mGestureNum = 0;
    SimpleDateFormat mDatePattern = new SimpleDateFormat("yyyyMMdd_HHmmss");
    float mLastEventX, mLastEventY;
    Handler mHandler = new Handler();
    boolean mFeedForward = false;
    int mProgram, mPos, mCol; // Has to be a global handle for OpenGL to work!
    FloatBuffer mVertices, mColors;
    ByteBuffer mIndices0, mIndices1;
    
    // The current stroke is stored as a raw array of points for ease of prototyping.
    int mStrokeSize;
    float[] mStroke = new float[12];
    long[] mStrokeStamps = new long[6];
    float mStrokeLength;
    RectF mStrokeBox = new RectF();
    Path mStrokePath = new Path();
    Paint mPaint = new Paint();
    RectF mBox = new RectF();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GLSurfaceView gl = new GLSurfaceView(this);
        gl.setEGLContextClientVersion(2);
        gl.setEGLConfigChooser(8, 8, 8, 8, 0, 0);
        gl.setRenderer(new DrawRenderer());
        Button b = new Button(this);
        b.setOnClickListener(this);
        b.setText("Add");
        addContentView(gl, new LayoutParams(-1, -1));
        addContentView(new DrawView(this), new LayoutParams(-1, -1));
        addContentView(b, new LayoutParams(-2, -2));

        // Load the initial set of gestures.
        try {
            mStore.load(getResources().openRawResource(R.raw.gestures), true);
        } catch (Exception e) { System.out.println(e.getMessage()); }

        // Mandatory hack to make a GestureStroke out of an array of points.
        try {
            mGestureStroke_constructor = GestureStroke.class.getDeclaredConstructor(RectF.class, float.class, float[].class, long[].class);
            mGestureStroke_constructor.setAccessible(true);
        } catch (NoSuchMethodException e) { System.out.println(e.getMessage()); }
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.BEVEL);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTextSize(48);
        
        // Above 16/10, the renderer has visual artefacts.
        final int resX = 13, resY = 8;
        mVertices = ByteBuffer.allocateDirect((resX + 1) * (resY + 1) * 8).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mColors = ByteBuffer.allocateDirect((resX + 1) * (resY + 1) * 12).order(ByteOrder.nativeOrder()).asFloatBuffer();
        for (int i = 0, v = 0, c = 0; i <= resY; i++) {
            for (int j = 0; j <= resX; j++) {
                mVertices.put(v++, j * (2560f / resX));
                mVertices.put(v++, i * (1550f / resY));
                mColors.put(c++, 0);
                mColors.put(c++, 0);
                mColors.put(c++, 0);
            }
        }
        
        // Create two sets of triangles that will display the grid as shades.
        mIndices0 = ByteBuffer.allocateDirect(resX * resY * 6);
        mIndices1 = ByteBuffer.allocateDirect(resX * resY * 6);
        for (int i = 0, index0 = 0, index1 = 0; i < resY; i++) {
            for (int j = 0; j < resX; j++) {
                byte topLeft = (byte)(i * (resX + 1) + j);
                byte topRight = (byte)(topLeft + 1);
                byte bottomLeft = (byte)(topLeft + resX + 1);
                byte bottomRight = (byte)(bottomLeft + 1);
                mIndices0.put(index0++, topLeft);
                mIndices0.put(index0++, bottomLeft);
                mIndices0.put(index0++, topRight);
                mIndices0.put(index0++, topRight);
                mIndices0.put(index0++, bottomLeft);
                mIndices0.put(index0++, bottomRight);
                mIndices1.put(index1++, bottomLeft);
                mIndices1.put(index1++, bottomRight);
                mIndices1.put(index1++, topLeft);
                mIndices1.put(index1++, topLeft);
                mIndices1.put(index1++, bottomRight);
                mIndices1.put(index1++, topRight);
            }
        }

        // Display the input dialog for participant number
        mParticipantInput = new EditText(this);
        mParticipantInput.setHint("Participant number");
        mParticipantInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setView(mParticipantInput)
                .setPositiveButton("OK", this)
                .show();
        if (!Environment.getExternalStorageState().equals("mounted"))
            Toast.makeText(this, "SD card not detected", Toast.LENGTH_SHORT).show();
    }



    // OnClickListener interface for receiving the participant number.
    public void onClick(DialogInterface dialog, int which) {
        String s = mParticipantInput.getText().toString();
        mDirectory = new File(Environment.getExternalStorageDirectory(), "Participant " + s);
        Toast.makeText(this, "Welcome :)", Toast.LENGTH_SHORT).show();
    }



    // OnClickListener interface for saving the current gesture when user presses Button.
    public void onClick(View v) {
        Gesture g = new Gesture();
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize);
        try {
            g.addStroke(mGestureStroke_constructor.newInstance(mStrokeBox, mStrokeLength, pts, Arrays.copyOf(mStrokeStamps, mStrokeSize / 2)));
        } catch (Exception e) { System.out.println(e.getMessage()); }
        String s = "Gesture " + Integer.toString(mGestureNum++);
        mStore.addGesture(s, g);
        Toast.makeText(this, s + " added!", Toast.LENGTH_SHORT).show();
        if (mGestureNum == 6) try {
            if (!mDirectory.exists())
                mDirectory.mkdirs();
            mStore.save(new FileOutputStream(new File(mDirectory, mDatePattern.format(new Date()))), true);
        } catch (Exception e) { System.out.println(e.getMessage()); }
    }



    // Refreshes the grid colors, for the feedforward.
    void update_colors() {
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize + 2);
        for (int i = 0; i < mVertices.limit() / 2; i++) {
            pts[mStrokeSize] = mVertices.get(2 * i);
            pts[mStrokeSize + 1] = mVertices.get(2 * i + 1);
            mBox.set(mStrokeBox);
            mBox.union(pts[mStrokeSize], pts[mStrokeSize + 1]);
            float len = hypot(pts[mStrokeSize] - pts[mStrokeSize - 2], pts[mStrokeSize + 1] - pts[mStrokeSize - 1]);

            // Use our hack-structor to run this custom gesture into the recogniser
            Gesture g = new Gesture();
            try {
                g.addStroke(mGestureStroke_constructor.newInstance(mBox, mStrokeLength + len, pts, sTimestamps));
            } catch (Exception e) { System.out.println(e.getMessage()); }
            ArrayList<Prediction> p = mStore.recognize(g);
            float score = Math.min((float)p.get(0).score / 4, 1);
            mColors.put(3 * i + 0, score);
            mColors.put(3 * i + 1, 0.5f);
            mColors.put(3 * i + 2, 1f - score);
        }
    }



    class DrawRenderer implements GLSurfaceView.Renderer {
        public void onDrawFrame(GL10 glUnused) {
            mIndices0.position(0);
            mIndices1.position(0);
            GLES20.glDisable(GLES20.GL_BLEND);
            if (!mFeedForward) {
                GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
            } else {
                GLES20.glDrawElements(GLES20.GL_TRIANGLES, mIndices0.limit(), GLES20.GL_UNSIGNED_BYTE, mIndices0);
                GLES20.glEnable(GLES20.GL_BLEND);
                GLES20.glDrawElements(GLES20.GL_TRIANGLES, mIndices1.limit(), GLES20.GL_UNSIGNED_BYTE, mIndices1);
            }
        }

        public void onSurfaceChanged(GL10 glUnused, int width, int height) {
            GLES20.glViewport(0, 0, width, height);
        }

        public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
            mProgram = GLES20.glCreateProgram();
            int vshader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
            int fshader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
            GLES20.glShaderSource(vshader, "attribute vec2 aPos; attribute vec3 aCol; varying lowp vec3 vCol; void main() { gl_Position = vec4(aPos.x/1280.0-1.0,1.0-aPos.y/775.0,0,1); vCol = aCol; }");
            GLES20.glShaderSource(fshader, "varying lowp vec3 vCol; void main() { gl_FragColor = vec4(vCol,0.5); }");
            GLES20.glCompileShader(vshader);
            GLES20.glCompileShader(fshader);
            GLES20.glAttachShader(mProgram, vshader);
            GLES20.glAttachShader(mProgram, fshader);
            GLES20.glLinkProgram(mProgram);
            GLES20.glUseProgram(mProgram);
            mPos = GLES20.glGetAttribLocation(mProgram, "aPos");
            mCol = GLES20.glGetAttribLocation(mProgram, "aCol");
            GLES20.glEnableVertexAttribArray(mPos);
            GLES20.glEnableVertexAttribArray(mCol);
            GLES20.glVertexAttribPointer(mPos, 2, GLES20.GL_FLOAT, false, 8, mVertices);
            GLES20.glVertexAttribPointer(mCol, 3, GLES20.GL_FLOAT, false, 12, mColors);
            GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
            GLES20.glBlendFunc (GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        }
    }



    class DrawView extends View implements Runnable {
        DrawView(Context context) { super(context); }
        
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            long t = event.getEventTime();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mStrokeSize = 2;
                    mStroke[0] = mLastEventX = x;
                    mStroke[1] = mLastEventY = y;
                    mStrokeStamps[0] = t;
                    mStrokeLength = 0;
                    mStrokeBox.set(x, y, x, y);
                    mStrokePath.rewind();
                    mStrokePath.moveTo(x, y);
                    mHandler.postDelayed(this, 500);
                    break;
                case MotionEvent.ACTION_MOVE:
                    mStrokeLength += hypot(x - mStroke[mStrokeSize - 2], y - mStroke[mStrokeSize - 1]);
                    mStrokeBox.union(x, y);
                    mStrokePath.lineTo(x, y);

                    // Reallocate mStroke if necessary
                    if (mStrokeSize + 2 > mStroke.length) {
                        mStroke = Arrays.copyOf(mStroke, 2 * mStroke.length);
                        mStrokeStamps = Arrays.copyOf(mStrokeStamps, 2 * mStrokeStamps.length);
                    }
                    mStrokeStamps[mStrokeSize / 2] = t;
                    mStroke[mStrokeSize++] = x;
                    mStroke[mStrokeSize++] = y;

                    // Reset the long press when the finger has moved too far
                    if (mFeedForward) {
                        update_colors();
                    } else if (hypot(x - mLastEventX, y - mLastEventY) > 50) {
                        mLastEventX = x;
                        mLastEventY = y;
                        mHandler.removeCallbacks(this);
                        mHandler.postDelayed(this, 500);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    mHandler.removeCallbacks(this);
                    mFeedForward = false;
                    break;
            }
            invalidate();
            return true;
        }

        // Runnable interface for implementing the long press
        public void run() {
            ((Vibrator)getSystemService(Context.VIBRATOR_SERVICE)).vibrate(30);
            mFeedForward = true;
            update_colors();
            invalidate();
        }

        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            mPaint.setColor(0x80000000);
            mPaint.setStrokeWidth(12);
            mPaint.setStyle(Paint.Style.STROKE);
            canvas.drawPath(mStrokePath, mPaint);
        }
    }
}
