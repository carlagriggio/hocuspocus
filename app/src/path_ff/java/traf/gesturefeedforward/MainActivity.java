package traf.gesturefeedforward;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.gesture.Gesture;
import android.gesture.GestureStroke;
import android.graphics.Point;
import android.graphics.RectF;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.InputType;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.view.ViewGroup.LayoutParams;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.Arrays;


public class MainActivity extends Activity implements DialogInterface.OnClickListener, View.OnClickListener {

    // TODO adjust size and atom count for Path-based feedforward
    // TODO evaluate > 4 paths, display top 3 and bottom 1

    Mode mode = Mode.OCTOPOCUS_FF;

    EditText mParticipantInput;
    TextView commandName;
    TextView commandInstruction;


    Button nextTrialButton;
    int participantID = 0;
    ExperimentSession experiment;
    File mDirectory;
    int mGestureNum = 0;
    SimpleDateFormat mDatePattern = new SimpleDateFormat("yyyyMMdd_HHmmss");


    int mStrokeSize;
    float[] mStroke = new float[12];
    float mStrokeLength;
    RectF mStrokeBox = new RectF();
    Constructor<GestureStroke> mGestureStroke_constructor;
    long[] mStrokeStamps = new long[6];

    GestureCanvasFrameWithFF ffCanvasView;
    private GestureCanvasFrame plainCanvas;
    private FrameLayout mainLayout;
    public DrawRenderer renderer;
    private Button backButton;


//    public void hideDetectedCommand() {
//        detectedGesture.setVisibility(View.INVISIBLE);
//    }
//    public void hideScore() {
//        currentScore.setVisibility(View.INVISIBLE);
//    }
//    public void showDetectedCommand() {
//        detectedGesture.setVisibility(View.VISIBLE);
//    }
//    public void showScore() {
//        currentScore.setVisibility(View.VISIBLE);
//    }

    public void startNextTask(Task nextTask) {
        experiment.setCurrentTask(nextTask);
        nextTask.start(this);
    }
    public void showFFCanvas() {
        if(plainCanvas != null) plainCanvas.setVisibility(View.GONE);

        try {
            ffCanvasView.setVisibility(View.VISIBLE);
            ffCanvasView.resetCanvas(this);
        } catch(Exception e) {
            ffCanvasView = new GestureCanvasFrameWithFF(this);
            ffCanvasView.isTimed = false;
            mainLayout.addView(ffCanvasView);
            ffCanvasView.resetCanvas(this);
        }
    }
    public void showPlainCanvas() {
        if(ffCanvasView != null) ffCanvasView.setVisibility(View.GONE);

        try {
            plainCanvas.setVisibility(View.VISIBLE);
            plainCanvas.resetCanvas(this);
        } catch(Exception e) {
            plainCanvas = new GestureCanvasFrame(this);
            mainLayout.addView(plainCanvas);
            plainCanvas.resetCanvas(this);
        }
    }




    // Resources needed for matching the stroke to existing templates
    File mGesturesFile = new File(Environment.getExternalStorageDirectory(), "gestures");
//    GestureStore mStore = new GestureStore();


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Session.getInstance().openConnection();
        GLSurfaceView gl = new GLSurfaceView(this);
        gl.setEGLContextClientVersion(2);
        gl.setEGLConfigChooser(8, 8, 8, 8, 0, 0);
        renderer = new DrawRenderer();
        gl.setRenderer(renderer);
        addContentView(gl, new LayoutParams(-1, -1));

        mainLayout = new FrameLayout(this);
        LinearLayout buttonsLayout = new LinearLayout(this);
        //commandName.setId(1);

        //create a text view
        commandName = new TextView(this);
        commandName.setText("");
        commandName.setTextSize(24.0f);
        //commandName.setId(1);
        commandName.setGravity(Gravity.CENTER_HORIZONTAL);
        commandName.setY(140);
        //adds the textview
        //layout.addView(commandName);

        commandInstruction = new TextView(this);
        commandInstruction.setText("");
        commandInstruction.setTextSize(16.0f);
        //commandName.setId(1);
        commandInstruction.setGravity(Gravity.CENTER_HORIZONTAL);
        commandInstruction.setY(240);


//        mainLayout.addView(commandName);
//        mainLayout.addView(commandInstruction);

        //create a button
        backButton = new Button(this);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goBackToPreviousTask();
            }
        });


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        //create a button
        nextTrialButton = new Button(this);
        nextTrialButton.setOnClickListener(this);
        //b.setId(2);
        nextTrialButton.setText("Save Gesture");
        nextTrialButton.setTextSize(24);
        nextTrialButton.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        //b.setId(2);
        backButton.setText("Back");
        backButton.setTextSize(30);
        //backButton.setX(350);
//        backButton.setGravity(Gravity.RIGHT);
        backButton.setVisibility(View.INVISIBLE);

        buttonsLayout.addView(nextTrialButton);
        buttonsLayout.addView(backButton);

        //I'm very sorry for this horrible way of NOT grouping views together :P

        addContentView(mainLayout, new LayoutParams(-1, -1));
//        addContentView(nextTrialButton, new LayoutParams(-2, -2));
//        addContentView(backButton, new LayoutParams(-2, -2));
        //addContentView(buttonsLayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));


        // Mandatory hack to make a GestureStroke out of an array of points.
        try {
            mGestureStroke_constructor = GestureStroke.class.getDeclaredConstructor(RectF.class, float.class, float[].class, long[].class);
            mGestureStroke_constructor.setAccessible(true);
        }
        catch (NoSuchMethodException e) {
            System.out.println(e.getMessage());
        }
        participantID = 1000;
        startPractice();
        // Load the initial set of templates
        // TODO: do we need this now?
//        try {
//            experiment.getGestureStore().load(mGesturesFile.canRead()
//                    ? new FileInputStream(mGesturesFile)
//                    : getResources().openRawResource(R.raw.gestures), true);
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
        //Toast.makeText(this, mStore.getGestureEntries().size() + " gestures loaded from " + (mGesturesFile.canRead() ? mGesturesFile.getPath() : "resource file"), Toast.LENGTH_SHORT).show();



//        mCommands = new Command[mStore.getGestureEntries().size()];
//        int i = 0;
//        for (String s : mStore.getGestureEntries())
//            mCommands[i++] = new Command(s, mStore.getGestures(s).get(0).getStrokes().get(0).points);



        // build the commands
//        buildGestureSet(experiment.getGestureSetNumber());
        //int gestureSetIndex = 0;
        if (!Environment.getExternalStorageState().equals("mounted"))
            Toast.makeText(this, "SD card not detected", Toast.LENGTH_SHORT).show();


    }

    private void goBackToPreviousTask() {
        Task previousTask = experiment.getCurrentTask().getPreviousTask();
        experiment.setCurrentTask(previousTask);
        previousTask.start(this);
    }

    public void showBackButton() {
        backButton.setVisibility(View.VISIBLE);
    }
    public void hideBackButton() {
        backButton.setVisibility(View.GONE);
    }



    // OnClickListener interface for receiving the participant number.
    public void onClick(DialogInterface dialog, int which) {
        //participantID = Integer.parseInt(mParticipantInput.getText().toString());
//        participantID = 1000;
//        startPractice();
        //buildGestureSet(1);
    }

//    void buildCommands(int gestureSetIndex) {
//        mCommands = new Command[6];
//        int i = 0;
//
//        switch (gestureSetIndex) {
//            case 0: // Gesture Set 1
//                // Cut (1 stroke)
//                mCommands[i++] = new Command("banana", 4, new int[]{1, 4, 1, 0},
//                        new int[]{1, -1, -1, 1}, new double[]{Math.PI * -0.67, 0, 0, 0},
//                        new float[]{1, 0.5f, 1, 0.5f});
//
//                // energy save on (1 stroke)
//                mCommands[i++] = new Command("lemon", 3, new int[]{4, 1, 0},
//                        new int[]{1, -1, 1}, new double[]{Math.PI * 0.42, 0, 0},
//                        new float[]{1, 0.5f, 0.5f});
//
//                // weather report (2 strokes)
//                mCommands[i++] = new Command("avocado", 2, new int[]{0, 0}, new int[]{-1, 1},
//                        new double[]{Math.PI, Math.PI * -0.5}, new float[]{1f, 1.5f});
//
//                // clock set (3 strokes)
//                mCommands[i++] = new Command("cherry", 3, new int[]{0, 0, 0},
//                        new int[]{1, 1, -1},
//                        new double[]{Math.PI * -0.4, Math.PI * 0.8, Math.PI * -0.8},
//                        new float[]{1f, 0.5f, 1.5f});
//                // paste (3 strokes)
//                mCommands[i++] = new Command("orange", 3, new int[]{0, 0, 0}, new int[]{1, 1, 1},
//                        new double[]{Math.PI * 0.5, Math.PI * -0.5, Math.PI * -0.5},
//                        new float[]{0.75f, 0.75f, 1.5f});
//
//                // news report (4 strokes)
//                mCommands[i++] = new Command("kiwi", 4, new int[]{0, 0, 0, 0},
//                        new int[]{1, 1, 1, 1},
//                        new double[]{Math.PI * 0.25, Math.PI * 0.5, Math.PI * -0.75, Math.PI * 0.6},
//                        new float[]{1.5f, 0.5f, 0.5f, 0.5f});
//
//
//                break;
//
//        }
//    }








    @Override
    // OnClickListener interface for saving the current gesture when user presses Button.
    public void onClick(View v) {
        experiment.getCurrentTask().finishTask(this);
    }




    public void saveGesture(String label, Gesture g) {
        experiment.getGestureStore().addGesture(label, g);
        //TODO: do the following at the end of the experiment with all gestures?
//        try {
//            if (!mDirectory.exists())
//                mDirectory.mkdirs();
//            mStore.save(new FileOutputStream(new File(mDirectory, "gesture-"+mDatePattern.format(new Date()))), true);
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
    }

    public void startNewTrial() {
        experiment.setCurrentTask(experiment.newTrial()); //returns the first task of the new trial
        renderer.mode = experiment.currentBlock.currentFeedForwardType();
        blinkCommandName();
//        ffCanvasView.currentScore.setText("");
        experiment.getCurrentTask().start(this);
    }

    public void setCommandLabelAndInstructions(String label, String instructions) {
        commandName.setText(label);
        commandInstruction.setText(instructions);
    }
    public void setLabelOfNextTaskButton(String label) {
        nextTrialButton.setText(label);
    }

    public void blinkCommandName() {
        blink(commandName);
    }
    public void blink(final TextView aTextView) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 150;    //in milissegunds
                int timesBlinked = 0;

                try {
                    Thread.sleep(timeToBlink);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (aTextView.getVisibility() == View.VISIBLE) {
                            aTextView.setVisibility(View.INVISIBLE);
                            blink(aTextView);
                        } else {
                            aTextView.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        }).start();
    }

    private void startExperiment() {
        experiment = new ExperimentSession(participantID);
        startNewBlock();
    }
    private void startPractice() {
        experiment = new NullExperiment(participantID, Mode.PATH_FF);
//        experiment.setParticipant(participantID);
        startNewBlock();
    }

    public void restartExperiment() {
        experiment = new ExperimentSessionReclycled(experiment);
        startNewBlock();
    }



    public void startNewBlock() {
        try {
            experiment.startNewBlock(this);
            startNewTrial();
            //popAlertNewGestureCreationBlock();
        } catch (NoMoreExperimentBlocksException e) {
            new AlertDialog.Builder(this)
                    .setTitle("EXPERIMENT COMPLETE!")
                    .setMessage("Thank you very much for your participation!")
                    .setCancelable(false)
                    .setPositiveButton("KEEP GOING!", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            restartExperiment();
                        }
                    })
                    .show();

        } catch (PracticeSessionOverException e) {
            new AlertDialog.Builder(this)
                    .setTitle("PRACTICE COMPLETE!")
                    .setMessage("Take a break if you want, and continue to the experiment.")
                    .setCancelable(false)
                    .setPositiveButton("START EXPERIMENT", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startExperiment();
                        }
                    })
                    .show();

        }

    }
    public void bufferGesture(GestureRegistrationTask gestureRegistrationTask) {
        Gesture g = new Gesture();
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize);
        try {
            g.addStroke(mGestureStroke_constructor.newInstance(mStrokeBox, mStrokeLength, pts, Arrays.copyOf(mStrokeStamps, mStrokeSize / 2)));
            gestureRegistrationTask.addGesture(experiment.getCurrentTask().command.label, g);
            //Log.i("BufferedGesture", experiment.getCurrentTask().command.label);

            //TODO: registration counter ++
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void showBoxesForReview() {
        this.showPlainCanvas();
        plainCanvas.setReviewFeedback();

    }

    public void showBoxesForRegistration() {
        this.showPlainCanvas();
        //this.showFFCanvas();
        plainCanvas.setRegistrationFeedback();
    }

    public void saveGestureFromCanvas(String label) {
        //ffCanvasView.gestureCanvas.saveGestureOn(experiment.getGestureStore());
    }

}
