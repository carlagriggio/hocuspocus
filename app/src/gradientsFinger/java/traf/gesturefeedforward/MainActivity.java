package traf.gesturefeedforward;

import android.app.Activity;
import android.content.Context;
import android.gesture.Gesture;
import android.gesture.GestureStore;
import android.gesture.GestureStroke;
import android.gesture.Prediction;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.opengl.GLSurfaceView;
import android.opengl.GLES20;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.widget.Toast;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;

public class MainActivity extends Activity {
    static final float RADIUS1 = 140f;
    static final float RADIUS2 = 220f;
    static final float RADIUS3 = 300f;
    static final float PI = (float)Math.PI;
    static final float[] sFan = {
            0, 0,
            RADIUS1 * FloatMath.cos(PI * 0 / 12), RADIUS1 * FloatMath.sin(PI * 0 / 12),
            RADIUS1 * FloatMath.cos(PI * 2 / 12), RADIUS1 * FloatMath.sin(PI * 2 / 12),
            RADIUS1 * FloatMath.cos(PI * 4 / 12), RADIUS1 * FloatMath.sin(PI * 4 / 12),
            RADIUS1 * FloatMath.cos(PI * 6 / 12), RADIUS1 * FloatMath.sin(PI * 6 / 12),
            RADIUS1 * FloatMath.cos(PI * 8 / 12), RADIUS1 * FloatMath.sin(PI * 8 / 12),
            RADIUS1 * FloatMath.cos(PI * 10 / 12), RADIUS1 * FloatMath.sin(PI * 10 / 12),
            RADIUS1 * FloatMath.cos(PI * 12 / 12), RADIUS1 * FloatMath.sin(PI * 12 / 12),
            RADIUS1 * FloatMath.cos(PI * 14 / 12), RADIUS1 * FloatMath.sin(PI * 14 / 12),
            RADIUS1 * FloatMath.cos(PI * 16 / 12), RADIUS1 * FloatMath.sin(PI * 16 / 12),
            RADIUS1 * FloatMath.cos(PI * 18 / 12), RADIUS1 * FloatMath.sin(PI * 18 / 12),
            RADIUS1 * FloatMath.cos(PI * 20 / 12), RADIUS1 * FloatMath.sin(PI * 20 / 12),
            RADIUS1 * FloatMath.cos(PI * 22 / 12), RADIUS1 * FloatMath.sin(PI * 22 / 12),
            RADIUS2 * FloatMath.cos(PI * 1 / 16), RADIUS2 * FloatMath.sin(PI * 1 / 16),
            RADIUS2 * FloatMath.cos(PI * 3 / 16), RADIUS2 * FloatMath.sin(PI * 3 / 16),
            RADIUS2 * FloatMath.cos(PI * 5 / 16), RADIUS2 * FloatMath.sin(PI * 5 / 16),
            RADIUS2 * FloatMath.cos(PI * 7 / 16), RADIUS2 * FloatMath.sin(PI * 7 / 16),
            RADIUS2 * FloatMath.cos(PI * 9 / 16), RADIUS2 * FloatMath.sin(PI * 9 / 16),
            RADIUS2 * FloatMath.cos(PI * 11 / 16), RADIUS2 * FloatMath.sin(PI * 11 / 16),
            RADIUS2 * FloatMath.cos(PI * 13 / 16), RADIUS2 * FloatMath.sin(PI * 13 / 16),
            RADIUS2 * FloatMath.cos(PI * 15 / 16), RADIUS2 * FloatMath.sin(PI * 15 / 16),
            RADIUS2 * FloatMath.cos(PI * 17 / 16), RADIUS2 * FloatMath.sin(PI * 17 / 16),
            RADIUS2 * FloatMath.cos(PI * 19 / 16), RADIUS2 * FloatMath.sin(PI * 19 / 16),
            RADIUS2 * FloatMath.cos(PI * 21 / 16), RADIUS2 * FloatMath.sin(PI * 21 / 16),
            RADIUS2 * FloatMath.cos(PI * 23 / 16), RADIUS2 * FloatMath.sin(PI * 23 / 16),
            RADIUS2 * FloatMath.cos(PI * 25 / 16), RADIUS2 * FloatMath.sin(PI * 25 / 16),
            RADIUS2 * FloatMath.cos(PI * 27 / 16), RADIUS2 * FloatMath.sin(PI * 27 / 16),
            RADIUS2 * FloatMath.cos(PI * 29 / 16), RADIUS2 * FloatMath.sin(PI * 29 / 16),
            RADIUS2 * FloatMath.cos(PI * 31 / 16), RADIUS2 * FloatMath.sin(PI * 31 / 16),
            RADIUS3 * FloatMath.cos(PI * 0 / 20), RADIUS3 * FloatMath.sin(PI * 0 / 20),
            RADIUS3 * FloatMath.cos(PI * 2 / 20), RADIUS3 * FloatMath.sin(PI * 2 / 20),
            RADIUS3 * FloatMath.cos(PI * 4 / 20), RADIUS3 * FloatMath.sin(PI * 4 / 20),
            RADIUS3 * FloatMath.cos(PI * 6 / 20), RADIUS3 * FloatMath.sin(PI * 6 / 20),
            RADIUS3 * FloatMath.cos(PI * 8 / 20), RADIUS3 * FloatMath.sin(PI * 8 / 20),
            RADIUS3 * FloatMath.cos(PI * 10 / 20), RADIUS3 * FloatMath.sin(PI * 10 / 20),
            RADIUS3 * FloatMath.cos(PI * 12 / 20), RADIUS3 * FloatMath.sin(PI * 12 / 20),
            RADIUS3 * FloatMath.cos(PI * 14 / 20), RADIUS3 * FloatMath.sin(PI * 14 / 20),
            RADIUS3 * FloatMath.cos(PI * 16 / 20), RADIUS3 * FloatMath.sin(PI * 16 / 20),
            RADIUS3 * FloatMath.cos(PI * 18 / 20), RADIUS3 * FloatMath.sin(PI * 18 / 20),
            RADIUS3 * FloatMath.cos(PI * 20 / 20), RADIUS3 * FloatMath.sin(PI * 20 / 20),
            RADIUS3 * FloatMath.cos(PI * 22 / 20), RADIUS3 * FloatMath.sin(PI * 22 / 20),
            RADIUS3 * FloatMath.cos(PI * 24 / 20), RADIUS3 * FloatMath.sin(PI * 24 / 20),
            RADIUS3 * FloatMath.cos(PI * 26 / 20), RADIUS3 * FloatMath.sin(PI * 26 / 20),
            RADIUS3 * FloatMath.cos(PI * 28 / 20), RADIUS3 * FloatMath.sin(PI * 28 / 20),
            RADIUS3 * FloatMath.cos(PI * 30 / 20), RADIUS3 * FloatMath.sin(PI * 30 / 20),
            RADIUS3 * FloatMath.cos(PI * 32 / 20), RADIUS3 * FloatMath.sin(PI * 32 / 20),
            RADIUS3 * FloatMath.cos(PI * 34 / 20), RADIUS3 * FloatMath.sin(PI * 34 / 20),
            RADIUS3 * FloatMath.cos(PI * 36 / 20), RADIUS3 * FloatMath.sin(PI * 36 / 20),
            RADIUS3 * FloatMath.cos(PI * 38 / 20), RADIUS3 * FloatMath.sin(PI * 38 / 20),
    };
    static final ByteBuffer sIndices = ByteBuffer.allocateDirect(228).put(new byte[]{
            0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 6, 0, 6, 7, 0, 7, 8, 0, 8, 9, 0, 9, 10, 0, 10, 11, 0, 11, 12, 0, 12, 1,
            1, 13, 2, 2, 13, 14, 2, 14, 15, 2, 15, 3, 3, 15, 16, 3, 16, 4, 4, 16, 17,
            4, 17, 5, 5, 17, 18, 5, 18, 19, 5, 19, 6, 6, 19, 20, 6, 20, 7, 7, 20, 21,
            7, 21, 8, 8, 21, 22, 8, 22, 23, 8, 23, 9, 9, 23, 24, 9, 24, 10, 10, 24, 25,
            10, 25, 11, 11, 25, 26, 11, 26, 27, 11, 27, 12, 12, 27, 28, 12, 28, 1, 1, 28, 13,
            13, 29, 30, 13, 30, 14, 14, 30, 31, 14, 31, 32, 14, 32, 15, 15, 32, 33, 15, 33, 16, 16, 33, 34, 16, 34, 17,
            17, 34, 35, 17, 35, 18, 18, 35, 36, 18, 36, 37, 18, 37, 19, 19, 37, 38, 19, 38, 20, 20, 38, 39, 20, 39, 21,
            21, 39, 40, 21, 40, 22, 22, 40, 41, 22, 41, 42, 22, 42, 23, 23, 42, 43, 23, 43, 24, 24, 43, 44, 24, 44, 25,
            25, 44, 45, 25, 45, 26, 26, 45, 46, 26, 46, 47, 26, 47, 27, 27, 47, 48, 27, 48, 28, 28, 48, 29, 28, 29, 13,
    });

    // The current stroke is stored as a raw array of points for ease of prototyping
    int mStrokeSize;
    float[] mStroke = new float[12];
    float mStrokeLength;
    RectF mStrokeBox = new RectF();
    Path mStrokePath = new Path();
    Paint mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    // Instances for keeping track of the feedforward
    boolean mFeedForward = false;
    Handler mHandler = new Handler();
    float mShowPressX, mShowPressY;
    RectF mBox = new RectF();
    long[] mTimestamps = new long[0];
    Paint mPaint = new Paint();
    int mVec, mProgram; // Has to be a global handle for OpenGL to work!
    FloatBuffer mVerts = ByteBuffer.allocateDirect(sFan.length * 8).order(ByteOrder.nativeOrder()).asFloatBuffer();

    // Resources needed for matching the stroke to existing templates
    File mGesturesFile = new File(Environment.getExternalStorageDirectory(), "gestures");
    GestureStore mStore = new GestureStore();
    Constructor<GestureStroke> mGestureStroke_constructor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new DrawView(this));

        // Load the initial set of templates
        try {
            mStore.load(mGesturesFile.canRead() ? new FileInputStream(mGesturesFile) : getResources().openRawResource(R.raw.gestures), true);
        } catch (Exception e) { System.out.println(e.getMessage()); }
        Toast.makeText(this, mStore.getGestureEntries().size() + " gestures loaded from " + (mGesturesFile.canRead() ? mGesturesFile.getPath() : "resource file"), Toast.LENGTH_SHORT).show();

        // Mandatory hack to make a GestureStroke out of an array of points.
        try {
            mGestureStroke_constructor = GestureStroke.class.getDeclaredConstructor(RectF.class, float.class, float[].class, long[].class);
            mGestureStroke_constructor.setAccessible(true);
        } catch (NoSuchMethodException e) { System.out.println(e.getMessage()); }
        mStrokePaint.setStrokeCap(Paint.Cap.ROUND);
        mStrokePaint.setStrokeJoin(Paint.Join.BEVEL);
        mStrokePaint.setStrokeWidth(12);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(20);
        mPaint.setStyle(Paint.Style.STROKE);
        
        mVerts.put(3, 0.65f);
        for (int i = 1; i < 13; i++)
            mVerts.put(4 * i + 3, 0.60f);
        for (int i = 13; i < 29; i++)
            mVerts.put(4 * i + 3, 0.50f);
        for (int i = 29; i < 49; i++)
            mVerts.put(4 * i + 3, 0.35f);
    }



    void update_colors() {
        mVerts.put(0, mStroke[mStrokeSize - 2]);
        mVerts.put(1, mStroke[mStrokeSize - 1]);
        mVerts.put(2, 1f);
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize + 2);
        for (int i = (mStrokeLength < 1f ? 1 : 0); i < sFan.length / 2; i++) {
            mVerts.put(4 * i, pts[mStrokeSize] = pts[mStrokeSize - 2] + sFan[2 * i]);
            mVerts.put(4 * i + 1, pts[mStrokeSize + 1] = pts[mStrokeSize - 1] + sFan[2 * i + 1]);
            mBox.set(mStrokeBox);
            mBox.union(pts[mStrokeSize], pts[mStrokeSize + 1]);
            float len = FloatMath.sqrt(sFan[2 * i] * sFan[2 * i] + sFan[2 * i + 1] * sFan[2 * i + 1]);

            // Use our hack-structor to run this custom gesture into the recogniser
            Gesture g = new Gesture();
            try {
                g.addStroke(mGestureStroke_constructor.newInstance(mBox, mStrokeLength + len, pts, mTimestamps));
            } catch (Exception e) { System.out.println(e.getMessage()); }
            Prediction p = mStore.recognize(g).get(0);
            mVerts.put(4 * i + 2, Math.min((float)p.score / 4, 1));
        }
    }



    class DrawRenderer implements GLSurfaceView.Renderer {
        public void onDrawFrame(GL10 glUnused) {
            mVerts.position(0);
            sIndices.position(0);
            GLES20.glDisable(GLES20.GL_BLEND);
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
            GLES20.glEnable(GLES20.GL_BLEND);
            if (mFeedForward)
                GLES20.glDrawElements(GLES20.GL_TRIANGLES, sIndices.limit(), GLES20.GL_UNSIGNED_BYTE, sIndices);
        }

        public void onSurfaceChanged(GL10 glUnused, int width, int height) {
            GLES20.glViewport(0, 0, width, height);
        }

        public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
            mProgram = GLES20.glCreateProgram();
            int vshader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
            int fshader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
            GLES20.glShaderSource(vshader, "attribute vec4 aVec; varying mwp vec4 vCol; void main() { gl_Position = vec4(aVec.x/1280.0-1.0,1.0-aVec.y/775.0,0,1); vCol = vec4(aVec.z,1.0-aVec.z,0,aVec.w); }");
            GLES20.glShaderSource(fshader, "varying lowp vec4 vCol; void main() { gl_FragColor = vCol; }");
            GLES20.glCompileShader(vshader);
            GLES20.glCompileShader(fshader);
            GLES20.glAttachShader(mProgram, vshader);
            GLES20.glAttachShader(mProgram, fshader);
            GLES20.glLinkProgram(mProgram);
            GLES20.glUseProgram(mProgram);
            mVec = GLES20.glGetAttribLocation(mProgram, "aVec");
            GLES20.glEnableVertexAttribArray(mVec);
            GLES20.glVertexAttribPointer(mVec, 4, GLES20.GL_FLOAT, false, 16, mVerts);
            GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glBlendFunc (GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        }
    }



    class DrawView extends GLSurfaceView implements Runnable {
        DrawView(Context context) {
            super(context);
            setEGLContextClientVersion(2);
            setEGLConfigChooser(8, 8, 8, 8, 0, 0);
            setRenderer(new DrawRenderer());
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mStrokeSize = 2;
                    mStroke[0] = mShowPressX = x;
                    mStroke[1] = mShowPressY = y;
                    mStrokeLength = 0;
                    mStrokeBox.set(x, y, x, y);
                    mStrokePath.moveTo(x, y);
                    mStrokePaint.setColor(0x80000000);
                    mHandler.postDelayed(this, 500);
                    break;
                case MotionEvent.ACTION_MOVE:
                    mStrokeLength += FloatMath.hypot(x - mStroke[mStrokeSize - 2], y - mStroke[mStrokeSize - 1]);
                    mStrokeBox.union(x, y);
                    mStrokePath.lineTo(x, y);

                    // Reallocate mStroke if necessary
                    if (mStrokeSize + 2 > mStroke.length)
                        mStroke = Arrays.copyOf(mStroke, 2 * mStroke.length);
                    mStroke[mStrokeSize++] = x;
                    mStroke[mStrokeSize++] = y;

                    // Reset the long press when the finger has moved too far
                    if (mFeedForward) {
                        update_colors();
                    } else if (FloatMath.hypot(x - mShowPressX, y - mShowPressY) > 50) {
                        mShowPressX = x;
                        mShowPressY = y;
                        mHandler.removeCallbacks(this);
                        mHandler.postDelayed(this, 500);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    mStrokePath.rewind();
                    mHandler.removeCallbacks(this);
                    mFeedForward = false;
                    break;
            }
            invalidate();
            return true;
        }

        // Runnable interface for implementing the long press
        public void run() {
            ((Vibrator)getSystemService(Context.VIBRATOR_SERVICE)).vibrate(30);
            mFeedForward = true;
            update_colors();
            invalidate();
        }
    }
}
