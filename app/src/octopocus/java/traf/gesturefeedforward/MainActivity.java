package traf.gesturefeedforward;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.gesture.Gesture;
import android.gesture.GestureLibrary;
import android.gesture.GestureLibraries;
import android.gesture.GestureStore;
import android.gesture.GestureStroke;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.media.audiofx.Visualizer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

public class MainActivity extends Activity implements DialogInterface.OnClickListener, View.OnClickListener {
    static final Random sRand = new Random();
    static final long[] sTimestamps = new long[0];
    public Visualizer nextTrialButton;
    public GestureCanvasFrame gestureCanvasFrame;

    static float hypot(float a, float b) {
        if (android.os.Build.VERSION.SDK_INT >= 17)
            return (float) Math.hypot(a, b);
        else
            return (float) Math.sqrt(a * a + b * b);
    }

    public void setCommandLabelAndInstructions(String label, String instruction) {
    }

    public void setLabelOfNextTaskButton(String labelOfNextTaskButton) {
//        this.labelOfNextTaskButton = labelOfNextTaskButton;
    }

    public void showBoxesForReview() {
    }

    public void hideBackButton() {
    }

    public void takeScreenshot() {
    }

    public void startNextTask(GestureTestTask nextTask) {
    }

    public void showBoxesForRegistration() {
    }

    public void showBackButton() {
    }

    public void startNewTrial() {
    }

    public void blinkCommandName() {
    }

    public void startNewBlock() {
    }

    public void showFFCanvas() {
    }


    class Command {
        String label;
        float[] points;
        Path path = new Path();
        float width, labelX, labelY;
        int color;
        float length;
        RectF box;
        Command(String l, float[] p) {
            label = l;
            points = p;
            color = 0x80000000 | sRand.nextInt(0x01000000);
        }
        Command(String l, int numAtoms, int[] atoms, int[] signs, double[] angles, float[] scales) {
            label = l;
            int numPoints = (numAtomSamples - 1) * numAtoms + 1;
            points = new float[numPoints * 2];
            float offsetX = points[0] = 0;
            float offsetY = points[1] = 0;

            float cosTheta;
            float sinTheta;
            int index = 2;
            int i, j;
            box = new RectF(0, 0, 0, 0);

            double lastAngle = 0;

            // Concatenate the atoms
            for (i = 0; i < numAtoms; i++) {
                cosTheta = (float) Math.cos(angles[i] + lastAngle);
                sinTheta = (float) Math.sin(angles[i] + lastAngle);
                for (j = 2; j < numAtomSamples * 2; j += 2, index += 2) {
                    points[index] = mAtoms[atoms[i]][j] * scales[i] * cosTheta - mAtoms[atoms[i]][j + 1] * scales[i] * sinTheta * signs[i] + offsetX;
                    points[index + 1] = mAtoms[atoms[i]][j] * scales[i] * sinTheta + mAtoms[atoms[i]][j + 1] * scales[i] * cosTheta * signs[i] + offsetY;
                    box.union(points[index], points[index+1]);
                }

                lastAngle = Math.atan2(points[index - 1] - points[index - 3], points[index - 2] - points[index - 4]);
                offsetX = points[index - 2];
                offsetY = points[index - 1];
            }

            // recalulate the total length from the points since some atoms may have been scaled
            length = 0;
            for (i = 2; i < numPoints * 2; i += 2) {
                length += hypot(points[i] - points[i - 2], points[i + 1] - points[i - 1]);
            }
        }
    }

    // Instances for keeping a list of existing gestures and displaying them.
    EditText mParticipantInput;
    GestureStore mStore = new GestureStore();
    Command[] mCommands;
    Constructor<GestureStroke> mGestureStroke_constructor;
    File mDirectory;
    int mGestureNum = 0;
    SimpleDateFormat mDatePattern = new SimpleDateFormat("yyyyMMdd_HHmmss");
    float mLastEventX, mLastEventY;
    Handler mHandler = new Handler();
    boolean mFeedForward = false;

    // The current stroke is stored as a raw array of points for ease of prototyping.
    int mStrokeSize;
    float[] mStroke = new float[12];
    long[] mStrokeStamps = new long[6];
    float mStrokeLength;
    RectF mStrokeBox = new RectF();
    Path mStrokePath = new Path();
    Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    double velx;
    double vely;
    float mLastStrokeLength;
    RectF mBox = new RectF();

    int numAtomSamples;
    float[][] mAtoms;
    float[][] mAtomLengths;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button b = new Button(this);
        b.setOnClickListener(this);
        b.setText("Add");
        addContentView(new DrawView(this), new LayoutParams(-1, -1));
        addContentView(b, new LayoutParams(-2, -2));

        // Load the initial set of gestures.
//        try {
//            mStore.load(getResources().openRawResource(R.raw.gestures), true);
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//        mCommands = new Command[mStore.getGestureEntries().size()];
//        int i = 0;
//        for (String s : mStore.getGestureEntries())
//            mCommands[i++] = new Command(s, mStore.getGestures(s).get(0).getStrokes().get(0).points);

        // Mandatory hack to make a GestureStroke out of an array of points.
        try {
            mGestureStroke_constructor = GestureStroke.class.getDeclaredConstructor(RectF.class, float.class, float[].class, long[].class);
            mGestureStroke_constructor.setAccessible(true);
        }
        catch (NoSuchMethodException e) {
            System.out.println(e.getMessage());
        }
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.BEVEL);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTextSize(48);

        // Display the input dialog for participant number
        mParticipantInput = new EditText(this);
        mParticipantInput.setHint("Participant number");
        mParticipantInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setView(mParticipantInput)
                .setPositiveButton("OK", this)
                .show();
        if (!Environment.getExternalStorageState().equals("mounted"))
            Toast.makeText(this, "SD card not detected", Toast.LENGTH_SHORT).show();

        float scale = 250;

        // create some atoms for composing new potential gestures
        numAtomSamples = 20;
        mAtoms = new float[6][numAtomSamples * 2];
        mAtomLengths = new float[6][numAtomSamples];
        float norm = 1f / ((float)numAtomSamples * 2f);

        // first atom is just a straight line
        mAtomLengths[0][0] = 0;
        int i, j;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[0][i] = i * norm * scale;
            mAtoms[0][i+1] = 0f * scale;
            if (i >= 2)
                mAtomLengths[0][j] = mAtomLengths[0][j-1]
                        + hypot(mAtoms[0][i] - mAtoms[0][i-2], mAtoms[0][i+1] - mAtoms[0][i-1]);
        }

        // next atom describes cosine arc from 0 – π/3
        mAtomLengths[1][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[1][i] = (float) Math.sin(i * norm * (float)Math.PI * 0.33f) * scale;
            mAtoms[1][i+1] = (1f - (float) Math.cos(i * norm * (float)Math.PI * 0.33f)) * scale;
            if (i >= 2)
                mAtomLengths[1][j] = mAtomLengths[1][j-1]
                        + hypot(mAtoms[1][i] - mAtoms[1][i-2], mAtoms[1][i+1] - mAtoms[1][i-1]);
        }

        // next atom describes cosine arc from 0 – π/2
        mAtomLengths[2][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[2][i] = (float) Math.sin(i * norm * (float)Math.PI * 0.5f) * scale * 0.75f;
            mAtoms[2][i+1] = (1f - (float) Math.cos(i * norm * (float)Math.PI * 0.5f)) * scale * 0.75f;
            if (i >= 2)
                mAtomLengths[2][j] = mAtomLengths[2][j-1]
                        + hypot(mAtoms[2][i] - mAtoms[2][i-2], mAtoms[2][i+1] - mAtoms[2][i-1]);
        }

        // next atom describes arc from 0 – 2π/3
        mAtomLengths[3][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[3][i] = (float) Math.sin(i * norm * (float)Math.PI) * scale * 0.67f;
            mAtoms[3][i+1] = (1f - (float) Math.cos(i * norm * (float)Math.PI)) * scale * 0.67f;
            if (i >= 2)
                mAtomLengths[3][j] = mAtomLengths[3][j-1]
                        + hypot(mAtoms[3][i] - mAtoms[3][i-2], mAtoms[3][i+1] - mAtoms[3][i-1]);
        }

        // next atom describes arc from 0 – 4π/3
        mAtomLengths[4][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[4][i] = (float) Math.sin(i * norm * (float)Math.PI * 1.333f) * scale * 0.5f;
            mAtoms[4][i+1] = (1f - (float) Math.cos(i * norm * (float)Math.PI * 1.333f)) * scale * 0.5f;
            if (i >= 2)
                mAtomLengths[4][j] = mAtomLengths[4][j-1]
                        + hypot(mAtoms[4][i] - mAtoms[4][i-2], mAtoms[4][i+1] - mAtoms[4][i-1]);
        }

        // next atom describes arc from 0 – 5π/3
        mAtomLengths[5][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[5][i] = (float) Math.sin(i * norm * (float)Math.PI * 1.667f) * scale * 0.5f;
            mAtoms[5][i+1] = (1f - (float) Math.cos(i * norm * (float)Math.PI * 1.667f)) * scale * 0.5f;
            if (i >= 2)
                mAtomLengths[5][j] = mAtomLengths[5][j-1]
                        + hypot(mAtoms[5][i] - mAtoms[5][i-2], mAtoms[5][i+1] - mAtoms[5][i-1]);
        }

        // build the commands
        int gestureSetIndex = 0;
        buildCommands(gestureSetIndex);
        int counter = 0;
        for (Command c : mCommands) {
            Gesture g = new Gesture();
            try {
                g.addStroke(mGestureStroke_constructor.newInstance(c.box, c.length, c.points, new long[0]));
                mStore.addGesture(c.label, g);
                Toast.makeText(this, "Created gesture for command "+counter+" '" + c.label + "'", Toast.LENGTH_SHORT).show();
            }
            catch (Exception e) {
                Toast.makeText(this, "Couldn't create gesture for command "+counter+" '"+c.label+"'", Toast.LENGTH_SHORT).show();
                System.out.println(e.getMessage());
            }
            ++counter;
        }
        java.util.Set<String> foo = mStore.getGestureEntries();
        for (String s : foo) {
            Toast.makeText(this, "Gesture '"+s+"' is in mStore", Toast.LENGTH_SHORT).show();
        }

        File GestureSets = new File(Environment.getExternalStorageDirectory(), "GestureSets");
        try {
            if (!GestureSets.exists())
                GestureSets.mkdirs();
            mStore.save(new FileOutputStream(new File(GestureSets, "Gesture_Set_"+gestureSetIndex), true));
            Toast.makeText(this, "Stored file GestureSet_"+gestureSetIndex, Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            System.out.println("Failed to save file: "+e.getMessage());
            Toast.makeText(this, "Did not store file GestureSet_"+gestureSetIndex+": "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    // OnClickListener interface for receiving the participant number.
    public void onClick(DialogInterface dialog, int which) {
        String s = mParticipantInput.getText().toString();
        mDirectory = new File(Environment.getExternalStorageDirectory(), "Participant " + s);
        Toast.makeText(this, "Welcome :)", Toast.LENGTH_SHORT).show();
    }

    // OnClickListener interface for saving the current gesture when user presses Button.
    public void onClick(View v) {
        Gesture g = new Gesture();
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize);
        try {
            g.addStroke(mGestureStroke_constructor.newInstance(mStrokeBox, mStrokeLength, pts, Arrays.copyOf(mStrokeStamps, mStrokeSize / 2)));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        String s = "Gesture " + Integer.toString(mGestureNum++);
        mStore.addGesture(s, g);
        mCommands = Arrays.copyOf(mCommands, mCommands.length + 1);
        mCommands[mCommands.length - 1] = new Command(s, pts);
        Toast.makeText(this, s + " added!", Toast.LENGTH_SHORT).show();
        if (mGestureNum == 6) {
            try {
                if (!mDirectory.exists())
                    mDirectory.mkdirs();
                mStore.save(new FileOutputStream(new File(mDirectory, mDatePattern.format(new Date()))), true);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    void buildCommands(int gestureSetIndex) {
        mCommands = new Command[6];
        int i = 0;

        switch (gestureSetIndex) {
            case 0: // Gesture Set 1
                // Cut (1 stroke)
                mCommands[i++] = new Command("cut", 4, new int[]{1, 4, 1, 0},
                        new int[]{-1, -1, -1, 1}, new double[]{Math.PI * -0.67, 0, 0, 0},
                        new float[]{1, 0.5f, 1, 0.5f});

                // Stock report (1 stroke)
                mCommands[i++] = new Command("stock report", 3, new int[]{2, 5, 2},
                        new int[]{-1, -1, -1}, new double[]{Math.PI * -1.75, 0, 0},
                        new float[]{1.2f, 0.3f, 1.2f});

                // camera on (2 strokes)
                mCommands[i++] = new Command("camera on", 2, new int[]{0, 0}, new int[]{1, 1},
                        new double[]{Math.PI * -0.25, Math.PI * 0.5}, new float[]{1f, 1.5f});

                // alarm set (3 strokes)
                mCommands[i++] = new Command("alarm set", 3, new int[]{0, 0, 0},
                        new int[]{1, 1, 1}, new double[]{Math.PI * 0.25, Math.PI * 0.5, Math.PI * 0.5},
                        new float[]{1f, 1f, 1f});

                // flashlight on (3 strokes)
                mCommands[i++] = new Command("flashlight on", 3, new int[]{0, 0, 0},
                        new int[]{1, 1, 1},
                        new double[]{Math.PI * 0.75, Math.PI * -0.85, Math.PI * -0.7},
                        new float[]{1.5f, 1f, 1f});

                // airplane mode on (4 strokes)
                mCommands[i++] = new Command("airplane mode on", 4, new int[]{0, 0, 0, 0},
                        new int[]{1, 1, 1, 1},
                        new double[]{Math.PI * -0.5, Math.PI * 0.5, Math.PI * 0.5, Math.PI * 0.5},
                        new float[]{1.8f, 0.75f, 0.75f, 1.5f});

                break;
            case 1: // Gesture Set 2
                // energy save on (1 stroke)
                mCommands[i++] = new Command("energy save on", 3, new int[]{4, 1, 0},
                        new int[]{1, -1, 1}, new double[]{Math.PI * -0.42, 0, 0},
                        new float[]{1, 0.5f, 0.5f});

                // timer set (1 stroke)
                mCommands[i++] = new Command("timer Set", 3, new int[]{3, 3, 3},
                        new int[]{-1, -1, -1}, new double[]{Math.PI * -0.5, 0, 0},
                        new float[]{1, 1, 1});

                // weather report (2 strokes)
                mCommands[i++] = new Command("weather report", 2, new int[]{0, 0}, new int[]{1, 1},
                        new double[]{Math.PI, Math.PI * -0.5}, new float[]{1f, 1.5f});

                // paste (3 strokes)
                mCommands[i++] = new Command("paste", 3, new int[]{0, 0, 0}, new int[]{1, 1, 1},
                        new double[]{Math.PI * -0.5, Math.PI * -0.5, Math.PI * -0.5},
                        new float[]{0.75f, 0.75f, 1.5f});

                // video record on (3 strokes)
                mCommands[i++] = new Command("video record on", 3, new int[]{0, 0, 0},
                        new int[]{1, 1, 1}, new double[]{Math.PI * 0.4, Math.PI * -0.8, Math.PI * 0.4},
                        new float[]{0.5f, 0.5f, 1.5f});

                // geolocation on (4 strokes)
                mCommands[i++] = new Command("geolocation on", 4, new int[]{0, 0, 0, 0},
                        new int[]{1, 1, 1, 1},
                        new double[]{Math.PI * -0.25, Math.PI * 0.5, Math.PI * -0.75, Math.PI * 0.6},
                        new float[]{1.5f, 0.5f, 0.5f, 0.5f});

                break;
            case 2: // Gesture Set 3
                // screen bright (1 stroke)
                mCommands[i++] = new Command("screen bright", 5, new int[]{0, 3, 0, 3, 0},
                        new int[]{1, 1, 1, -1, -1}, new double[]{Math.PI * -0.25, 0, 0, 0, 0},
                        new float[]{0.8f, 0.15f, 0.3f, 0.15f, 1.2f});

                // bluetooth on (1 stroke)
                mCommands[i++] = new Command("bluetooth on", 3, new int[]{1, 3, 1},
                        new int[]{-1, -1, -1}, new double[]{Math.PI * -1.24, 0, 0},
                        new float[]{1.5f, 0.8f, 1.5f});

                // search (2 strokes)
                mCommands[i++] = new Command("search", 3, new int[]{2, 2, 0},
                        new int[]{1, 1, 1}, new double[]{Math.PI * 0.04, Math.PI, 0},
                        new float[]{1.5f, 1.5f, 0.5f});

                // audio record on (3 strokes)
                mCommands[i++] = new Command("audio record on", 3, new int[]{0, 0, 0},
                        new int[]{1, 1, 1},
                        new double[]{Math.PI * -0.75, Math.PI * -0.75, Math.PI * -0.75},
                        new float[]{1.5f, 1f, 1.5f});

                // clock set (3 strokes)
                mCommands[i++] = new Command("clock set", 3, new int[]{0, 0, 0},
                        new int[]{1, 1, 1},
                        new double[]{Math.PI * 0.4, Math.PI * 0.8, Math.PI * -0.8},
                        new float[]{1f, 0.5f, 1.5f});

                // news report (4 strokes)
                mCommands[i++] = new Command("news report", 4, new int[]{0, 0, 0, 0},
                        new int[]{1, 1, 1, 1},
                        new double[]{Math.PI * -0.5, Math.PI * -0.8, Math.PI * -0.7, Math.PI * 0.7},
                        new float[]{1.8f, 1f, 0.5f, 1f});

                break;
        }
    }

    // Refreshes the paths and widths, for the feedforward.
    void updateOctopocus() {
        for (Command c : mCommands) {
            c.path.rewind();
            c.width = 0;
            int pos = 0;
            float length = 0;
            while (pos < c.points.length - 2 && length < mStrokeLength) {
                length += hypot(c.points[pos + 2] - c.points[pos], c.points[pos + 3] - c.points[pos + 1]);
                pos += 2;
            }
            if (pos >= c.points.length - 2)
                continue;

            // Create a composite stroke at the join point.
            c.path.moveTo(mStroke[mStrokeSize - 2], mStroke[mStrokeSize - 1]);
            mBox.set(mStrokeBox);
            float[] pts = Arrays.copyOf(mStroke, mStrokeSize + c.points.length - pos - 2);
            float dx = c.points[pos++] - mStroke[mStrokeSize - 2];
            float dy = c.points[pos++] - mStroke[mStrokeSize - 1];
            float len = 0;
            int dst = mStrokeSize;
            while (pos < c.points.length) {
                float X = pts[dst++] = c.points[pos++] - dx;
                float Y = pts[dst++] = c.points[pos++] - dy;
                mBox.union(X, Y);
                len += hypot(X - pts[dst - 4], Y - pts[dst - 3]);
                if (len < 400)
                    c.path.lineTo(c.labelX = X, c.labelY = Y);
            }

//            // Match the gesture with the existing commands
//            Gesture g = new Gesture();
//            try {
//                g.addStroke(mGestureStroke_constructor.newInstance(mBox, mStrokeLength + len, pts, sTimestamps));
//            }
//            catch (Exception e) {
//                System.out.println(e.getMessage());
//            }
//            double score = mStore.recognize(g).get(0).score;
//            c.width = Double.isNaN(score) ? 32 : Math.min((float)score * 2, 32);
        }
    }

    class DrawView extends View implements Runnable {
        DrawView(Context context) { super(context); }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            long t = event.getEventTime();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mStrokeSize = 4;
                    // Reallocate mStroke if necessary
                    if (mStrokeSize > mStroke.length) {
                        mStroke = Arrays.copyOf(mStroke, 2 * mStroke.length);
                        mStrokeStamps = Arrays.copyOf(mStrokeStamps, 2 * mStrokeStamps.length);
                    }
                    mStroke[0] = mLastEventX = x;
                    mStroke[1] = mLastEventY = y;
                    mStroke[2] = x + 1;
                    mStroke[3] = y + 1;
                    mLastStrokeLength = mStrokeLength = hypot(1, 1);
                    velx = vely = 0;
                    mStrokeBox.set(x, y, x + 1, y + 1);
                    mStrokePath.rewind();
                    mStrokePath.moveTo(x, y);
                    mStrokePath.lineTo(x + 1, y + 1);
                    mHandler.postDelayed(this, 500);
                    break;
                case MotionEvent.ACTION_MOVE:
                    // check if has moved more than threshold
                    float distMoved = hypot(x - mStroke[mStrokeSize - 2],
                            y - mStroke[mStrokeSize - 1]);
                    if (distMoved < 5)
                        break;
                    if (mStrokeSize >= 4) {
                        // try smoothing last point
                        // calculate midpoint between current sample and z-2
                        float mpx = (mStroke[mStrokeSize-4] + x) * 0.5f;
                        float mpy = (mStroke[mStrokeSize-3] + y) * 0.5f;

                        // interpolate between midpoint and z-1
                        mStroke[mStrokeSize-2] = mStroke[mStrokeSize-2] * 0.1f + mpx * 0.9f;
                        mStroke[mStrokeSize-1] = mStroke[mStrokeSize-1] * 0.1f + mpy * 0.9f;

                        // remove previous strokelength
                        mStrokeLength -= mLastStrokeLength;

                        // add new previous strokelength
                        mStrokeLength += hypot(mStroke[mStrokeSize-2]
                                        - mStroke[mStrokeSize-4],
                                mStroke[mStrokeSize-1]
                                        - mStroke[mStrokeSize-3]);

                        // add current strokeLength
                        mLastStrokeLength = hypot(x - mStroke[mStrokeSize-2],
                                y - mStroke[mStrokeSize-1]);
                        mStrokeLength += mLastStrokeLength;

                        velx = x - mStroke[mStrokeSize-2];
                        vely = y - mStroke[mStrokeSize-1];
                    }

                    mStrokeBox.union(x, y);
                    mStrokePath.lineTo(x, y);

                    // Reallocate mStroke if necessary
                    if (mStrokeSize + 2 > mStroke.length)
                        mStroke = Arrays.copyOf(mStroke, 2 * mStroke.length);
                    mStroke[mStrokeSize++] = x;
                    mStroke[mStrokeSize++] = y;

                    // Reset the long press when the finger moves far enough
                    if (mFeedForward) {
                        updateOctopocus();
                    }
                    else if (hypot(x - mLastEventX, y - mLastEventY) > 50) {
                        mHandler.removeCallbacks(this);
                        mHandler.postDelayed(this, 500);
                        mLastEventX = x;
                        mLastEventY = y;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    mHandler.removeCallbacks(this);
                    mFeedForward = false;
                    break;
            }
            invalidate();
            return true;
        }

        // Runnable interface for implementing the long press
        public void run() {
            ((Vibrator)getSystemService(Context.VIBRATOR_SERVICE)).vibrate(30);
            mFeedForward = true;
            updateOctopocus();
            invalidate();
        }

        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            mPaint.setColor(0x80000000);
            mPaint.setStrokeWidth(20);
            canvas.drawPath(mStrokePath, mPaint);
            if (mFeedForward) {
                for (Command c : mCommands) {
//                    if (c.width > 8) {
//                        mPaint.setColor(c.color);
                        mPaint.setColor(0x80880000);
//                        mPaint.setStrokeWidth(c.width);
                        canvas.drawPath(c.path, mPaint);
                        mPaint.setStyle(Paint.Style.FILL);
                        canvas.drawText(c.label, c.labelX, c.labelY + 12, mPaint);
                        mPaint.setStyle(Paint.Style.STROKE);
//                    }
                }
            }
        }
    }

}
