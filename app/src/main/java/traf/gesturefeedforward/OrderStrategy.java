package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 03/02/16.
 */
public abstract class OrderStrategy {
    abstract CommandLabel getCommandForTrial(int participantId,int aBlock, int aTrial, ExperimentSession exp);
}
