package traf.gesturefeedforward;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by carlagriggio on 10/26/15.
 */
abstract public class Block {
    public ExperimentSession experiment;
    public int trialNumber = 0;
    public int blockNumber;
    ArrayList<Task> trials = new ArrayList<>();

    public Task nextTask() {
        return this.trials.get(this.trialNumber);
    }

    public int getBlockNumber() {
        return blockNumber;
    }

    public ArrayList<Task> getTrials() {
        return trials;
    }

    abstract public void startBlock(MainActivity mainActivity);

    public abstract void trialFinished(MainActivity mainActivity);

    public abstract Mode currentFeedForwardType();

    public abstract String instructionsTextForCommand();

    public int getParticipantId() {
        return experiment.participantID;
    }

    public Task previousTask() {
        return this.trials.get(trialNumber-1);
    }
}
