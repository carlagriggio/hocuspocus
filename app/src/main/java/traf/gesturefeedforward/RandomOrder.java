package traf.gesturefeedforward;

import java.util.Random;

/**
 * Created by carlagriggio on 03/02/16.
 */
public class RandomOrder extends OrderStrategy {
    private final Random commandRandomizer = new Random();
    ;

    @Override
    public CommandLabel getCommandForTrial(int participantId, int aBlock, int aTrial, ExperimentSession exp) {
        int idx = commandRandomizer.nextInt(exp.commands.size());
        CommandLabel command = exp.commands.get(idx);
        exp.commands.remove(command);
        return command;
    }
}
