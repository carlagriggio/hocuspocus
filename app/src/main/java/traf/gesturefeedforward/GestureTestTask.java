package traf.gesturefeedforward;

import android.gesture.Gesture;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by carlagriggio on 11/20/15.
 */
public class GestureTestTask extends GestureRegistrationTask {

    public GestureTestTask(int trialNumber, CommandLabel command, Block block) {
        super(trialNumber, command, block);
        gestureExampleNumber = 1;
    }
    public GestureTestTask(int trialNumber, CommandLabel command, Block block, int aGestureExampleNumber) {
        super(trialNumber, command, block);
        gestureExampleNumber = aGestureExampleNumber;
    }
    public void initTask(MainActivity mainActivity) {
        GestureBuffer.put(command.label,new ArrayList<Gesture>());

        mainActivity.setCommandLabelAndInstructions(command.label,  command.instruction);
        mainActivity.setLabelOfNextTaskButton(this.textForNextTaskButton());
        mainActivity.showBoxesForReview();
        mainActivity.hideBackButton();
        mainActivity.nextTrialButton.setEnabled(false);
    }
    public String textForNextTaskButton() {
         return "TEST "+gestureExampleNumber+"/3";
    }
    public void gestureFinished(GestureCanvas aCanvas) {
        aCanvas.mainActivity.takeScreenshot();
//        this.logGesture(canvas.mStrokeSize, this.startLastGesture,System.currentTimeMillis(),canvas.gestureDistance <= canvas.gesturePerformanceFeedback.okThreashold() ? "CORRECT" : "WRONG"); //this is wrong
        this.logGesture(aCanvas.mStrokeSize, this.startLastGesture,System.currentTimeMillis(),aCanvas.rawScore >= aCanvas.gesturePerformanceFeedback.okThreashold() ? "CORRECT" : "WRONG", aCanvas.gestureLength); //this is wrong
        this.startLastGesture = 0;
        ((ArrayList<Gesture>) GestureBuffer.get(command.label)).add(0,aCanvas.lastGestureDrawn);

        if(aCanvas.mStrokeLength < 100 ) { // gestures shorter than 10px could be just the user touching the screen to preview the FF
            aCanvas.mainActivity.nextTrialButton.setEnabled(false);
        } else {
            aCanvas.mainActivity.nextTrialButton.setEnabled(true);
        }
    };

    public void finishTask(MainActivity mainActivity) {
        this.end();
        if(gestureExampleNumber == 3) { //3 is the amount of samples of the same command we want to test. It was 1 before.
            block.trialFinished(mainActivity);
        } else {
            GestureTestTask nextTask = new GestureTestTask(this.trialNumber,this.command,this.block, gestureExampleNumber +1);
            mainActivity.startNextTask(nextTask);
            nextTask.setPreviousTask(this);
        }
    }

    protected String taskNameForLogger() {
        return "T"; //+String.valueOf(gestureExampleNumber);
    }

    public boolean canRedraw() {
        if(((ArrayList<Gesture>) GestureBuffer.get(command.label)).size()>0) {
            Log.i("LastGestureDrawnLength", String.valueOf(((ArrayList<Gesture>) GestureBuffer.get(command.label)).get(0).getLength()));
            return ((ArrayList<Gesture>) GestureBuffer.get(command.label)).size() < 1 || ((ArrayList<Gesture>) GestureBuffer.get(command.label)).get(0).getLength() < 100; //gestures shorter than 100px are likely to be mistakes
        } else {
            return ((ArrayList<Gesture>) GestureBuffer.get(command.label)).size() < 1;
        }
    }
}


