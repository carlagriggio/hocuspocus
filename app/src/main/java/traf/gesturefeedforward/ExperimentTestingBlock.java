package traf.gesturefeedforward;

import android.app.AlertDialog;
import android.content.DialogInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by carlagriggio on 10/16/15.
 */
public class ExperimentTestingBlock extends ExperimentBlock {
    private Mode blockFeedforwardType;

    public ExperimentTestingBlock(ExperimentBlock experimentBlock) {
        super();
        blockFeedforwardType = experimentBlock.currentFeedForwardType();
        this.experiment = experimentBlock.experiment;
        this.numberOfTrials = experimentBlock.numberOfTrials;
        this.blockNumber = experimentBlock.blockNumber;
        ArrayList<Integer> positionsExtremes = new ArrayList<Integer>();
        ArrayList<Integer> positionsMiddle = new ArrayList<Integer>();
        Task[] testTrials = new Task[numberOfTrials];
        Random commandRandomizer = new Random();

        positionsMiddle.add(numberOfTrials/2);
        positionsMiddle.add(numberOfTrials/2+1);
        positionsMiddle.add(numberOfTrials/2+2);
        positionsMiddle.add(numberOfTrials/2-1);

        for (int i=0; i<numberOfTrials/2-1; i++) {
            positionsExtremes.add(i);
        }
        for (int i=(numberOfTrials/2+3); i<numberOfTrials; i++) {
            positionsExtremes.add(i);
        }
        //takes the first 2 and last 2 trials and puts them in a random position of the 4 test middle trials
        //the rest of the trials are randomly assigned to the rest of positions
        for (int i=0; i<numberOfTrials; i++) {
            Task t = experimentBlock.trials.get(i);
            //testTrials.add(new GestureTestTask(t.trialNumber, t.command, this));
            if (i < 2 || i >= this.numberOfTrials - 2)
                testTrials[positionsMiddle.remove(commandRandomizer.nextInt(positionsMiddle.size()))] = new GestureTestTask(t.trialNumber, t.command, this);
            else
                testTrials[positionsExtremes.remove(commandRandomizer.nextInt(positionsExtremes.size()))] = new GestureTestTask(t.trialNumber, t.command, this);
        }
        for (int i=0; i<numberOfTrials; i++) {
            trials.add(testTrials[i]);
        }
    }

    public ExperimentTestingBlock() {

    }

    public void trialFinished(final MainActivity mainActivity) {
        trialNumber = trialNumber + 1;
        if(this.trialNumber == this.numberOfTrials) {
            new AlertDialog.Builder(mainActivity)
                    .setTitle("BLOCK #"+blockNumber+" COMPLETE")
                    .setMessage("\n\nPlease SHOW THIS SCREEN to the experimenter before continuing. \n\n")
                    .setPositiveButton("START", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mainActivity.startNewBlock();
                        }
                    })
                    .setCancelable(false)
                    .show();
        } else {
            mainActivity.startNewTrial();
        }
    }
    public Mode currentFeedForwardType() {
        //should take it from the counterbalancing
        return blockFeedforwardType;
    }

    public void startBlock(final MainActivity mainActivity) {
        new AlertDialog.Builder(mainActivity)
                .setTitle("TEST")
                .setMessage("Perform each of the gestures you created 3 times as well as you can remember.")
                .setPositiveButton("START", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //startNewCreatedGestureEvaluationTrial();
//                        mainActivity.hideScore();
//                        mainActivity.showDetectedCommand();
                        mainActivity.startNewTrial();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public String textForNextTaskButton() {
        return "NEXT COMMAND";
    }
    public String instructionsTextForCommand() {
        return "Draw the gesture you created for: ";
    }


}
