package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 03/02/16.
 */
public class NoFFExperimentBlock extends ExperimentBlock {
    public NoFFExperimentBlock(ExperimentSession anExperiment, int aNumberOfTrials, BlockingStrategy aBlockingStrategy) {
        experiment = anExperiment;
        numberOfTrials = aNumberOfTrials;
        blockNumber = anExperiment.numberOfBlocks() +1;
        blockingStrategy = aBlockingStrategy;


        for(int i=0; i<this.numberOfTrials; i++) {
            trials.add(new GestureGenerationWithRegistrationTask(i +1, experiment.getCommandForGestureCreation(i+1, blockNumber),this));
        }
    }
}
