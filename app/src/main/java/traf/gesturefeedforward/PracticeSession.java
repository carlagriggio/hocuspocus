package traf.gesturefeedforward;

import android.gesture.GestureStore;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by carlagriggio on 10/30/15.
 */
public class PracticeSession extends ExperimentSession {

    public PracticeSession(int participantID, GestureStore aGestureStore) {
        super(participantID);
        gestureStore = aGestureStore;
    }

    public PracticeSession(int participantID) {
        super(participantID);
    }

    protected void createCommands() {
        Collections.addAll(this.commands, new CommandLabel[]{
                new CommandLabel("Open Maps", "Open the Maps application"),
//                new CommandLabel("Phone vibrate", "Turn the phone ringer to only vibrate"),
//                new CommandLabel("Bookmark page", "Bookmark this page in my web browser"),
                new CommandLabel("Set timer", "Start a 10-minutes timer"),
                new CommandLabel("Weather report", "Show the weather report for today"),
//                new CommandLabel("Calendar today", "Show my agenda for today on my calendar"),
                new CommandLabel("Stop music", "Stop playing music"),
//                new CommandLabel("Take selfie", "Open the Camera app using the front camera of the smartphone"),
//                new CommandLabel("Airplane-mode On", "Turn Airplane mode On")


                //commands for protocol 3.5:
//                new CommandLabel("Open Maps", "Open the Maps application"),
//                new CommandLabel("Phone vibrate", "Turn the phone ringer to only vibrate"),
//                new CommandLabel("Bookmark page", "Bookmark this page in my web browser"),
//                new CommandLabel("Set timer", "Start a 10-minutes timer"),
//                new CommandLabel("Weather report", "Show the weather report for today"),
//                new CommandLabel("Calendar today", "Show my agenda for today on my calendar"),
//                new CommandLabel("Stop music", "Stop playing music"),
//                new CommandLabel("Take selfie", "Open the Camera app using the front camera of the smartphone"),
//                new CommandLabel("Airplane-mode On", "Turn Airplane mode On")



                //new CommandLabel("Track running", "Start tracking my running session with my favorite Sports app"),
                //new CommandLabel("Flashlight On", "Turn on my phone's flashlight"),
                //new CommandLabel("Set timer", "Set a 10 minutes timer starting now"),
                //new CommandLabel("Facebook song", "Share on Facebook the song I'm currently listening to"),
//                new CommandLabel("Count calories", "Register a meal on my calorie counter app at the current time of the day"),
//                new CommandLabel("Flight check-in", "Open the check-in online form for my upcoming flight"),
//                new CommandLabel("Supermarket List", "Open my supermarket grocery list"),

                //new CommandLabel("Text friend", "Write a new SMS to my best friend"),
        });
    }
    protected void createBlocks() {
        this.blocks.add(new PracticeBlock(this,3,new SimpleBlockingStrategy()));
        this.blocks.add(new PracticeBlock(this,3,new SimpleBlockingStrategy()));
        this.blocks.add(new PracticeBlock(this, 3, new SimpleBlockingStrategy()));
        //this.blocks.add(new FinalTestingBlock(this));
    }

    @Override
    public void addBlock(int trials, BlockingStrategy aBlockingStrategy) {
        this.blocks.add(new PracticeBlock(this, trials, aBlockingStrategy));
    }

    @Override
    protected void lastBlockFinished() throws PracticeSessionOverException, NoMoreExperimentBlocksException {
        throw new PracticeSessionOverException();
    }

    @Override
    protected String sessionPrefix() {
        return "pra";
    }
}
