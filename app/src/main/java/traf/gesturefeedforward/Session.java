package traf.gesturefeedforward;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Session {
	private static Session instance = null;
	private String ipAddress = "192.168.0.8";
	private int port = 43211;
    private DatagramSocket s;

   
	protected Session() {
		   try {
				s = new DatagramSocket();
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
   
	public static Session getInstance() {
		if(instance == null) {
			instance = new Session();
		}
		return instance;
	}
   
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void sendMessage(String messageStr) {
		int msg_length=messageStr.length();
		byte[] message = messageStr.getBytes();
		Log.i("IP",this.getIpAddress());
		DatagramPacket p;
		if((s != null) && s.isConnected()) {
			p = new DatagramPacket(message, msg_length);
			Log.i("MSG",messageStr);
			try {	
				s.send(p);
			} catch (IOException e) {
//				e.printStackTrace();
				this.openConnection();
			}
		}
	}

	public void closeConnection() {
        s.close();
	}

	public void openConnection() {
		final String ip = this.getIpAddress();
		final int thePort = this.getPort();
		new Thread(new Runnable() {
	        public void run() {	        	
	        	try {
	    			s.connect(InetAddress.getByName(ip), thePort);
	    		} catch (UnknownHostException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}		  	  
	        }
	    }).start();
	}
}