package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 03/02/16.
 */
public class IncrementalOrder extends OrderStrategy {
    private int commandNumber = -1;

    @Override
    CommandLabel getCommandForTrial(int participantId, int aBlock, int aTrial, ExperimentSession exp) {
        commandNumber = commandNumber+1;
        return exp.commands.get(commandNumber);
    }
}
