package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 03/02/16.
 */
public class Counterbalanced3x6Order extends OrderStrategy {
    private int commandNumber = -1;
    private int commandIndexInCategory;

    @Override
    CommandLabel getCommandForTrial(int participantId, int aBlock, int aTrial, ExperimentSession exp) {
        int commandNumberIndex = (aBlock -1) * 2;
        if(aTrial > 6) {
            commandNumberIndex = commandNumberIndex+1;
        }
        commandIndexInCategory = this.counterbalancingCommandNumber()[participantId-1][commandNumberIndex];

        commandNumber = (this.counterbalancingCategoriesOrder()[(participantId-1)*3+(aBlock-1)][aTrial-1]-1)*6+ commandIndexInCategory;

        return exp.commands.get(commandNumber-1);
    }

    protected int[][] counterbalancingCommandNumber() {
        return new int[][]{{1, 2, 6, 3, 5, 4},{4, 6, 5, 2, 3, 1},{6,3,5,1,4,2},{5,3,6,2,1,4},{1,3,6,4,2,5},{1,5,3,6,2,4} //participants 1-6
        };
    }
    protected int[][] counterbalancingCategoriesOrder() {
        return new int[][]{{3, 4, 2, 5, 1, 6, /**/ 4, 5, 3, 6, 2, 1}, {6, 1, 5, 2, 4, 3, /**/ 2, 3, 1, 4, 6, 5}, {5, 6, 4, 1, 3, 2, /**/1, 2, 6, 3, 5, 4,},
                {4, 5, 3, 6, 2, 1, /**/ 5, 6, 4, 1, 3, 2}, {6, 1, 5, 2, 4, 3, /**/ 1, 2, 6, 3, 5, 4}, {2, 3, 1, 4, 6, 5, /**/ 3, 4, 2, 5, 1, 6},
                {1, 2, 6, 3, 5, 4, /**/ 6, 1, 5, 2, 4, 3}, {3, 4, 2, 5, 1, 6, /**/ 2, 3, 1, 4, 6, 5}, {5, 6, 4, 1, 3, 2, /**/ 4, 5, 3, 6, 2, 1},
                {4, 5, 3, 6, 2, 1, /**/ 1, 2, 6, 3, 5, 4}, {6, 1, 5, 2, 4, 3, /**/ 3, 4, 2, 5, 1, 6}, {5, 6, 4, 1, 3, 2, /**/ 2, 3, 1, 4, 6, 5},
                {1, 2, 6, 3, 5, 4, /**/ 4, 5, 3, 6, 2, 1}, {3, 4, 2, 5, 1, 6, /**/ 6, 1, 5, 2, 4, 3}, {2, 3, 1, 4, 6, 5, /**/5, 6, 4, 1, 3, 2},
                {3, 4, 2, 5, 1, 6, /**/ 1, 2, 6, 3, 5, 4}, {6, 1, 5, 2, 4, 3, /**/ 4, 5, 3, 6, 2, 1}, {5, 6, 4, 1, 3, 2, /**/ 2, 3, 1, 4, 6, 5}
        };
    }
}
