package traf.gesturefeedforward;

import android.gesture.Gesture;
import android.graphics.Color;

import java.util.ArrayList;

/**
 * Created by carlagriggio on 03/02/16.
 */
public class GestureGenerationWithRegistrationTask extends GestureGenerationTask {
    public GestureGenerationWithRegistrationTask(int trialNumber, CommandLabel command, Block block) {
        super(trialNumber, command, block);
        GestureRegistrationTask.GestureBuffer.put(command.label, new ArrayList<Gesture>());

    }

    public String textForNextTaskButton() {
        return "REGISTER 1/3";
    }
    public void initTask(MainActivity mainActivity) {
        super.initTask(mainActivity);
        mainActivity.gestureCanvasFrame.isTimed = false;
        mainActivity.gestureCanvasFrame.textIndicator.setText("CREATE A NEW GESTURE");
        mainActivity.nextTrialButton.setEnabled(false);
        mainActivity.showFFCanvas();

    }
    public void finishTask(MainActivity mainActivity) {
        this.end();
        ArrayList<Gesture> buffer = (ArrayList<Gesture>) GestureRegistrationTask.GestureBuffer.get(command.label);
        buffer.add(mainActivity.gestureFromFFCanvas());

        GestureRegistrationTask nextTask = new GestureRegistrationTask(this.trialNumber,this.command,this.block,2);
        mainActivity.startNextTask(nextTask);
        nextTask.setPreviousTask(this);
    }
}
