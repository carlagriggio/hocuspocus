package traf.gesturefeedforward;

import android.annotation.TargetApi;
import android.content.Context;
import android.gesture.Gesture;
import android.gesture.GestureStore;
import android.gesture.GestureStroke;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.util.FloatMath;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Constructor;
import java.util.Arrays;

/**
 * Created by carlagriggio on 11/18/15.
 */

class GestureCanvasFrame extends FrameLayout {
    protected GestureCanvas gestureCanvas;
    protected TextView textIndicator;
    public boolean isTimed = false;
    private Handler mHandler = new Handler();
    protected TextView timeIndicator;
    protected long mStartTime;

    public void startTimer() {
        if(isTimed)
            mUpdateTimeTask.run();
    }
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            final long start = mStartTime;
            long millis = SystemClock.uptimeMillis() - start;
            int seconds = (int) (millis / 1000);
            if (seconds < 60) {
                if(seconds > 50)
                    timeIndicator.setText("0:0" + (60 - seconds));
                else
                    timeIndicator.setText("0:" + (60 - seconds));
                mHandler.postAtTime(this,
                        start + ((seconds + 1) * 1000));
            } else {
                //goToNextTask();
                ((GestureCanvasWithFF) gestureCanvas).renderer.mFeedForward = false;

                timeIndicator.setText("0:00");
                timeIndicator.setTextColor(Color.RED);
                if(gestureCanvas.currentTask.startLastGesture > 0) //the user was drawing when the canvas timed out
                    gestureCanvas.currentTask.timeOut(gestureCanvas);
                removeGestureCanvas();
            }
        }
    };
    private void removeGestureCanvas() {
        removeView(gestureCanvas);
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public GestureCanvasFrame(Context context) {
        super(context);


        textIndicator = new TextView(context);
        textIndicator.setText("");
        textIndicator.setTextSize(30.0f);
        textIndicator.setY(400);
        //commandName.setId(1);
        textIndicator.setGravity(Gravity.CENTER_HORIZONTAL);
        textIndicator.setPadding(0, 20, 20, 0);
        textIndicator.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));

        addView(textIndicator);
        //this.resetCanvas(context);

    }

    public void resetCanvas(Context context) {
        removeView(gestureCanvas);
        gestureCanvas = new GestureCanvas(context);
        addView(gestureCanvas);
        gestureCanvas.setTextIndicator(textIndicator);
        textIndicator.setText("");
        setRegistrationFeedback();
    }


    public void setReviewFeedback() {
        gestureCanvas.setGesturePerformanceFeedback(new TestingGestureFeedback());
    }

    public void setRegistrationFeedback() {
        gestureCanvas.setGesturePerformanceFeedback(new CreationGestureFeedback());
    }
}

class GestureCanvas extends View implements Runnable {
    protected final GestureStore gestureStore;
    public final MainActivity mainActivity;
    // The current stroke is stored as a raw array of points for ease of prototyping
    int mStrokeSize;
    float[] mStroke = new float[12];
    float mStrokeLength;
    RectF mStrokeBox = new RectF();
    Path mStrokePath = new Path();
    Paint mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    double mScore;
    String mScoreName;
    double velx;
    double vely;
    float mLastStrokeLength;
    double angle;
    RectF mBox;
    float mLastEventX, mLastEventY;
    //    Handler mHandler = new Handler();
    float mNextLength;
    Paint mPaint;
    float gestureLength;

    long[] mStrokeStamps = new long[6];
    ExperimentSession experiment;
    protected Mode currentFF;
    protected Task currentTask;

    Constructor<GestureStroke> mGestureStroke_constructor;
    protected TextView textIndicator;
    protected GesturePerformanceFeedback gesturePerformanceFeedback = new NoGestureFeedback();
    private TextView timeIndicator;

    protected Gesture lastGestureDrawn = null;
    public double rawScore;
    public double gestureDistance;
//    private TextView gestureLengthIndicator;


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    GestureCanvas(Context context) {
        super(context);

        mainActivity = (MainActivity) context;
        experiment = mainActivity.experiment;
        currentFF = experiment.currentBlock.currentFeedForwardType();
        currentTask = experiment.getCurrentTask();
        gestureStore = experiment.getGestureStore();


        mStrokePaint.setStrokeCap(Paint.Cap.ROUND);
        mStrokePaint.setStrokeJoin(Paint.Join.BEVEL);
        mStrokePaint.setStrokeWidth(20);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mPaint = new Paint(mStrokePaint);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTextSize(48);
        mBox = new RectF();

        // Mandatory hack to make a GestureStroke out of an array of points.
        try {
            mGestureStroke_constructor = GestureStroke.class.getDeclaredConstructor(RectF.class, float.class, float[].class, long[].class);
            mGestureStroke_constructor.setAccessible(true);
        }
        catch (NoSuchMethodException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if(!this.canRedraw())
                    break;

                currentTask.newGestureStarted();
                gesturePerformanceFeedback.reset(this);

                mStrokeSize = 4;
                mStroke[0] = mLastEventX = x;
                mStroke[1] = mLastEventY = y;
                mStroke[2] = x + 1;
                mStroke[3] = y + 1;
                mLastStrokeLength = mStrokeLength = hypot(1, 1);
                velx = vely = 0;
                mStrokeBox.set(x, y, x + 1, y + 1);

                mStrokePath.rewind();
                mStrokePath.moveTo(x, y);
                mStrokePath.lineTo(x + 1, y + 1);
                mScore = -1;
//                    mHandler.postDelayed(this, 500);
                this.run();

                currentTask.logGestureFragment("TOUCH_DOWN", x, y);
                break;
            case MotionEvent.ACTION_MOVE:
                if(!this.canRedraw())
                    break;
                this.actionMove(x,y);
                break;
            case MotionEvent.ACTION_UP:
                if(!this.canRedraw())
                    break;
                this.saveLastGesture();
                this.actionUp(x, y);

                Log.i("rawScore", String.valueOf(rawScore));
                Log.i("gestureDistance", String.valueOf(gestureDistance));
                break;
        }
        invalidate();
        return true;
    }

    private boolean canRedraw() {
        return currentTask.canRedraw() || (!currentTask.canRedraw() && lastGestureDrawn == null);
    }


    // Runnable interface for implementing the long press
    public void run() {
//        if (mode == Mode.NO_FF)
//            return;

//        ((Vibrator)getSystemService(Context.VIBRATOR_SERVICE)).vibrate(30);
//        mFeedForward = true;
//        switch (mode) {
////                case OCTOPOCUS_FF:
////                    updateOctopocus();
////                    break;
//            case PATH_FF:
//                appendPathCandidates(4, 4);
//                updatePathCandidates();
//                break;
//            case FIELD_FF:
//                updateField();
//                break;
//        }

        updateStroke();
        invalidate();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        Paint paint = new Paint();
        Rect r = new Rect(this.getLeft(), this.getTop(), this.getWidth(), this.getHeight());

        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(3f);
        canvas.drawRect(r, paint);

//        if (mode == Mode.NO_FF || mode == Mode.OCTOPOCUS_FF)
            mStrokePaint.setColor(0xff222222);
//        else
////                mStrokePaint.setColor(mScore < 0 ? 0xff222222 : (mScore * 0x00020102) ^ 0xff007fff);
//            mStrokePaint.setColor(mScore < 0 ? 0xff222222 : ((int) mScore * 0x00020102) ^ 0xff007fff); //TODO: change to double


        canvas.drawPath(mStrokePath, mStrokePaint);
//        if (mFeedForward) {
//            if (mode == Mode.OCTOPOCUS_FF) {
//                for (Command c : mCommands) {
//                    if (c.width > 8) {
//                        mPaint.setColor(0x80880000);
//                        mPaint.setStrokeWidth(c.width);
//                        canvas.drawPath(c.path, mPaint);
//                        mPaint.setStyle(Paint.Style.FILL);
//                        canvas.drawText(c.label, c.labelX, c.labelY + 12, mPaint);
//                        mPaint.setStyle(Paint.Style.STROKE);
//                    }
//                }
//            }
//            else if (mode == Mode.PATH_FF) {
//                for (Candidate c : mCandidates) {
//                    if (c == null || c.active == false)
//                        continue;
//                    mPaint.setColor((c.score * 0x00020102) ^ 0x80007fff);
//                    canvas.drawPath(c.opaquePath, mPaint);
//                    mPaint.setStyle(Paint.Style.FILL);
//                    canvas.drawText(c.label, c.labelX, c.labelY + 12, mPaint);
//                    mPaint.setStyle(Paint.Style.STROKE);
//                    mPaint.setColor((c.score * 0x00020102) ^ 0x20007fff);
//                    canvas.drawPath(c.translucentPath, mPaint);
//                }
//            }
//        }

    }

    double convertScoreToDistance(double score) {
        // score returned by Android gesture learner is a weight equal to 1/cosineDistance

        // convert to cosine distance
        double cosDst = (score == 0 || Double.isNaN(score)) ? Double.MAX_VALUE : 1/score;
        // convert to angular distance
        double angDst = 2 * Math.acos(1 - cosDst) / Math.PI;
        return angDst;
    }

    void updateStroke() {
        // check score for current stroke
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize - 1);
        Gesture g = new Gesture();
        try {
            g.addStroke(mGestureStroke_constructor.newInstance(mStrokeBox, mStrokeLength, pts, new long[0]));
            rawScore = gestureStore.recognize(g).get(0).score;
            gestureDistance = convertScoreToDistance(rawScore);
            mScore = Math.min(rawScore * (127 / (GesturePerformanceFeedback.OkThreashold * 2)), 127);
            mScoreName = gestureStore.recognize(g).get(0).name;
            currentTask.saveLastPredictionResults(gestureStore.recognize(g));
        }
        catch (Exception e) {
            Log.e("Update Stroke", e.getMessage());
            mScore = 0;
        }
    }

    static float hypot(float a, float b) {
        if (Build.VERSION.SDK_INT >= 17)
            return (float) Math.hypot(a, b);
        else
            return (float) Math.sqrt(a * a + b * b);
    }

    public void setTextIndicator(TextView textIndicator) {
        this.textIndicator = textIndicator;
        gesturePerformanceFeedback.reset(this);
    }

    public void setGesturePerformanceFeedback(GesturePerformanceFeedback gesturePerformanceFeedback) {
        this.gesturePerformanceFeedback = gesturePerformanceFeedback;
    }

    public GesturePerformanceFeedback getGesturePerformanceFeedback() {
        return gesturePerformanceFeedback;
    }
    protected void actionMove(float x,float y) {
        // check if has moved more than threshold
        float distMoved = hypot(x - mStroke[mStrokeSize - 2],
                y - mStroke[mStrokeSize - 1]);
        if (distMoved < 5)
            return;
        if (mStrokeSize >= 4) {
            // try smoothing last point
            // calculate midpoint between current sample and z-2
            float mpx = (mStroke[mStrokeSize-4] + x) * 0.5f;
            float mpy = (mStroke[mStrokeSize-3] + y) * 0.5f;

            // interpolate between midpoint and z-1
            mStroke[mStrokeSize-2] = mStroke[mStrokeSize-2] * 0.1f + mpx * 0.9f;
            mStroke[mStrokeSize-1] = mStroke[mStrokeSize-1] * 0.1f + mpy * 0.9f;

            // remove previous strokelength
            mStrokeLength -= mLastStrokeLength;

            // add new previous strokelength
            mStrokeLength += hypot(mStroke[mStrokeSize-2]
                            - mStroke[mStrokeSize-4],
                    mStroke[mStrokeSize-1]
                            - mStroke[mStrokeSize-3]);

            // add current strokeLength
            mLastStrokeLength = hypot(x - mStroke[mStrokeSize-2],
                    y - mStroke[mStrokeSize-1]);
            mStrokeLength += mLastStrokeLength;

            velx = x - mStroke[mStrokeSize-2];
            vely = y - mStroke[mStrokeSize-1];
        }

        mStrokeBox.union(x, y);
        mStrokePath.lineTo(x, y);

        // Reallocate mStroke if necessary
        if (mStrokeSize + 2 > mStroke.length)
            mStroke = Arrays.copyOf(mStroke, 2 * mStroke.length);
        mStroke[mStrokeSize++] = x;
        mStroke[mStrokeSize++] = y;

        Log.i("StrokeLength", String.valueOf(mStrokeLength));
        updateStroke();
        //
        currentTask.logGestureFragment("TOUCH_MOVE", x, y);
    }
    protected void actionUp(float x, float y) {
        gesturePerformanceFeedback.updateTextIndicator(this);
//        this.updateGestureLengthFeedback();
        gestureLength = lastGestureDrawn.getLength();
        currentTask.logGestureFragment("TOUCH_UP", x, y);
        currentTask.gestureFinished(this);
    }

    public void setTimeIndicator(TextView timeIndicator) {
        this.timeIndicator = timeIndicator;
    }


    public Gesture drawnGesture() {

        return lastGestureDrawn;
    }

    public void saveLastGesture() {
        Gesture g = new Gesture();
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize);
        try {
            g.addStroke(mGestureStroke_constructor.newInstance(mStrokeBox, mStrokeLength, pts, Arrays.copyOf(mStrokeStamps, mStrokeSize / 2)));
            lastGestureDrawn = g;
            Log.i("BufferedGesture", currentTask.command.label);

            //TODO: registration counter ++
        }
        catch (Exception e) {
            Log.i("Failed saveLastGesture", currentTask.command.label);
        }
    }

//    protected void updateGestureLengthFeedback() {
//        double score = Math.abs((mStrokeSize/2));
//        gestureLengthIndicator.setText(String.format("%1$,.0f", (score < 300) ? Math.abs((score/300*100)-100) : 0)  + "%");
//    }
}
