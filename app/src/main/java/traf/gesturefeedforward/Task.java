package traf.gesturefeedforward;

import android.gesture.Prediction;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by carlagriggio on 9/22/15.
 */
public abstract class Task {
    public final CommandLabel command;
    protected final Block block;
    protected Mode feedforward;
    protected int GestureSet;
    long endTime;
    long startTime;
    long startLastGesture = 0; //milliseconds
    int numberOfGesturesTried = 0;
    int trialNumber;
//    Map<String, Double> lastAttempts = new HashMap<>();
    int participantID;
    private GestureGenerationTask previousTask;
    private ArrayList<Prediction> lastPredictionResults;
    static String logFilePath;

    public Task(Mode feedForwardType, CommandLabel command, int number, Block aBlock, int participantId) {
        this.feedforward = feedForwardType;
        this.command = command;
        this.trialNumber = number;
        participantID = participantId;
        block = aBlock;

    }


    public void start(MainActivity mainActivity) {
        startTime = System.currentTimeMillis();
        this.initTask(mainActivity);
    }

    protected abstract void initTask(MainActivity activity);


    public void newGestureStarted() {
        //this.lastAttempts = new HashMap<>();
        this.startLastGesture = System.currentTimeMillis();
        this.numberOfGesturesTried = this.numberOfGesturesTried + 1;
    }


//    public void addLastAttempt(String commandLabel, double recognitionScore) {
//        this.lastAttempts.put(commandLabel, recognitionScore);
//    }


    public void logGestureFragment(final String action, final float x, final float y) {
        new Thread(new Runnable() {
            public void run() {
                String log = String.valueOf(participantID).concat(",");    //participant ID
                log = log.concat("BL").concat(String.valueOf(block.blockNumber)).concat(",");    //block number
                log = log.concat(taskNameForLogger() == "T" ? "Test" : "Create").concat(",");
                log = log.concat(feedforward.equals(Mode.FIELD_FF) ? "Field" : (feedforward.equals(Mode.PATH_FF) ? "Path" : "None")).concat(",");
                log = log.concat("TN").concat(String.valueOf(trialNumber)).concat(",");    //trial - command number
                log = log.concat("SS").concat(String.valueOf(block.experiment.gestureStore.getGestureEntries().size())).concat(",");    //set size
                log = log.concat(String.valueOf(command.category)).concat(",");    //trial - command label
                log = log.concat(String.valueOf(command.number)).concat(",");    //trial - command label
                log = log.concat(String.valueOf(command.commandCode())).concat(",");    //trial - command label
                log = log.concat(String.valueOf(command.label)).concat(",");    //trial - command label
                log = log.concat(String.valueOf(taskNameForLogger())).concat(",");    //generation, registration or test
                log = log.concat("GT").concat(String.valueOf(numberOfGesturesTried)).concat(",");
                log = log.concat(String.valueOf(System.currentTimeMillis())).concat(","); //timestamp
//                log.concat(String.valueOf(endTime - startTime)).concat(",");
                log = log.concat(action).concat(",");
                log = log.concat(String.valueOf(x)).concat(",");
                log = log.concat(String.valueOf(y)).concat(",");
//                Iterator<Map.Entry<String, Double>> iterator = lastAttempts.entrySet().iterator();
//                while (iterator.hasNext()) {
//                    Map.Entry<String, Double> pair = iterator.next();
//                    Double value =  pair.getValue();
//                    log = log.concat(pair.getKey().replace(" ", "-")).concat(",");
//                    log = log.concat(String.valueOf(pair.getValue())).concat(",");
//                }
                if(lastPredictionResults != null) {
                    for(Prediction prediction: lastPredictionResults) {
                        try {
                            log = log.concat(String.valueOf(block.experiment.getCommandCodeWithLabel(prediction.name))).concat(",");
                            log = log.concat(String.valueOf(prediction.score)).concat(",");
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                        //log = log.concat(String.valueOf(convertScoreToDistance(lastPredictionResults.get(0).score)));
                    }
                }
                    //log = log.concat("ENDOFLOG");

                //Session.getInstance().sendMessage(log);
                logOnFile(block.experiment.logFileName()+"-cin",log);
            }
        }).start();
    }

    public void logGesture(final double strokeLength, final long gestureStartTime, final long gestureEndTime, final String comments, final float gestureLength) {
        new Thread(new Runnable() {
            public void run() {
                String log = String.valueOf(participantID).concat(",");    //participant ID
                log = log.concat("BL").concat(String.valueOf(block.blockNumber)).concat(",");    //block number
                log = log.concat(taskNameForLogger() == "T" ? "Test" : "Create").concat(","); // block type
                log = log.concat(feedforward.equals(Mode.FIELD_FF) ? "Field" : (feedforward.equals(Mode.PATH_FF) ? "Path" : "None")).concat(",");
                log = log.concat("TN").concat(String.valueOf(trialNumber)).concat(",");    //trial - command number
                log = log.concat("SS").concat(String.valueOf(block.experiment.gestureStore.getGestureEntries().size())).concat(",");    //set size
                log = log.concat(String.valueOf(command.category)).concat(",");    //trial - command label
                log = log.concat(String.valueOf(command.number)).concat(",");    //trial - command label
                log = log.concat(String.valueOf(command.commandCode())).concat(",");    //trial - command label
                log = log.concat(String.valueOf(command.label)).concat(",");    //trial - command label
                log = log.concat(String.valueOf(taskNameForLogger())).concat(",");    //generation, registration or test
                log = log.concat("GT").concat(String.valueOf(numberOfGesturesTried)).concat(",");
//                log = log.concat(String.vaxqlueOf(System.currentTimeMillis())).concat(","); //timestamp
                log = log.concat(String.valueOf(startTime)).concat(","); //task start time
                log = log.concat(String.valueOf(gestureStartTime)).concat(","); //gesture start time
                log = log.concat(String.valueOf(gestureEndTime - gestureStartTime)).concat(","); //task start time
                log = log.concat(String.valueOf(Math.ceil(strokeLength))).concat(",");
                //top predicted gesture name and score
                if(lastPredictionResults != null) {
                    log = log.concat(String.valueOf(block.experiment.getCommandCodeWithLabel(lastPredictionResults.get(0).name))).concat(",");
                    log = log.concat(String.valueOf(lastPredictionResults.get(0).score)).concat(",");
                    //log = log.concat(String.valueOf(convertScoreToDistance(lastPredictionResults.get(0).score))).concat(",");
                    if(lastPredictionResults.get(0).score >= GesturePerformanceFeedback.OkThreashold)
                        log = log.concat(taskNameForLogger() == "T" ? (command.label.equals(lastPredictionResults.get(0).name) ? "1" : "0") : "0"); // for test tasks, if the drawn gesture is recognized it means that the task was successful; for the creation tasks, it' a fail
                    else
                        log = log.concat(taskNameForLogger() == "T" ? "0" : "1");
                }
//                else {
//                    log = log.concat("null,").concat("null,").concat("null,");
//                }
                //log = log.concat(String.valueOf(Math.ceil(gestureLength))).concat(",");

                //log = log.concat(comments);

                //Session.getInstance().sendMessage(log);
                logOnFile(block.experiment.logFileName()+"-stat",log);
            }
        }).start();
    }

    public abstract String textForNextTaskButton();

    public void gestureFinished(GestureCanvas canvas) {
        canvas.mainActivity.takeScreenshot();
        this.logGesture(canvas.mStrokeLength, this.startLastGesture,System.currentTimeMillis(),"", canvas.gestureLength);
        this.startLastGesture = 0;
    };
    public void timeOut(GestureCanvas canvas) {
        this.logGesture(canvas.mStrokeLength,this.startLastGesture, System.currentTimeMillis(),"TIMEOUT", canvas.gestureLength);
        this.logGestureFragment("TIMEOUT",0,0);
    }

    public void end() {
        this.endTime = System.currentTimeMillis();
    };

    public void logOnFile(String filesuffix, String text)
    {
//        File logFile = new File(logFilePath,"log-"+filesuffix+".txt");
        File logFile = new File(this.block.experiment.fileLocation()+"/log-"+filesuffix+".txt");
        Log.i("LOG-"+filesuffix, text);
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                Log.i("NOLOG: ",filesuffix);
                e.printStackTrace();
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.flush();
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.i("NOLOG: ", filesuffix);

        }
    }

    public void setPreviousTask(GestureGenerationTask previousTask) {
        this.previousTask = previousTask;
    }

    public Task getPreviousTask() {
        return previousTask;
    }

    protected abstract String taskNameForLogger();


    public abstract boolean canRedraw();

    public void saveLastPredictionResults(ArrayList<Prediction> recognized) {
        lastPredictionResults = recognized;
    };

    public abstract void finishTask(MainActivity mainActivity);

    public void skip(MainActivity mainActivity) {
        block.trialFinished(mainActivity);
    }

    double convertScoreToDistance(double score) {
        // score returned by Android gesture learner is a weight equal to 1/cosineDistance

        // convert to cosine distance
        double cosDst = (score == 0 || Double.isNaN(score)) ? Double.MAX_VALUE : 1/score;
        // convert to angular distance
        double angDst = 2 * Math.acos(1 - cosDst) / Math.PI;
        return angDst;
    }
}

