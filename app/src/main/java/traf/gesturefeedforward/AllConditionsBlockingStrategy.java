package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 18/01/16.
 */
public class AllConditionsBlockingStrategy extends BlockingStrategy {
    @Override
    protected int conditionIndex(int participantId, int blockNumber,int trialNumber) {
        //3 is the number of conditions
        //Mode.values().size()
        return this.counterbalanceOrder()[participantId-1][(blockNumber-1)*3+trialNumber]-1;
    }
}
