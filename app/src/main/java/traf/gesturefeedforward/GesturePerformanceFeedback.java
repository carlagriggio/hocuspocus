package traf.gesturefeedforward;

import android.graphics.Color;

/**
 * Created by carlagriggio on 11/20/15.
 */
public abstract class GesturePerformanceFeedback {
//    public static double OkThreashold = 0.65; //distance threashold
    public static double OkThreashold = 1.5;

    public abstract void updateTextIndicator(GestureCanvas gestureCanvas);
    public void reset(GestureCanvas gestureCanvas) {
        ((MainActivity) gestureCanvas.getContext()).commandName.setTextColor(Color.BLACK);
        ((MainActivity) gestureCanvas.getContext()).commandName.setBackgroundColor(Color.TRANSPARENT);
        gestureCanvas.textIndicator.setText("");
    }

    public double okThreashold() {
        return OkThreashold;
    }
}
