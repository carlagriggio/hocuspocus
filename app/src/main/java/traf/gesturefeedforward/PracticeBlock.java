package traf.gesturefeedforward;

import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by carlagriggio on 22/01/16.
 */

public class PracticeBlock extends ExperimentBlock {
    public PracticeBlock(ExperimentSession anExperiment, int aNumberOfTrials, BlockingStrategy aBlockingStrategy) {
        experiment = anExperiment;
        numberOfTrials = aNumberOfTrials;
        blockNumber = anExperiment.numberOfBlocks() +1;
        blockingStrategy = aBlockingStrategy;

        for(int i=0; i<this.numberOfTrials; i++) {
            trials.add(new GestureGenerationTask(i +1, experiment.getCommandForGestureCreation((blockNumber-1)*2+i+1, blockNumber),this));
        }
    }


    public PracticeBlock() {

    }

    public void startBlock(final MainActivity mainActivity) {
        String title;
        if(this.numberOfTrials == 1) title = "CREATE 1 NEW GESTURE";
        else title = "CREATE "+this.numberOfTrials+" NEW GESTURES";
        new AlertDialog.Builder(mainActivity)
                .setTitle(title)
                .setMessage("\nPlease DO NOT draw letters or numbers as gestures.\n\nAll gestures must be different from the ones you already created. \n\n")
                .setPositiveButton("START", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mainActivity.startNewTrial();
                        mainActivity.blinkCommandName();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public void trialFinished(MainActivity mainActivity) {
        trialNumber = trialNumber + 1;

        if(this.trialNumber == this.numberOfTrials) {
            mainActivity.startNewBlock();
        } else {
            mainActivity.startNewTrial();
        }
    }
}
