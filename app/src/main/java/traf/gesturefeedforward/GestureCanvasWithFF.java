package traf.gesturefeedforward;

import android.annotation.TargetApi;
import android.content.Context;
import android.gesture.Gesture;
import android.gesture.Prediction;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.opengl.GLES20;

import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.FloatMath;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by carlagriggio on 11/20/15.
 */

class GestureCanvasFrameWithFF extends GestureCanvasFrame {


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public GestureCanvasFrameWithFF(Context context) {
        super(context);
        timeIndicator = new TextView(context);
        timeIndicator.setText("");
        timeIndicator.setTextSize(30.0f);
        //commandName.setId(1);
        timeIndicator.setGravity(Gravity.CENTER_HORIZONTAL);
        timeIndicator.setY(400);
        timeIndicator.setPadding(0, 20, 20, 0);
        timeIndicator.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));

        addView(timeIndicator);
    }

    public void resetCanvas(Context context) {
        removeView(gestureCanvas);
        gestureCanvas = new GestureCanvasWithFF(context);
        addView(gestureCanvas);
        gestureCanvas.setTextIndicator(textIndicator);
        textIndicator.setText("");
        setRegistrationFeedback();

        //setRegistrationFeedback();
        if(isTimed) {
            gestureCanvas.setTimeIndicator(timeIndicator);
            timeIndicator.setText("1:00");
            timeIndicator.setTextColor(Color.BLACK);
            mStartTime = SystemClock.uptimeMillis();
        }
    }



}


public class GestureCanvasWithFF extends GestureCanvas {
    static final Random sRand = new Random();
    public final DrawRenderer renderer;


//    class Command {
//        String label;
//        float[] points;
//        Path path = new Path();
//        float width, labelX, labelY;
//        int color;
//        float length;
//        RectF box;

//        Command(String l, float[] p) {
//            label = l;
//            points = p;
//            color = 0x80000000 | sRand.nextInt(0x01000000);
//        }
//
//        Command(String l, int numAtoms, int[] atoms, int[] signs, double[] angles, float[] scales) {
//            label = l;
//            int numPoints = (numAtomSamples - 1) * numAtoms + 1;
//            points = new float[numPoints * 2];
//            float offsetX = points[0] = 0;
//            float offsetY = points[1] = 0;
//
//            float cosTheta;
//            float sinTheta;
//            int index = 2;
//            int i, j;
//            box = new RectF(0, 0, 0, 0);
//
//            double lastAngle = 0;
//
//            // Concatenate the atoms
//            for (i = 0; i < numAtoms; i++) {
//                cosTheta = (float) Math.cos(angles[i] + lastAngle);
//                sinTheta = (float) Math.sin(angles[i] + lastAngle);
//                for (j = 2; j < numAtomSamples * 2; j += 2, index += 2) {
//                    points[index] = mAtoms[atoms[i]][j] * scales[i] * cosTheta - mAtoms[atoms[i]][j + 1] * scales[i] * sinTheta * signs[i] + offsetX;
//                    points[index + 1] = mAtoms[atoms[i]][j] * scales[i] * sinTheta + mAtoms[atoms[i]][j + 1] * scales[i] * cosTheta * signs[i] + offsetY;
//                    box.union(points[index], points[index + 1]);
//                }
//
//                lastAngle = Math.atan2(points[index - 1] - points[index - 3], points[index - 2] - points[index - 4]);
//                offsetX = points[index - 2];
//                offsetY = points[index - 1];
//            }
//
//            // recalulate the total length from the points since some atoms may have been scaled
//            length = 0;
//            for (i = 2; i < numPoints * 2; i += 2) {
//                length += hypot(points[i] - points[i - 2], points[i + 1] - points[i - 1]);
//            }
//        }
//    }
//
//
//    Command[] mCommands;

    // Class for keeping track of the feedforward gesture candidates
    class Candidate {
        float[] points;
        float[] lengths;
        String label;
        Path opaquePath;
        Path translucentPath;
        float width;
        float startLength;
        int index;
        int score;
        float labelX, labelY;
        boolean active;
        Candidate(String _label, float[] _points, float[] _lengths) {
            label = _label;
            points = _points;
            lengths = _lengths;
            opaquePath = new Path();
            translucentPath = new Path();
            width = 0;
            index = 0;
//            startLength = mStrokeLength;
// TODO: uncomment?
            active = true;
        }
    }
    LinkedList<Candidate> mCandidates = new LinkedList<>();

    public GestureCanvasWithFF(Context context) {
        super(context);

        mStrokePaint.setStrokeCap(Paint.Cap.ROUND);
        mStrokePaint.setStrokeJoin(Paint.Join.BEVEL);
        mStrokePaint.setStrokeWidth(20);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mPaint = new Paint(mStrokePaint);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTextSize(48);
        mBox = new RectF();


        renderer = ((MainActivity) this.getContext()).renderer;
    }

    // Runnable interface for implementing the long press
    public void run() {
        if (currentFF == Mode.NO_FF)
            return;

        //((Vibrator)getSystemService(Context.VIBRATOR_SERVICE)).vibrate(30);
        renderer.mFeedForward = true;
        switch (currentFF) {
//                case OCTOPOCUS_FF:
//                    updateOctopocus();
//                    break;
            case PATH_FF:
                appendPathCandidates(4, 4);
                updatePathCandidates();
                break;
            case FIELD_FF:
                updateField();
                break;
        }
        updateStroke();
        invalidate();
    }

    void updatePathCandidates() {
        float strokeVel = 0;
        if (mStrokeSize > 4)
            strokeVel = hypot((float)velx, (float)vely);
        for (Candidate c : mCandidates) {
            // check if velocities match approximately between atom position and user stroke
            float atomVelx = 0;
            float atomVely = 0;
            if ((c.index > 1) && (c.index < c.lengths.length - 1)) {
                atomVelx = c.points[c.index*2] - c.points[c.index*2-2];
                atomVely = c.points[c.index*2+1] - c.points[c.index*2-1];
            }

            // update atom offset index
            if (c.index < c.lengths.length - 1) {
                float offset = mStrokeLength - c.startLength;
                while (c.lengths[c.index] < offset) {
                    ++c.index;
                    if (c.index >= c.lengths.length - 1)
                        break;
                }
            }

            // TODO if end of atom, append another one
            if (c.index >= c.lengths.length - 1) {
                c.opaquePath.rewind();
                c.translucentPath.rewind();
                c.active = false;
                continue;
            }

            // update candidate path
            if (c.opaquePath == null)
                c.opaquePath = new Path();
            else if (!c.opaquePath.isEmpty())
                c.opaquePath.rewind();
            if (c.translucentPath == null)
                c.translucentPath = new Path();
            else if (!c.translucentPath.isEmpty())
                c.translucentPath.rewind();
            mBox.set(mStrokeBox);
            int pos = c.index * 2;
            float[] pts = Arrays.copyOf(mStroke, mStrokeSize + c.points.length - c.index * 2 - 2);
            float offsetx = mStroke[mStrokeSize - 2] - c.points[pos++];
            float offsety = mStroke[mStrokeSize - 1] - c.points[pos++];
            int dst = mStrokeSize;
            c.opaquePath.moveTo(mStroke[mStrokeSize - 2], mStroke[mStrokeSize - 1]);
            c.translucentPath.moveTo(mStroke[mStrokeSize - 2], mStroke[mStrokeSize - 1]);
            float len = 0;
            while (pos < c.points.length) {
                float X = pts[dst++] = c.points[pos++] + offsetx;
                float Y = pts[dst++] = c.points[pos++] + offsety;
                mBox.union(X, Y);
                len += hypot(X - pts[dst - 4], Y - pts[dst - 3]);
                if (len < 300) {
                    mBox.union(X, Y);
                    c.opaquePath.lineTo(c.labelX = X, c.labelY = Y);
                }
                c.translucentPath.lineTo(X, Y);
            }

            Gesture g = new Gesture();
            try {
                float remainingLength = c.lengths[c.lengths.length-1] - c.lengths[c.index];
                g.addStroke(mGestureStroke_constructor.newInstance(mBox,
                        mStrokeLength + remainingLength, pts, new long[0]));
                double score = gestureStore.recognize(g).get(0).score;
                //score = convertScoreToDistance(score);
                c.width = Double.isNaN(score) ? 20 : Math.min(Math.max((float)score * 10, 1), 40);
                c.score = Math.min((int)score * 32, 127); //TODO: change to double?
            }
            catch (Exception e) {
                System.out.println("3: "+e.getMessage());
                c.width = 0;
//                c.opaquePath = null;
//                c.translucentPath = null;
            }
        }
    }
    void appendPathCandidates(int numCandidates, int numAtomsPer) {
        Candidate[] candidates = new Candidate[numCandidates];

        for (int c = 0; c < candidates.length; c++) {

            int[] atoms = new int[numAtomsPer];
            int[] signs = new int[numAtomsPer];
            double[] angles = new double[numAtomsPer];

            // Generate a random gesture.
            for (int i = 0; i < numAtomsPer; i++) {
                atoms[i] = sRand.nextInt(renderer.mAtoms.length);
                signs[i] = sRand.nextInt(2) == 1 ? 1 : -1;
            }

            int numPoints = (renderer.numAtomSamples - 1) * numAtomsPer + 1;
            float[] points = new float[numPoints * 2];
            float[] lengths = new float [numPoints];
            float offsetX = points[0] = 0;
            float offsetY = points[1] = 0;
            lengths[0] = 0;
            angles[0] = Math.atan2(vely, velx);
            float offsetLength = 0;
            float cosTheta;
            float sinTheta;
            int index = 2;
            int j, m, n = 1;

            // Append the atoms to the candidate
            for (int i = 0; i < numAtomsPer; i++) {
                cosTheta = (float)Math.cos(angles[i]);
                sinTheta = (float)Math.sin(angles[i]);
                for (j = 2, m = 1; j < renderer.numAtomSamples * 2; j += 2, m++, n++, index += 2) {
                    points[index] = renderer.mAtoms[atoms[i]][j] * cosTheta - renderer.mAtoms[atoms[i]][j+1] * sinTheta * signs[i] + offsetX;
                    points[index+1] = renderer.mAtoms[atoms[i]][j] * sinTheta + renderer.mAtoms[atoms[i]][j+1] * cosTheta * signs[i] + offsetY;
                    lengths[n] = renderer.mAtomLengths[atoms[i]][m] + offsetLength;
                }
                if (i < numAtomsPer-1) {
                    angles[i+1] = angle = Math.atan2(points[index-1] - points[index-3], points[index-2] - points[index-4]);
                }
                offsetX = points[index-2];
                offsetY = points[index-1];
                offsetLength = lengths[n-1];
            }

            candidates[c] = new Candidate("<Command>", points, lengths);
            mCandidates.add(candidates[c]);
        }
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //no progressive feedback
        mStrokePaint.setColor(0xff222222);

        /* PROGRESSIVE FEEDBACK */
//        if (currentFF == Mode.NO_FF || currentFF == Mode.OCTOPOCUS_FF)
//            mStrokePaint.setColor(0xff222222);
//        else {
////          mStrokePaint.setColor(mScore < 0 ? 0xff222222 : (mScore * 0x00020102) ^ 0xff007fff);
//            mStrokePaint.setColor(Color.BLACK); //TODO: change to double
//            mStrokePaint.setStyle(Paint.Style.STROKE);
//            mStrokePaint.setStrokeWidth(20.0f);
//
//            canvas.drawPath(mStrokePath, mStrokePaint);
//
//            mStrokePaint.setStrokeWidth(16.0f);
//
//            mStrokePaint.setColor(mScore < 0 ? 0xff222222 : ((int) mScore * 0x00020102) ^ 0xff007fff); //TODO: change to double
//        }
        canvas.drawPath(mStrokePath, mStrokePaint);

        if (renderer.mFeedForward) {
//            if (experiment.currentBlock.currentFeedForwardType() == Mode.OCTOPOCUS_FF) {
//                for (Command c : mCommands) {
//                    if (c.width > 8) {
//                        mPaint.setColor(0x80880000);
//                        mPaint.setStrokeWidth(c.width);
//                        canvas.drawPath(c.path, mPaint);
//                        mPaint.setStyle(Paint.Style.FILL);
//                        canvas.drawText(c.label, c.labelX, c.labelY + 12, mPaint);
//                        mPaint.setStyle(Paint.Style.STROKE);
//                    }
//                }
//            }
//            else
            if (currentFF == Mode.PATH_FF) {
                for (Candidate c : mCandidates) {
                    if (c == null || c.active == false )
                        continue;
                    mPaint.setColor((c.score * 0x00020102) ^ 0x80007fff);
                    canvas.drawPath(c.opaquePath, mPaint);
                    mPaint.setStyle(Paint.Style.FILL);
                    //canvas.drawText(c.label, c.labelX, c.labelY + 12, mPaint);
                    mPaint.setStyle(Paint.Style.STROKE);
                    //mPaint.setColor((c.score * 0x00020102) ^ 0x20007fff);
                    canvas.drawPath(c.translucentPath, mPaint);
                }
            }
        }

    }

    //    void updateStroke() {
//        // check score for current stroke
//        float[] pts = Arrays.copyOf(mStroke, mStrokeSize - 1);
//        Gesture g = new Gesture();
//        try {
//            g.addStroke(mGestureStroke_constructor.newInstance(mStrokeBox, mStrokeLength, pts, new long[0]));
//            double score = experiment.getGestureStore().recognize(g).get(0).score;
//            mScore = Math.min(score * 32, 127);
//            userScore = Math.min(score * 25, 100);
//            mScoreName = experiment.getGestureStore().recognize(g).get(0).name;
//            for(Prediction prediction: experiment.getGestureStore().recognize(g)) {
//                experiment.getCurrentTask().addLastAttempt(prediction.name, prediction.score);
//            }
//        }
//        catch (Exception e) {
//            Log.e("Update Stroke", e.getMessage());
//            mScore = 0;
//        }
//    }
    void updateField() {
        FloatBuffer mVerts = renderer.mVerts;
        float[] pts = Arrays.copyOf(mStroke, mStrokeSize + 2);
        for (int i = 0; i < mVerts.limit() / 4; i++) {
            pts[mStrokeSize] = mVerts.get(4 * i);
            pts[mStrokeSize + 1] = mVerts.get(4 * i + 1);
            mBox.set(mStrokeBox);
            mBox.union(pts[mStrokeSize], pts[mStrokeSize + 1]);
            float len = hypot(pts[mStrokeSize] - pts[mStrokeSize - 2],
                    pts[mStrokeSize + 1] - pts[mStrokeSize - 1]);

            Gesture g = new Gesture();
            try {
                g.addStroke(mGestureStroke_constructor.newInstance(mBox, mStrokeLength + len, pts, new long[0]));
                Prediction p = gestureStore.recognize(g).get(0);
                //double score = convertScoreToDistance(p.score);
                mVerts.put(4 * i + 2, Math.min((float) p.score / 4 , 1));
            } catch (Exception e) {
                System.out.println("5: "+e.getMessage());
            }
        }
    }
    protected void actionMove(float x, float y) {
        super.actionMove(x,y);

        // Reset the long press when the finger moves far enough
        if (renderer.mFeedForward) {
            switch (currentFF) {
//                            case OCTOPOCUS_FF:
//                                updateOctopocus();
//                                break;
                case PATH_FF:
                    updatePathCandidates();
                    break;
                case FIELD_FF:
                    updateField();
                    break;
            }
        }
//                    else if (hypot(x - mLastEventX, y - mLastEventY) > 50) {
//                        mHandler.removeCallbacks(this);
//                        mHandler.postDelayed(this, 500);
//                        mLastEventX = x;
//                        mLastEventY = y;
//                    }

    }
    protected void actionUp(float x, float y) {
        gesturePerformanceFeedback.updateTextIndicator(this);

        currentTask.logGestureFragment("TOUCH_UP", x, y);
        currentTask.gestureFinished(this);

        renderer.mFeedForward = false;
        mCandidates.clear();
    }
}


