package traf.gesturefeedforward;

import android.graphics.Color;

/**
 * Created by carlagriggio on 11/19/15.
 */
public class TestingGestureFeedback extends GesturePerformanceFeedback{

    public void updateTextIndicator(GestureCanvas gestureCanvas) {
            gestureCanvas.textIndicator.setText(this.feedbackText(gestureCanvas));
    }

    private String feedbackText(GestureCanvas gestureCanvas) {
        if(gestureCanvas.experiment.getCurrentTask().command.label.equals(gestureCanvas.mScoreName) && gestureCanvas.rawScore >= this.okThreashold())
//        if(gestureCanvas.experiment.getCurrentTask().command.label.equals(gestureCanvas.mScoreName) && gestureCanvas.gestureDistance < this.okThreashold())
            return "OK!";
        else
            return "WRONG GESTURE";
    }


}
