package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 18/01/16.
 */
public abstract class BlockingStrategy {
    public Mode feedForwardForTrial(int participantId, int blockNumber, int trialNumber) {
        Mode modesPerBlock = Mode.values()[this.conditionIndex(participantId,blockNumber,trialNumber)];
        return modesPerBlock;
    }

    protected abstract int conditionIndex(int participantId, int blockNumber,int trialNumber);

    protected int[][] counterbalanceOrder() {
        return new int[][]{{2, 3, 1},{1, 2, 3},{2, 1, 3},{3, 1, 2},{1, 3, 2},{3, 2, 1}, //participants 1-6
                //after this, not verified
                {1, 2, 3},{2, 3, 1},{3, 1, 2},{1, 3, 2},{3, 2, 1},{2, 1, 3}, //participants 7-12
                {1, 2, 3},{2, 3, 1},{3, 1, 2},{1, 3, 2},{3, 2, 1},{2, 1, 3}, //participants 13-18
                {1, 2, 3},{2, 3, 1},{3, 1, 2},{1, 3, 2},{3, 2, 1},{2, 1, 3}}; //participants 19-24
    }
}
