package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 13/01/16.
 */
public class NullTask extends Task {
    public NullTask(Mode feedForwardType, CommandLabel command, int number, Block aBlock, int participantId) {
        super(feedForwardType,command,number,aBlock,participantId);
    }

    @Override
    protected void initTask(MainActivity mainActivity) {
        mainActivity.showFFCanvas();
    }

    @Override
    public String textForNextTaskButton() {
        return null;
    }

    @Override
    public void finishTask(MainActivity mainActivity) {

    }

    @Override
    protected String taskNameForLogger() {
        return "NullTask";
    }

    @Override
    public boolean canRedraw() {
        return false;
    }
}
