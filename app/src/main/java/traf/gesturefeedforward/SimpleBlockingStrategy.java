package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 18/01/16.
 */
public class SimpleBlockingStrategy extends BlockingStrategy {
    //picks only one condition per block
    @Override
    protected int conditionIndex(int participantId, int blockNumber,int trialNumber) {
        return this.counterbalanceOrder()[participantId-1][blockNumber-1]-1;
    }

}
