package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 9/16/15.
 */
public class GestureGenerationTask extends Task {

    public GestureGenerationTask(int trialNumber, CommandLabel command, Block block) {
        super(block.currentFeedForwardType(), command, trialNumber, block, block.getParticipantId());
    }

    public String textForNextTaskButton() {
        return "REGISTER";
    }

    public void initTask(MainActivity mainActivity) {
        mainActivity.setCommandLabelAndInstructions(/*block.instructionsTextForCommand() + */command.label, command.instruction);
        mainActivity.setLabelOfNextTaskButton(this.textForNextTaskButton());
        mainActivity.hideBackButton();
        mainActivity.showFFCanvas();
        mainActivity.gestureCanvasFrame.startTimer();

    }
    public void finishTask(MainActivity mainActivity) {
        this.end();
        GestureRegistrationTask nextTask = new GestureRegistrationTask(this.trialNumber,this.command,this.block,2);
        mainActivity.startNextTask((GestureTestTask) nextTask);
        nextTask.setPreviousTask(this);
    }

    protected String taskNameForLogger() {
        return "G";
    }

    @Override
    public boolean canRedraw() {
        return true;
    }

    public void gestureFinished(GestureCanvas aCanvas) {
        super.gestureFinished(aCanvas);
//        if(aCanvas.gestureDistance >= GesturePerformanceFeedback.OkThreashold || aCanvas.gestureDistance == 0.0) {
//            aCanvas.mainActivity.nextTrialButton.setEnabled(true);
//        } else {
        if(aCanvas.rawScore > GesturePerformanceFeedback.OkThreashold || aCanvas.mStrokeLength < 100 ) { // gestures shorter than 10px could be just the user touching the screen to preview the FF
            aCanvas.mainActivity.nextTrialButton.setEnabled(false);
        } else {
            aCanvas.mainActivity.nextTrialButton.setEnabled(true);
        }
    }
}
