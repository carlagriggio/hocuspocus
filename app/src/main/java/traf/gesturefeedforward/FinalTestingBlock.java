package traf.gesturefeedforward;

import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by carlagriggio on 10/20/15.
 */
public class FinalTestingBlock extends ExperimentTestingBlock {


    public FinalTestingBlock(ExperimentSession anExperiment) {
        super();
        experiment = anExperiment;
        blockNumber = anExperiment.numberOfBlocks() +1;
        int num = 0;
        for (Task t : experiment.allTrials()) {
            num = num + 1;
            this.trials.add(new GestureTestTask(num, t.command, this));
            //TODO: review tasks should log the original task they are reviewing? careful with trial number and block number!
        }
    }

    public void startBlock(final MainActivity mainActivity) {
        new AlertDialog.Builder(mainActivity)
                .setTitle("FINAL REVIEW")
                .setMessage("Review ALL the gestures you created. \nDraw each of the gestures you created 3 times as well as you can remember. \nThen press the \"NEXT COMMAND\" button.")
                .setPositiveButton("START", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mainActivity.startNewTrial();

                        //startNewCreatedGestureEvaluationTrial();
//                        mainActivity.hideScore();
//                        mainActivity.showDetectedCommand();
                    }
                })
                .setCancelable(false)
                .setIcon(android.R.drawable.btn_star_big_on)
                .show();
    }

    public void trialFinished(MainActivity mainActivity) {
        trialNumber = trialNumber + 1;
        if(this.trialNumber == this.trials.size()) {
            mainActivity.startNewBlock();
        } else {
            mainActivity.startNewTrial();
        }
    }


}
