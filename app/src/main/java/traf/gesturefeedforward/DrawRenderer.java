package traf.gesturefeedforward;

import android.gesture.Gesture;
import android.graphics.Path;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by carlagriggio on 11/20/15.
 */
public class DrawRenderer implements GLSurfaceView.Renderer {
    public FloatBuffer mVerts;
    ByteBuffer mIndices0, mIndices1;
    int mVec, mProgram; // Has to be a global handle for OpenGL to work!
    static final long[] sTimestamps = new long[0];
    int numAtomSamples;
    float[][] mAtoms;
    float[][] mAtomLengths;
    boolean mFeedForward = false;
    public Mode mode;


    public DrawRenderer() {
        // create some atoms for composing new potential gestures
        numAtomSamples = 20;
        mAtoms = new float[6][numAtomSamples * 2];
        mAtomLengths = new float[6][numAtomSamples];
        float norm = 1f / ((float)numAtomSamples * 2f);
        // Stuff for Path-Based Feedforward
        float scale = 250;

        // first atom is just a straight line
        mAtomLengths[0][0] = 0;
        int i, j;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[0][i] = i * norm * scale;
            mAtoms[0][i+1] = 0f * scale;
            if (i >= 2)
                mAtomLengths[0][j] = mAtomLengths[0][j-1]
                        + hypot(mAtoms[0][i] - mAtoms[0][i-2], mAtoms[0][i+1] - mAtoms[0][i-1]);
        }

        // next atom describes cosine arc from 0 – π/3
        mAtomLengths[1][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[1][i] = (float) Math.sin(i * norm * (float) Math.PI * 0.33f) * scale;
            mAtoms[1][i+1] = (1f - (float) Math.cos(i * norm * (float)Math.PI * 0.33f)) * scale;
            if (i >= 2)
                mAtomLengths[1][j] = mAtomLengths[1][j-1]
                        + hypot(mAtoms[1][i] - mAtoms[1][i-2], mAtoms[1][i+1] - mAtoms[1][i-1]);
        }

        // next atom describes cosine arc from 0 – π/2
        mAtomLengths[2][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[2][i] = (float) Math.sin(i * norm * (float)Math.PI * 0.5f) * scale * 0.75f;
            mAtoms[2][i+1] = (1f - (float) Math.cos(i * norm * (float)Math.PI * 0.5f)) * scale * 0.75f;
            if (i >= 2)
                mAtomLengths[2][j] = mAtomLengths[2][j-1]
                        + hypot(mAtoms[2][i] - mAtoms[2][i-2], mAtoms[2][i+1] - mAtoms[2][i-1]);
        }

        // next atom describes arc from 0 – 2π/3
        mAtomLengths[3][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[3][i] = (float) Math.sin(i * norm * (float)Math.PI) * scale * 0.67f;
            mAtoms[3][i+1] = (1f - (float) Math.cos(i * norm * (float)Math.PI)) * scale * 0.67f;
            if (i >= 2)
                mAtomLengths[3][j] = mAtomLengths[3][j-1]
                        + hypot(mAtoms[3][i] - mAtoms[3][i-2], mAtoms[3][i+1] - mAtoms[3][i-1]);
        }

        // next atom describes arc from 0 – 4π/3
        mAtomLengths[4][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[4][i] = (float) Math.sin(i * norm * (float)Math.PI * 1.333f) * scale * 0.5f;
            mAtoms[4][i+1] = (1f - (float) Math.cos(i * norm * (float)Math.PI * 1.333f)) * scale * 0.5f;
            if (i >= 2)
                mAtomLengths[4][j] = mAtomLengths[4][j-1]
                        + hypot(mAtoms[4][i] - mAtoms[4][i-2], mAtoms[4][i+1] - mAtoms[4][i-1]);
        }

        // next atom describes arc from 0 – 5π/3
        mAtomLengths[5][0] = 0;
        for (i = 0, j = 0; i < numAtomSamples * 2; i += 2, j++) {
            mAtoms[5][i] = (float) Math.sin(i * norm * (float)Math.PI * 1.667f) * scale * 0.5f;
            mAtoms[5][i+1] = (1f - (float) Math.cos(i * norm * (float)Math.PI * 1.667f)) * scale * 0.5f;
            if (i >= 2)
                mAtomLengths[5][j] = mAtomLengths[5][j-1]
                        + hypot(mAtoms[5][i] - mAtoms[5][i-2], mAtoms[5][i+1] - mAtoms[5][i-1]);
        }

        // Stuff for Field-based feedforward
        // Above 16/10, the renderer has visual artefacts.
        // tablet:
//        final int resX = 13, resY = 8;
//        final float incX = 2560f / resX;
//        final float incY = 1550f / resY;
        // smartphone:
        final int resX = 8, resY = 13;
        final float incX = 1080f / resX;
        final float incY = 1920f / resY;
        mVerts = ByteBuffer.allocateDirect((resX + 1) * (resY + 1) * 16).order(ByteOrder.nativeOrder()).asFloatBuffer();
        int index;
        for (i = 0, index = 0; i <= resY; i++) {
            for (j = 0; j <= resX; j++) {
                mVerts.put(index++, j * incX);
                mVerts.put(index++, i * incY);
                mVerts.put(index++, 0);
                mVerts.put(index++, 0);
            }
        }

        // Create two sets of triangles that will display the grid as shades.
        mIndices0 = ByteBuffer.allocateDirect(resX * resY * 6);
        mIndices1 = ByteBuffer.allocateDirect(resX * resY * 6);
        int index0, index1;
        for (i = 0, index0 = 0, index1 = 0; i < resY; i++) {
            for (j = 0; j < resX; j++) {
                byte topLeft = (byte)(i * (resX + 1) + j);
                byte topRight = (byte)(topLeft + 1);
                byte bottomLeft = (byte)(topLeft + resX + 1);
                byte bottomRight = (byte)(bottomLeft + 1);
                mIndices0.put(index0++, topLeft);
                mIndices0.put(index0++, bottomLeft);
                mIndices0.put(index0++, topRight);
                mIndices0.put(index0++, topRight);
                mIndices0.put(index0++, bottomLeft);
                mIndices0.put(index0++, bottomRight);
                mIndices1.put(index1++, bottomLeft);
                mIndices1.put(index1++, bottomRight);
                mIndices1.put(index1++, topLeft);
                mIndices1.put(index1++, topLeft);
                mIndices1.put(index1++, bottomRight);
                mIndices1.put(index1++, topRight);
            }
        }
    }
    public void onDrawFrame(GL10 glUnused) {
        mIndices0.position(0);
        mIndices1.position(0);
        GLES20.glDisable(GLES20.GL_BLEND);

        if (mode == Mode.FIELD_FF && mFeedForward) {
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, mIndices0.limit(), GLES20.GL_UNSIGNED_BYTE, mIndices0);
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, mIndices1.limit(), GLES20.GL_UNSIGNED_BYTE, mIndices1);
        }
        else {
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
        }
    }

    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
    }

    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
        mProgram = GLES20.glCreateProgram();
        int vshader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
        int fshader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
        // tablet:
//        GLES20.glShaderSource(vshader, "attribute vec4 aVec; varying lowp vec4 vCol; void main() { gl_Position = vec4(aVec.x/1280.0-1.0,1.0-aVec.y/775.0,0,1); vCol = vec4(aVec.z,0.5-aVec.z*0.5,1.0-aVec.z,0.5); }");
        // smartphone:
        GLES20.glShaderSource(vshader, "attribute vec4 aVec; varying lowp vec4 vCol; void main() { gl_Position = vec4(aVec.x/540.0-1.0,1.0-aVec.y/960.0,0,1); vCol = vec4(aVec.z,0.5-aVec.z*0.5,1.0-aVec.z,0.5); }");
        GLES20.glShaderSource(fshader, "varying lowp vec4 vCol; void main() { gl_FragColor = vCol; }");
        GLES20.glCompileShader(vshader);
        GLES20.glCompileShader(fshader);
        GLES20.glAttachShader(mProgram, vshader);
        GLES20.glAttachShader(mProgram, fshader);
        GLES20.glLinkProgram(mProgram);
        GLES20.glUseProgram(mProgram);
        mVec = GLES20.glGetAttribLocation(mProgram, "aVec");
        GLES20.glEnableVertexAttribArray(mVec);
        GLES20.glVertexAttribPointer(mVec, 4, GLES20.GL_FLOAT, false, 16, mVerts);
        GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        GLES20.glBlendFunc (GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
    }

    static float hypot(float a, float b) {
        if (android.os.Build.VERSION.SDK_INT >= 17)
            return (float) Math.hypot(a, b);
        else
            return (float) Math.sqrt(a * a + b * b);
    }
}
