package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 27/03/16.
 */
public class Counterbalanced3x6OrderBetween extends Counterbalanced3x6Order {
    private int commandNumber = -1;
    private int commandIndex;

    @Override
    CommandLabel getCommandForTrial(int participantId, int aBlock, int aTrial, ExperimentSession exp) {
        int commandNumberIndex = aBlock - 1;
        int participantIndex = (int) ((participantId - 1) - (6 * Math.floor((participantId - 1) / 6)));
        commandIndex = this.counterbalancingCommandNumber()[participantIndex][commandNumberIndex];

        commandNumber = (this.counterbalancingCategoriesOrder()[participantIndex+(aBlock - 1)][aTrial-1]-1)*6+commandIndex;

        return exp.commands.get(commandNumber-1);
    }

    protected int[][] counterbalancingCommandNumber() {
        return new int[][]{
                {1,2,3,4,5,6},{2,3,1,5,6,4},{3,2,1,6,4,5},{1,3,2,4,6,5},{2,1,3,5,4,6},{3,2,1,6,5,4}, //participants 1-6
                {1,2,3,4,5,6},{2,3,1,5,6,4},{3,2,1,6,4,5},{1,3,2,4,6,5},{2,1,3,5,4,6},{3,2,1,6,5,4}, //participants 7-12
                {1,2,3,4,5,6},{2,3,1,5,6,4},{3,2,1,6,4,5},{1,3,2,4,6,5},{2,1,3,5,4,6},{3,2,1,6,5,4}, //participants 13-18
                {1,2,3,4,5,6},{2,3,1,5,6,4},{3,2,1,6,4,5},{1,3,2,4,6,5},{2,1,3,5,4,6},{3,2,1,6,5,4}, //participants 19-24
                {1,2,3,4,5,6},{2,3,1,5,6,4},{3,2,1,6,4,5},{1,3,2,4,6,5},{2,1,3,5,4,6},{3,2,1,6,5,4}, //participants 25-30
                {1,2,3,4,5,6},{2,3,1,5,6,4},{3,2,1,6,4,5},{1,3,2,4,6,5},{2,1,3,5,4,6},{3,2,1,6,5,4}  //participants 31-36
        };
    }
    protected int[][] counterbalancingCategoriesOrder() {
        return new int[][]{
                {1,2,6,3,5,4},{2,3,1,4,6,5},{3,4,2,5,1,6},{4,5,3,6,2,1},{5,6,4,1,3,2},{6,1,5,2,4,3},//participants 1-6
                {3,4,2,5,1,6},{1,2,6,3,5,4},{2,3,1,4,6,5},{6,1,5,2,4,3},{4,5,3,6,2,1},{5,6,4,1,3,2},//participants 7-12
                {2,3,1,4,6,5},{3,4,2,5,1,6},{1,2,6,3,5,4},{5,6,4,1,3,2},{6,1,5,2,4,3},{4,5,3,6,2,1},//participants 13-18
                {1,2,6,3,5,4},{2,3,1,4,6,5},{3,4,2,5,1,6},{4,5,3,6,2,1},{5,6,4,1,3,2},{6,1,5,2,4,3},//participants 19-24
                {1,2,6,3,5,4},{2,3,1,4,6,5},{3,4,2,5,1,6},{4,5,3,6,2,1},{5,6,4,1,3,2},{6,1,5,2,4,3},//participants 25-30
                {1,2,6,3,5,4},{3,4,2,5,1,6},{2,3,1,4,6,5},{4,5,3,6,2,1},{6,1,5,2,4,3},{5,6,4,1,3,2}//participants 31-36
        };
    }
}
