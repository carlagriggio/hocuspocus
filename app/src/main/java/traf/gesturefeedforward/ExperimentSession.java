package traf.gesturefeedforward;

import android.gesture.Gesture;
import android.gesture.GestureStore;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by carlagriggio on 9/19/15.
 */

enum Mode { NO_FF, FIELD_FF, PATH_FF, NO_FF_EVAL, OCTOPOCUS_FF } //NO_FF_EVAL is just for logging

public class ExperimentSession {
    int participantID;
    Block currentBlock;
    int blockNumber;
    ArrayList<Block> blocks = new ArrayList<>();
    GestureStore gestureStore = new GestureStore();
    ArrayList<CommandLabel> commands = new ArrayList<CommandLabel>();
    Task currentTask;
    public OrderStrategy trialOrderStrategy = new RandomOrder();
    private String logPrefix;
    private HashMap<String, String> commandsByName = new HashMap<>();


    public GestureStore getGestureStore() {
        return gestureStore;
    }

    public void setGestureStore(GestureStore gestureStore) {
        this.gestureStore = gestureStore;
    }

    public Task getCurrentTask() {
        return currentTask;
    }
    public void setCurrentTask(Task currentTask) {
        this.currentTask = currentTask;
    }

    public ExperimentSession(int participantID) {
        this.participantID = participantID;
//        this.createCommands();
//        this.createBlocks();
        this.setGestureStore(new GestureStore());
    }
    public ExperimentSession(int participantID, GestureStore gestureStore) {
        this.participantID = participantID;
        //this.createCommands();
        this.createBlocks();
        this.setGestureStore(gestureStore);
    }

    protected void createNamelessCommands() {
        for (int i=1; i<37; i++) {
            this.commands.add(new CommandLabel(String.valueOf(i), ""));
        }
    }
    protected void createCommands() {
        Collections.addAll(this.commands, new CommandLabel[]{
                //communication
                new CommandLabel("Call Mom", "Dial Mom's phone number", "R", "1"),
                new CommandLabel("Text late", "Text my best friend that I'm arriving late", "R", "2"),
                new CommandLabel("Poke partner", "Text my favorite romantic emoji (such as \uD83D\uDE18) to my partner", "R", "3"),
                new CommandLabel("Forward parents", "Forward this e-mail to my parents", "R", "4"),
                new CommandLabel("Skype BFF", "Start a Skype call with my best friend", "R", "5"),
                new CommandLabel("Group-chat friends", "Open group chat with my best friends", "R", "6"),

                //social media
                new CommandLabel("Instagram #LifeInParis", "Upload picture to Instagram with hashtag #LifeInParis", "S", "1"),
                new CommandLabel("Publish video", "Publish current video on Youtube", "S", "2"),
                new CommandLabel("Facebook share", "Share current webpage on Facebook", "S", "3"),
                new CommandLabel("Tweet photo", "Post to Twitter the last taken photo", "S", "4"),
                new CommandLabel("Tag me", "Tag myself on current Facebook picture", "S", "5"),
                new CommandLabel("Check #TrafficAlert", "Check all live tweets with hashtag #TrafficAlert", "S", "6"),

                //media context
                new CommandLabel("Dropbox pdf", "Save current PDF file to my Dropbox", "M", "1"),
                new CommandLabel("Next Episode", "Watch next episode of the TV show I'm watching", "M", "2"),
                new CommandLabel("Translate text", "Translate the selected text into my native language", "M", "3"),
                new CommandLabel("Song lyrics", "Display the lyrics of the current playing song", "M", "4"),
                new CommandLabel("Repeat song", "Repeat the last played song", "M", "5"),
                new CommandLabel("Bookmark page", "Bookmark this page in my web browser", "M", "6"),

                //location context
                new CommandLabel("Nearby restaurants", "Show nearby restaurants on a map", "L", "1"),
                new CommandLabel("Bus schedule", "Check the bus time schedule to go from home to work", "L", "2"),
                new CommandLabel("Family location", "Share my current location with my family", "L", "3"),
                new CommandLabel("Route home", "Show route to go home on a map", "L", "4"),
                new CommandLabel("Show location", "Show my current location on a map", "L", "5"),
                new CommandLabel("Navigation On", "Turn on the voice navigation system on my maps application", "L", "6"),

                //App shortcuts
                new CommandLabel("Set timer", "Set a 5 minutes timer", "A", "1"),
                new CommandLabel("Record voice", "Start recording a voice memo", "A", "2"),
                new CommandLabel("Snooze alarm", "Stop alarm and re-set it for 10 minutes from now", "A", "3"),
                new CommandLabel("Play CandyCrush", "Play my favorite game, Candy Crush", "A", "4"),
                new CommandLabel("Calendar today", "Show my agenda for today on my calendar", "A", "5"),
                new CommandLabel("Take selfie", "Open the Camera app using the front camera of the smartphone", "A", "6"),

                //OS settings
                new CommandLabel("Battery-saver On", "Turn Battery-saver mode on", "O", "1"),
                new CommandLabel("Dismiss notifications", "Dismiss all the unread notifications", "O", "2"),
                new CommandLabel("Close all", "Close all running aplications", "O", "3"),
                new CommandLabel("Geolocation Off", "Disable all geolocation services", "O", "4"),
                new CommandLabel("Airplane-mode On", "Turn Airplane mode On", "O", "5"),
                new CommandLabel("Phone vibrate", "Turn the phone ringer to only vibrate", "O", "6"),

                //new CommandLabel("Approve LinkedIn", "Approve all pending LinkedIn contact requests"),
                //new CommandLabel("Photo Jamie", "Text a photo to Jamie (best friend)"),
                //new CommandLabel("Skype Grandma", "Open a skype connection to Grandma"),
                //new CommandLabel("Follow Twitter", "Follow this person on Twitter"),
                //new CommandLabel("Set sepia", "Set the Sepia image filter in camera"),
                //new CommandLabel("Check in", "Check in to my upcoming flight"),
                //new CommandLabel("Remove tag", "Remove tag of myself on current Facebook photo"),
                //new CommandLabel("Play Thriller", "Play my favorite song, Michael Jackson's Thriller"),
                //new CommandLabel("Scan food", "Scan food into my calorie counter"),
                // new CommandLabel("Forward Terry", "Forward email to Terry (my manager)"),
                // new CommandLabel("Sam link", "Send link to current webpage to Sam, my sibling"),
        });
       this.cacheCommandsByName();


        //argh, horrible code repetition. See TrainingSession
       CommandLabel[] trainingCommands = new CommandLabel[]{
                new CommandLabel("Text friend", "Write a new SMS to my best friend", "T", "1"),
                new CommandLabel("Flashlight On", "Turn on my phone's flashlight", "T", "2"),
                new CommandLabel("Weather report", "Show a weather report for today", "T", "3"),
                new CommandLabel("Facebook song", "Share on Facebook the song I'm currently listening to", "T", "4"),
                new CommandLabel("Supermarket List", "Open my supermarket grocery list", "T", "5"),
                new CommandLabel("Track running", "Start tracking my running session with my favorite Sports app", "T", "6")};
        for (CommandLabel command:trainingCommands) {
            commandsByName.put(command.label,command.commandCode());
        }
    }

    protected void cacheCommandsByName() {
        for (CommandLabel command:this.commands) {
            commandsByName.put(command.label,command.commandCode());
        }
    }

    public void addBlock(int trials, BlockingStrategy aBlockingStrategy) {
        this.blocks.add(new ExperimentBlock(this, trials, aBlockingStrategy));
    }
    public void addBlock(Block aBlock) {
        this.blocks.add(aBlock);
    }
    public void addFinalTestingBlock() {
        this.blocks.add(new FinalTestingBlock(this));
    }
    protected void createBlocks() {
        this.addBlock(6, new AllConditionsBlockingStrategy());
        this.addBlock(6, new AllConditionsBlockingStrategy());
        this.addBlock(6, new AllConditionsBlockingStrategy());
        this.addBlock(6, new AllConditionsBlockingStrategy());


//        this.blocks.add(new ExperimentBlock(this,3));
//        this.blocks.add(new ExperimentBlock(this,3));
//        this.blocks.add(new ExperimentBlock(this,2));
//        this.blocks.add(new ExperimentBlock(this,2));
//        this.blocks.add(new ExperimentBlock(this,2));
//        this.blocks.add(new ExperimentBlock(this,1));
//        this.blocks.add(new ExperimentBlock(this,1));
//        this.blocks.add(new ExperimentBlock(this,1));
//        this.blocks.add(new ExperimentBlock(this,1));
//        this.blocks.add(new ExperimentBlock(this,1));
//        this.blocks.add(new ExperimentBlock(this,1));


        this.addFinalTestingBlock();
    }

//    public Mode getFeedForwardType() {
//        Mode fftype = Mode.NO_FF; //for every block 1
//        if(blockNumber == 2) {
//            if(participantID > 6)
//                fftype = Mode.PATH_FF;
//            else
//                fftype = Mode.FIELD_FF;
//        }
//        if(blockNumber == 3) {
//            if(participantID > 6)
//                fftype = Mode.FIELD_FF;
//            else
//                fftype = Mode.PATH_FF;
//        }
//        return fftype;
//    }


    private int[] counterbalanceOrder() {
        int[] orders = {1,2,3,3,2,1,3,1,2,3,2,1,1,3,2,2,1,3};
        int orderIndex = participantID - 1;
//        if (participantID>6) orderIndex = orderIndex - 6; //repeat the order for the participants 7-12
        return orders;
    }


    public CommandLabel getCommandForGestureCreation(int i, int aBlockNumber) {
        if(this.commands.isEmpty()) this.createCommands();
        return trialOrderStrategy.getCommandForTrial(participantID,aBlockNumber, i, this);
    }

//    public CommandLabel getCommandForGestureCreationTrial() { //int aBlock, int aTrial
////        commandSet = this.commands()[this.counterbalanceOrderA()[aBlock - 1] - 1]; //set number for participant in this block
////        return commandSet[this.counterbalanceOrderA()[aTrial] - 1];
//        int idx = commandRandomizer.nextInt(this.commands.size());
//        CommandLabel command = this.commands.get(idx);
//        this.commands.remove(command);
//        return command;
//    }

    public Task newTrial() {
        return currentBlock.nextTask();
    }
    public Task previousTask() {
        return currentBlock.previousTask();
    }

    public void startNewBlock(MainActivity mainActivity) throws PracticeSessionOverException, NoMoreExperimentBlocksException, TrainingSessionOverException {
        blockNumber = blockNumber + 1;
        if(blockNumber <= this.blocks.size()) {
            currentBlock = blocks.get(blockNumber-1);
            currentBlock.startBlock(mainActivity);
        }
        else
            this.lastBlockFinished();
    }

    protected void lastBlockFinished() throws PracticeSessionOverException, NoMoreExperimentBlocksException, TrainingSessionOverException {
        throw new NoMoreExperimentBlocksException();
    }

    public void trialFinished(MainActivity mainActivity) {
        currentBlock.trialFinished(mainActivity);
    }

    public void setCurrentBlock(ExperimentTestingBlock currentBlock) {
        this.currentBlock = currentBlock;
    }

    public int numberOfBlocks() {
        return this.blocks.size();
    }



    public ArrayList<Task> allTrials() {
        ArrayList<Task> allTrials = new ArrayList<>();
        for(Block aBlock:blocks) {
            allTrials.addAll(aBlock.getTrials());
        }
        return allTrials;
    }

    public void setParticipant(int participant) {
        this.participantID = participant;
    }

    public String logFileName() {
        return "P"+participantID+"-"+this.logPrefix()+"-"+this.sessionPrefix();
    }

    protected String sessionPrefix() {
        return "exp";
    }

    public String logPrefix() {
        return (logPrefix == null ? "" : logPrefix);
    }
    public void setLogPrefix(String prefix) {
        logPrefix = prefix;
    }

    public String fileLocation() {
        File folder = new File(Environment.getExternalStorageDirectory() + "/GestureGenerationStudy-"+this.logPrefix());
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if (!success)
            Log.i("Log","Could not create experiment folder");

        return folder.getAbsolutePath();

    }

    public void saveGesture(String label, Gesture g) {
        this.getGestureStore().addGesture(label,g);
        try {
            this.getGestureStore().save(new FileOutputStream(new File(this.fileLocation() + "/gestures-" + this.logFileName())), true);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public String getCommandCodeWithLabel(String name) {
        return commandsByName.get(name);
    }
}

class CommandLabel {
    String label;
    String instruction;
    String category;
    String number;
    CommandLabel(String l, String inst) { label = l; instruction = inst; }
    CommandLabel(String l, String inst, String cat, String num) { label = l; instruction = inst; category = cat; number = num; }
    public String commandCode() {
        return this.category+this.number;
    }

}
