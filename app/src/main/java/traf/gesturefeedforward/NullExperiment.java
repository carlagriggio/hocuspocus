package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 13/01/16.
 */
public class NullExperiment extends ExperimentSession {
    public final Mode ff_type;

    public NullExperiment(int participantID) {
        super(participantID);
        ff_type = null;
    }
    protected void createBlocks() {
        this.blocks.add(new NullExperimentBlock(this, 1));
    }
    public NullExperiment(int participantID, Mode feedforward) {
        super(participantID);
        ff_type = feedforward;
    }

    private class NullExperimentBlock extends Block {
        public NullExperimentBlock(NullExperiment nullExperiment, int i) {
            super();
            experiment = nullExperiment;
        }

        @Override
        public void startBlock(MainActivity mainActivity) {
            //mainActivity.showFFCanvas();
            mainActivity.startNextTask(this.nextTask());
        }

        @Override
        public void trialFinished(MainActivity mainActivity) {

        }

        @Override
        public Mode currentFeedForwardType() {
            return ((NullExperiment) experiment).ff_type;
        }

        @Override
        public String instructionsTextForCommand() {
            return null;
        }
        public Task nextTask() {
            return new NullTask(this.currentFeedForwardType(),new CommandLabel("",""),0,this,0);
        }
    }
}
