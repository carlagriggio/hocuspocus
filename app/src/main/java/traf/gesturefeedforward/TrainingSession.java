package traf.gesturefeedforward;

import java.util.Collections;

/**
 * Created by carlagriggio on 22/01/16.
 */

public class TrainingSession extends ExperimentSession {

    public TrainingSession(int participantID) {
        super(participantID);
        trialOrderStrategy = new IncrementalOrder();
    }

    protected void createCommands() {
        Collections.addAll(this.commands, new CommandLabel[]{
                new CommandLabel("Text friend", "Write a new SMS to my best friend","T","1"),
                new CommandLabel("Flashlight On", "Turn on my phone's flashlight","T","2"),
                new CommandLabel("Weather report", "Show a weather report for today","T","3"),
                new CommandLabel("Facebook song", "Share on Facebook the song I'm currently listening to","T","4"),
                new CommandLabel("Supermarket List", "Open my supermarket grocery list","T","5"),
                new CommandLabel("Track running", "Start tracking my running session with my favorite Sports app","T","6"),
                new CommandLabel("Count calories", "Register a meal on my calorie counter app at the current time of the day", "T", "7"),
                new CommandLabel("Approve LinkedIn", "Approve the last LinkedIn contact request", "T", "8"),
                new CommandLabel("Bluetooth Off", "Turn Bluetooth Off", "T", "9"),
                new CommandLabel("Follow Twitter", "Follow the current Twitter profile", "T", "10"),
                new CommandLabel("Set sepia", "Set the Sepia image filter in camera", "T", "11"),
                new CommandLabel("Flight check-in", "Open the check-in online form for my upcoming flight", "T", "12"),
//                new CommandLabel("Count calories", "Register a meal on my calorie counter app at the current time of the day"),
//                new CommandLabel("Flight check-in", "Open the check-in online form for my upcoming flight"),
        });
        this.cacheCommandsByName();
    }

    protected void createBlocks() {
        this.blocks.add(new PracticeBlock(this, 2, new FixedConditionBlockingStrategy(Mode.NO_FF)));
        this.blocks.add(new PracticeBlock(this, 2, new FixedConditionBlockingStrategy(Mode.PATH_FF)));
        this.blocks.add(new PracticeBlock(this, 2, new FixedConditionBlockingStrategy(Mode.FIELD_FF)));
        //this.blocks.add(new FinalTestingBlock(this));
    }
    @Override
    public void addBlock(int trials, BlockingStrategy aBlockingStrategy) {
        this.blocks.add(new PracticeBlock(this, trials, aBlockingStrategy));
    }

    @Override
    protected void lastBlockFinished() throws PracticeSessionOverException, NoMoreExperimentBlocksException, TrainingSessionOverException {
        throw new TrainingSessionOverException();
    }

    @Override
    protected String sessionPrefix() {
        return "tra";
    }
    @Override
    public void startNewBlock(MainActivity mainActivity) throws PracticeSessionOverException, NoMoreExperimentBlocksException, TrainingSessionOverException {
        mainActivity.gestureCanvasFrame.isTimed = false;
        super.startNewBlock(mainActivity);
    }
}
