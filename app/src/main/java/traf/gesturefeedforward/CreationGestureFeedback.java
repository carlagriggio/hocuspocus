package traf.gesturefeedforward;

import android.graphics.Color;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by carlagriggio on 11/19/15.
 */
public class CreationGestureFeedback extends GesturePerformanceFeedback{

    public void updateTextIndicator(GestureCanvas gestureCanvas) {
//        double score = gestureCanvas.gestureDistance;
        double score = gestureCanvas.rawScore;
        //gestureCanvas.textIndicator.setText(String.format("%1$,.0f", Double.isNaN(score) ? 0 : score)  + "% ");
        TextView commandLabel = ((MainActivity) gestureCanvas.getContext()).commandName;
        if(score < this.okThreashold()) {
            commandLabel.setTextColor(Color.WHITE);
            commandLabel.setBackgroundColor(Color.BLUE);
        } else {
            commandLabel.setTextColor(Color.WHITE);
            commandLabel.setBackgroundColor(Color.RED);
        }
    }

}
