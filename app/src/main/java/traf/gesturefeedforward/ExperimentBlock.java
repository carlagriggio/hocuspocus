package traf.gesturefeedforward;

import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by carlagriggio on 10/16/15.
 */
public class ExperimentBlock extends Block {
    public int numberOfTrials;
    protected BlockingStrategy blockingStrategy;
    public boolean testAfterEachBlock = true;
    public boolean showTitleAlerts = true;

    public ExperimentBlock(ExperimentSession anExperiment, int aNumberOfTrials, BlockingStrategy aBlockingStrategy) {
        experiment = anExperiment;
        numberOfTrials = aNumberOfTrials;
        blockNumber = anExperiment.numberOfBlocks() +1;
        blockingStrategy = aBlockingStrategy;

        for(int i=0; i<this.numberOfTrials; i++) {
//            trials.add(new GestureGenerationTask(i +1, experiment.getCommandForGestureCreation(i+1),this));
            trials.add(new GestureGenerationWithRegistrationTask(i +1, experiment.getCommandForGestureCreation(i+1, blockNumber),this));

        }
    }

    public ExperimentBlock(ExperimentSession anExperiment, int aNumberOfTrials, BlockingStrategy aBlockingStrategy, boolean showTitleAlerts, boolean testAfterEachBlock) {
        experiment = anExperiment;
        numberOfTrials = aNumberOfTrials;
        blockNumber = anExperiment.numberOfBlocks() +1;
        blockingStrategy = aBlockingStrategy;

        for(int i=0; i<this.numberOfTrials; i++) {
            trials.add(new GestureGenerationWithRegistrationTask(i+1, experiment.getCommandForGestureCreation(i+1, blockNumber),this));
        }

        this.showTitleAlerts = showTitleAlerts;
        this.testAfterEachBlock = testAfterEachBlock;
    }

    public ExperimentBlock() {

    }

    public void startBlock(final MainActivity mainActivity) {
//        mainActivity.hideDetectedCommand();
//        mainActivity.showScore();
        //mainActivity.hideBoxes();

        if(this.showTitleAlerts) {
            String title;
            if (this.numberOfTrials == 1) title = "CREATE 1 NEW GESTURE";
            else title = "CREATE " + this.numberOfTrials + " NEW GESTURES";
            new AlertDialog.Builder(mainActivity)
                    .setTitle(title)
                    .setMessage("\n\nPlease DO NOT draw letters or numbers as gestures.\n\nAll gestures must be different from the ones you already created. \n\n")
                    .setPositiveButton("START", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mainActivity.startNewTrial();
                            mainActivity.blinkCommandName();
                        }
                    })
                    .setCancelable(false)
                    .show();
        } else {
            mainActivity.startNewTrial();
            mainActivity.blinkCommandName();
        }
    }

    public void trialFinished(final MainActivity mainActivity) {
        trialNumber = trialNumber + 1;

        if(this.trialNumber == this.numberOfTrials) {
            if(this.testAfterEachBlock) {
                experiment.setCurrentBlock(new ExperimentTestingBlock(this));
                experiment.currentBlock.startBlock(mainActivity);
            } else {
//                new AlertDialog.Builder(mainActivity)
//                        .setTitle("BLOCK COMPLETE")
//                        .setMessage("\n\nPlease SHOW THIS SCREEN to the experimenter before continuing: \n\n"+currentFeedForwardType())
//                        .setPositiveButton("START", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
                                mainActivity.startNewBlock();
//                            }
//                        })
//                        .setCancelable(false)
//                        .show();
            }
        } else {
            mainActivity.startNewTrial();
        }
    }

    public Mode currentFeedForwardType() {
        //should take it from the counterbalancing
        //return Mode.PATH_FF;
        return blockingStrategy.feedForwardForTrial(experiment.participantID,this.blockNumber, this.trialNumber);
//        return experiment.feedForwardForBlock(this.blockNumber);
    }

    public String instructionsTextForCommand() {
        return "Create a gesture for: ";
    }



}
