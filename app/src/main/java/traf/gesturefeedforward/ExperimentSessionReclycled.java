package traf.gesturefeedforward;

/**
 * Created by carlagriggio on 25/11/15.
 */
public class ExperimentSessionReclycled extends ExperimentSession {
    static int round = 1;
    public ExperimentSessionReclycled(int participantID) {
        super(participantID);
        round = round + 1;
    }

    public ExperimentSessionReclycled(ExperimentSession experiment) {
        super(experiment.participantID);
        this.gestureStore = experiment.gestureStore;
    }

    protected void createCommands() {
        super.createCommands();
        for(CommandLabel c: this.commands) {
            c.label = c.label + " "+round;
        }
    }
}
