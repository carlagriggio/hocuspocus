package traf.gesturefeedforward;

import android.gesture.Gesture;

import java.util.ArrayList;

/**
 * Created by carlagriggio on 11/16/15.
 */
public class GestureRegistrationTask extends GestureGenerationTask {
    private ArrayList<Gesture> gestureBuffer = new ArrayList<Gesture>();

    public GestureRegistrationTask(int trialNumber, CommandLabel command, Block block) {
        super(trialNumber, command, block);
    }
    public void initTask(MainActivity mainActivity) {
        mainActivity.setCommandLabelAndInstructions(command.label, command.instruction);
        mainActivity.setLabelOfNextTaskButton(this.textForNextTaskButton());
        mainActivity.showBoxesForRegistration();
        //mainActivity.showBackButton();
    }
    public String textForNextTaskButton() {
        return "SAVE";
    }
    public boolean shouldRegisterGestures() {
        return true;
    }
    public void gestureFinished(MainActivity mainActivity) {
        mainActivity.bufferGesture(this);
//        mainActivity.blinkDetectedGesture();
    }
    public void addGesture(String label, Gesture g) {
        gestureBuffer.add(g);
    }
    public boolean canSaveGesture() {
        return gestureBuffer.size() == 3;
    }
    public void finishTask(MainActivity mainActivity) {
        for(Gesture g : gestureBuffer) {
            mainActivity.saveGesture(command.label,g);
        }
        block.trialFinished(mainActivity);
        //mainActivity.hideBoxes();
    }

    protected String taskNameForLogger() {
        return "R";
    }
}
