package traf.gesturefeedforward;

import android.gesture.Gesture;

import java.util.ArrayList;

import traf.gesturefeedforward.GesturePerformanceFeedback;

/**
 * Created by carlagriggio on 03/02/16.
 */
public class GestureGenerationWithRegistrationTask extends GestureGenerationTask {
    public GestureGenerationWithRegistrationTask(int trialNumber, CommandLabel command, Block block) {
        super(trialNumber, command, block);
        GestureRegistrationTask.GestureBuffer.put(command.label, new ArrayList<Gesture>());

    }
    public String textForNextTaskButton() {
        return "REGISTER 1/3";
    }

    public void gestureFinished(GestureCanvas aCanvas) {
        super.gestureFinished(aCanvas);
        if(aCanvas.rawScore >= GesturePerformanceFeedback.OkThreashold) {
            aCanvas.mainActivity.nextTrialButton.setEnabled(false);
        } else {
            aCanvas.mainActivity.nextTrialButton.setEnabled(true);
        }
    }
    public void initTask(MainActivity mainActivity) {
        mainActivity.gestureCanvasFrame.isTimed = false;
        mainActivity.nextTrialButton.setEnabled(false);
        super.initTask(mainActivity);
        mainActivity.showBackButton();
    }
    public void finishTask(MainActivity mainActivity) {
        this.end();
        ArrayList<Gesture> buffer = (ArrayList<Gesture>) GestureRegistrationTask.GestureBuffer.get(command.label);
        buffer.add(mainActivity.gestureFromFFCanvas());

        GestureRegistrationTask nextTask = new GestureRegistrationTask(this.trialNumber,this.command,this.block,2);
        mainActivity.startNextTask(nextTask);
        nextTask.setPreviousTask(this);
    }
}
