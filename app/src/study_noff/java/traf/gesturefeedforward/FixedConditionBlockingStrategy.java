package traf.gesturefeedforward;

import java.util.Arrays;

/**
 * Created by carlagriggio on 03/02/16.
 */
public class FixedConditionBlockingStrategy extends SimpleBlockingStrategy {
    private final Mode condition;

    public FixedConditionBlockingStrategy(Mode aFFCondition) {
        condition = aFFCondition;
    }
    protected int conditionIndex(int blockNumber,int trialNumber) {
        return Arrays.asList(Mode.values()).indexOf(condition);
    }
}
